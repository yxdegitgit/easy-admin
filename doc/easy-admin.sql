/*
 Navicat Premium Data Transfer

 Source Server         : localhost8.0
 Source Server Type    : MySQL
 Source Server Version : 80036
 Source Host           : localhost:3307
 Source Schema         : easy-admin

 Target Server Type    : MySQL
 Target Server Version : 80036
 File Encoding         : 65001

 Date: 05/03/2024 17:51:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_controller_methods
-- ----------------------------
DROP TABLE IF EXISTS `gen_controller_methods`;
CREATE TABLE `gen_controller_methods`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint NULL DEFAULT NULL COMMENT '表id',
  `is_add` tinyint NULL DEFAULT 0 COMMENT '是否生成添加方法 0 生成 1 不生成',
  `is_delete` tinyint NULL DEFAULT 0 COMMENT '是否生成删除方法 0 生成 1 不生成',
  `is_update` tinyint NULL DEFAULT 0 COMMENT '是否生成更新方法 0 生成 1 不生成',
  `is_list` tinyint NULL DEFAULT 0 COMMENT '是否生成查询方法 0 生成 1 不生成',
  `is_export` tinyint NULL DEFAULT 0 COMMENT '是否生成导出方法 0 生成 1 不生成',
  `is_import` tinyint NULL DEFAULT 0 COMMENT '是否生成导入方法 0 生成 1 不生成',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建者id',
  `create_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` tinyint NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成控制器方法' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_controller_methods
-- ----------------------------
INSERT INTO `gen_controller_methods` VALUES (1, 156, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 09:47:47', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (2, 157, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 09:55:42', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (3, 158, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 10:45:28', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (4, 159, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (5, 160, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (6, 161, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 11:04:39', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (7, 162, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 11:15:08', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (8, 163, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 11:19:23', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (9, 164, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 11:21:02', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (10, 165, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-06 14:46:52', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (11, 166, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-18 21:19:02', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (12, 167, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-19 14:53:03', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (13, 168, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-19 15:30:02', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (14, 169, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-19 15:37:41', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (15, 170, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-21 21:59:53', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (16, 171, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (17, 172, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2023-12-22 16:21:44', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (18, 173, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (19, 174, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-15 21:25:47', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (20, 175, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (21, 176, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (22, 177, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (23, 178, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (24, 179, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-25 13:44:39', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (25, 180, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-25 17:21:49', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (26, 181, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (27, 182, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (28, 183, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-02-05 04:09:22', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (29, 184, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-02-05 04:24:00', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_controller_methods` VALUES (30, 185, 0, 0, 0, 0, 0, 0, 1000000000000000001, 'admin', '2024-02-05 04:49:01', NULL, '', NULL, NULL, 0);

-- ----------------------------
-- Table structure for gen_create_table_record
-- ----------------------------
DROP TABLE IF EXISTS `gen_create_table_record`;
CREATE TABLE `gen_create_table_record`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `table_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '中文名称',
  `table_en_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '英文名称',
  `gen_base_field` tinyint NULL DEFAULT NULL COMMENT '是否生成基础字段 0 生成 1 不生成',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1171489978102841405 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '建表记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_create_table_record
-- ----------------------------
INSERT INTO `gen_create_table_record` VALUES (1171489978102841356, '会员表', 'ap_member', 0, 1000000000000000001, '2023-11-18 13:53:18', 'admin', NULL, '2023-11-18 13:53:18', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841357, '品牌表', 'ap_brand', 1, 1000000000000000001, '2023-11-18 13:58:11', 'admin', NULL, '2023-11-18 13:58:12', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841358, '订单表', 'ap_order', 0, 1000000000000000001, '2023-11-18 15:25:42', 'admin', NULL, '2023-11-18 15:25:42', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841359, '字典类型', 'sys_dict_type', 0, 1000000000000000001, '2023-11-18 16:12:30', 'admin', NULL, '2023-11-18 16:12:30', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841360, '测试表 ', 'ap_test', 0, 1000000000000000001, '2023-11-19 14:18:27', 'admin', NULL, '2023-11-19 14:18:26', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841361, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 14:23:32', 'admin', NULL, '2023-11-19 14:23:31', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841362, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 16:19:35', 'admin', NULL, '2023-11-19 16:19:34', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841363, '测试1表', 'ap_test1', 0, 1000000000000000001, '2023-11-19 17:45:08', 'admin', NULL, '2023-11-19 17:45:08', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841364, '测试2表', 'ap_test2', 0, 1000000000000000001, '2023-11-19 17:47:21', 'admin', NULL, '2023-11-19 17:47:21', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841365, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 18:05:08', 'admin', NULL, '2023-11-19 18:05:08', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841366, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 18:09:37', 'admin', NULL, '2023-11-19 18:09:36', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841367, '测试表', 'ap_test1', 0, 1000000000000000001, '2023-11-19 18:13:11', 'admin', NULL, '2023-11-19 18:13:11', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841368, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 18:31:29', 'admin', NULL, '2023-11-19 18:31:28', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841369, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 18:37:36', 'admin', NULL, '2023-11-19 18:37:36', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841370, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 18:48:32', 'admin', NULL, '2023-11-19 18:48:31', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841371, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-19 21:49:19', 'admin', NULL, '2023-11-19 21:49:19', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841372, '测试1表', 'ap_test1', 0, 1000000000000000001, '2023-11-20 21:21:10', 'admin', NULL, '2023-11-20 21:21:10', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841373, '测试1表', 'ap_test1', 0, 1000000000000000001, '2023-11-20 21:21:10', 'admin', NULL, '2023-11-20 21:21:10', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841374, '测试1表', 'ap_test1', 0, 1000000000000000001, '2023-11-20 21:21:10', 'admin', NULL, '2023-11-20 21:21:10', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841375, '系统配置表', 'sys_config', 0, 1000000000000000001, '2023-11-20 21:37:06', 'admin', NULL, '2023-11-20 21:37:10', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841376, '系统配置表', 'sys_config', 0, 1000000000000000001, '2023-11-20 22:11:31', 'admin', NULL, '2023-11-20 22:11:30', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841377, '文件存储表', 'sys_oss', 0, 1000000000000000001, '2023-11-20 22:43:13', 'admin', NULL, '2023-11-20 22:43:12', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841378, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-21 17:29:22', 'admin', NULL, '2023-11-21 17:29:22', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841379, '测试2表', 'ap_test2', 0, 1000000000000000001, '2023-11-21 17:46:41', 'admin', NULL, '2023-11-21 17:46:41', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841380, '测试2表', 'ap_test3', 0, 1000000000000000001, '2023-11-21 18:03:01', 'admin', NULL, '2023-11-21 18:03:01', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841381, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-23 14:09:05', 'admin', NULL, '2023-11-23 14:09:05', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841382, '测试表', 'ap_test', 0, 1000000000000000001, '2023-11-25 11:29:25', 'admin', NULL, '2023-11-25 11:29:25', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841383, '测试1表', 'ap_test1', 0, 1000000000000000001, '2023-11-27 17:48:51', 'admin', NULL, '2023-11-27 17:48:52', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841384, '公告管理表', 'sys_notify', 0, 1000000000000000001, '2023-12-06 09:47:38', 'admin', NULL, '2023-12-06 09:47:39', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841385, '通知公告表', 'sys_notify', 0, 1000000000000000001, '2023-12-06 09:55:26', 'admin', NULL, '2023-12-06 09:55:26', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841386, '消息表', 'sys_message', 0, 1000000000000000001, '2023-12-06 14:13:37', 'admin', NULL, '2023-12-06 14:13:37', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841387, '测试表5', 'ap_test5', 0, 1000000000000000001, '2023-12-18 21:18:50', 'admin', NULL, '2023-12-18 21:18:50', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841388, '商品表', 'goods', 1, 1000000000000000001, '2023-12-19 14:52:51', 'admin', NULL, '2023-12-19 14:52:51', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841389, '状态跟踪', 'tracking_state', 0, 1000000000000000001, '2023-12-19 15:37:05', 'admin', NULL, '2023-12-19 15:37:05', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841390, '测试建表', 'test_table', 0, 1000000000000000001, '2023-12-19 16:19:10', 'admin', NULL, '2023-12-19 16:19:09', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841391, '测试3表3', 'ap_test3', 0, 1000000000000000001, '2023-12-21 21:48:28', 'admin', NULL, '2023-12-21 21:48:28', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841392, '测试表3', 'ap_test3', 0, 1000000000000000001, '2023-12-21 21:50:09', 'admin', NULL, '2023-12-21 21:50:09', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841393, '测试表3', 'ap_test3', 0, 1000000000000000001, '2023-12-21 21:50:38', 'admin', NULL, '2023-12-21 21:50:38', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841394, '测试表3', 'ap_test3', 0, 1000000000000000001, '2023-12-21 21:52:08', 'admin', NULL, '2023-12-21 21:52:08', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841395, '测试表3', 'ap_test3', 0, 1000000000000000001, '2023-12-21 21:58:29', 'admin', NULL, '2023-12-21 21:58:29', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841396, '测试表4', 'ap_test4', 0, 1000000000000000001, '2023-12-21 22:14:38', 'admin', NULL, '2023-12-21 22:14:37', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841397, '测试表6', 'ap_test6', 1, 1000000000000000001, '2023-12-22 16:21:36', 'admin', NULL, '2023-12-22 16:21:36', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841398, '测试9表', 'ap_test9', 0, 1000000000000000001, '2024-01-15 21:12:11', 'admin', NULL, '2024-01-15 21:12:11', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841399, '学生表', 'student', 0, 1000000000000000001, '2024-01-15 21:25:18', 'admin', NULL, '2024-01-15 21:25:19', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841400, '测试表10', 'app_test10', 0, 1000000000000000001, '2024-01-15 21:36:59', 'admin', NULL, '2024-01-15 21:37:00', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841401, '学生表', 'students', 0, 1000000000000000001, '2024-01-15 21:47:24', 'admin', NULL, '2024-01-15 21:47:25', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841402, '课程表', 'course', 0, 1000000000000000001, '2024-01-16 23:54:42', 'admin', NULL, '2024-01-16 23:54:42', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841403, '测试表8', 'ap_test8', 0, 1000000000000000001, '2024-01-27 00:53:06', 'admin', NULL, '2024-01-27 00:53:06', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841404, '测试用户表', 'ap_user_test', 0, 1000000000000000001, '2024-01-27 00:55:47', 'admin', NULL, '2024-01-27 00:55:47', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841405, '用户表', 'a_user', 0, 1000000000000000001, '2024-02-05 04:05:46', 'admin', NULL, '2024-02-05 04:05:45', NULL, 0);

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_menu_type` tinyint NULL DEFAULT NULL COMMENT '1 目录 2 菜单',
  `gen_html_js` tinyint NULL DEFAULT 0 COMMENT '是否生成前端html、js 0 生成 1 不生成',
  `gen_controller` tinyint NULL DEFAULT 0 COMMENT '是否生成控制器 0 生成 1 不生成',
  `gen_parent_menu_id` bigint NULL DEFAULT NULL COMMENT '生成的父级菜单id',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建者id',
  `create_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` tinyint NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 186 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (125, 'ap_member', '会员表', 'ApMember', 'crud', 'com.mars.module.admin', 'admin', 'member', '会员', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-18 13:53:18', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (126, 'ap_brand', '品牌表', 'ApBrand', 'crud', 'com.mars.module.admin', 'admin', 'brand', '品牌', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-18 13:58:12', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (127, 'ap_order', '订单表', 'ApOrder', 'crud', 'com.mars.module.admin', 'admin', 'order', '订单', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-16 11:11:33', NULL, '', '2023-11-16 11:25:10', NULL, 1);
INSERT INTO `gen_table` VALUES (128, 'ap_order', '订单表', 'ApOrder', 'crud', 'com.mars.module.admin', 'admin', 'order', '订单', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-18 15:25:42', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (129, 'sys_dict_type', '字典类型', 'SysDictType', 'crud', 'com.mars.module.admin', 'admin', 'dictType', '字典类型', 'mars', 2, 0, 0, 1171101653676327296, '{}', 1000000000000000001, 'admin', '2023-11-18 16:12:30', 1000000000000000001, 'admin', '2023-11-18 16:13:43', NULL, 1);
INSERT INTO `gen_table` VALUES (130, 'sys_dict_data', '字典数据表', 'SysDictData', 'crud', 'com.mars.module.admin', 'admin', 'dictData', '字典数据', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-18 16:33:54', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (131, 'sys_dict_data', '字典数据表', 'SysDictData', 'crud', 'com.mars.module.admin', 'admin', 'dictData', '字典数据', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-18 16:35:52', 1000000000000000001, 'admin', '2023-11-18 16:37:22', NULL, 1);
INSERT INTO `gen_table` VALUES (132, 'sys_dict_data', '字典数据表', 'SysDictData', 'crud', 'com.mars.module.admin', 'admin', 'dictData', '字典数据', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-18 16:35:52', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (133, 'sys_dict_type', '字典类型', 'SysDictType', 'crud', 'com.mars.module.admin', 'admin', 'dictType', '字典类型', 'mars', 2, 0, 0, 1171101653676327296, '{}', 1000000000000000001, 'admin', '2023-11-18 16:12:30', 1000000000000000001, 'admin', '2023-11-18 16:48:19', NULL, 1);
INSERT INTO `gen_table` VALUES (134, 'ap_test', '测试表 ', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试 ', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-19 14:18:26', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (135, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-19 14:23:31', 1000000000000000001, 'admin', '2023-11-19 14:24:00', NULL, 1);
INSERT INTO `gen_table` VALUES (136, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-19 16:19:34', 1000000000000000001, 'admin', '2023-11-19 16:20:01', NULL, 1);
INSERT INTO `gen_table` VALUES (137, 'ap_test1', '测试1表', 'ApTest1', 'crud', 'com.mars.module.admin', 'admin', 'test1', '测试1', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-19 17:45:07', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (138, 'ap_test2', '测试2表', 'ApTest2', 'crud', 'com.mars.module.admin', 'admin', 'test2', '测试2', 'mars', 2, 0, 0, 1171101653676327337, '{}', 1000000000000000001, 'admin', '2023-11-19 17:47:21', 1000000000000000001, 'admin', '2023-11-19 17:47:46', NULL, 1);
INSERT INTO `gen_table` VALUES (139, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-19 18:05:07', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (140, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-11-19 18:09:36', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (141, 'ap_test1', '测试表', 'ApTest1', 'crud', 'com.mars.module.admin', 'admin', 'test1', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-19 18:13:11', 1000000000000000001, 'admin', '2023-11-19 18:13:39', NULL, 1);
INSERT INTO `gen_table` VALUES (142, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-19 18:31:28', 1000000000000000001, 'admin', '2023-11-19 18:32:11', NULL, 1);
INSERT INTO `gen_table` VALUES (143, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-19 18:37:35', 1000000000000000001, 'admin', '2023-11-19 18:38:12', NULL, 1);
INSERT INTO `gen_table` VALUES (144, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-19 18:48:31', 1000000000000000001, 'admin', '2023-11-19 18:49:16', NULL, 1);
INSERT INTO `gen_table` VALUES (145, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-19 21:49:19', 1000000000000000001, 'admin', '2023-11-19 21:50:00', NULL, 1);
INSERT INTO `gen_table` VALUES (146, 'ap_test1', '测试1表', 'ApTest1', 'crud', 'com.mars.module.admin', 'admin', 'test1', '测试1', 'mars', 2, 0, 0, 1171101653676327420, '{}', 1000000000000000001, 'admin', '2023-11-20 21:21:09', 1000000000000000001, 'admin', '2023-11-20 21:22:38', NULL, 1);
INSERT INTO `gen_table` VALUES (147, 'sys_config', '系统配置表', 'SysConfig', 'crud', 'com.mars.module.system', 'system', 'config', '系统配置', 'mars', 2, 0, 0, 1171101653676327296, '{}', 1000000000000000001, 'admin', '2023-11-20 21:37:06', 1000000000000000001, 'admin', '2023-11-20 21:42:10', NULL, 1);
INSERT INTO `gen_table` VALUES (148, 'sys_config', '系统配置表', 'SysConfig', 'crud', 'com.mars.module.admin', 'admin', 'config', '系统配置', 'mars', 2, 0, 0, 1171101653676327296, '{}', 1000000000000000001, 'admin', '2023-11-20 22:11:29', 1000000000000000001, 'admin', '2023-11-20 22:13:41', NULL, 1);
INSERT INTO `gen_table` VALUES (149, 'sys_oss', '文件存储表', 'SysOss', 'crud', 'com.mars.module.admin', 'admin', 'oss', '文件存储', 'mars', 2, 0, 0, 1171101653676327296, '{}', 1000000000000000001, 'admin', '2023-11-20 22:43:12', 1000000000000000001, 'admin', '2023-11-20 22:43:48', NULL, 1);
INSERT INTO `gen_table` VALUES (150, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-21 17:29:22', 1000000000000000001, 'admin', '2023-11-21 17:29:51', NULL, 1);
INSERT INTO `gen_table` VALUES (151, 'ap_test2', '测试2表', 'ApTest2', 'crud', 'com.mars.module.admin', 'admin', 'test2', '测试2', 'mars', 2, 0, 0, 1171101653676327459, '{}', 1000000000000000001, 'admin', '2023-11-21 17:46:41', 1000000000000000001, 'admin', '2023-11-21 17:47:15', NULL, 1);
INSERT INTO `gen_table` VALUES (152, 'ap_test3', '测试2表', 'ApTest3', 'crud', 'com.mars.module.admin', 'admin', 'test3', '测试2', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-21 18:03:00', 1000000000000000001, 'admin', '2023-11-21 18:03:49', NULL, 1);
INSERT INTO `gen_table` VALUES (153, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-23 14:09:05', 1000000000000000001, 'admin', '2023-11-23 14:10:45', NULL, 1);
INSERT INTO `gen_table` VALUES (154, 'ap_test', '测试表', 'ApTest', 'crud', 'com.mars.module.admin', 'admin', 'test', '测试', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-25 11:29:25', 1000000000000000001, 'admin', '2023-11-25 11:30:20', NULL, 0);
INSERT INTO `gen_table` VALUES (155, 'ap_test1', '测试1表', 'ApTest1', 'crud', 'com.mars.module.system', 'system', 'test1', '测试1', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-11-27 17:48:52', 1000000000000000001, 'admin', '2024-01-09 13:57:08', NULL, 0);
INSERT INTO `gen_table` VALUES (156, 'sys_notify', '公告管理表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '公告管理', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-06 09:47:38', 1000000000000000001, 'admin', '2023-12-06 09:48:09', NULL, 1);
INSERT INTO `gen_table` VALUES (157, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-06 09:55:26', 1000000000000000001, 'admin', '2023-12-06 09:57:18', NULL, 1);
INSERT INTO `gen_table` VALUES (158, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-06 10:43:53', 1000000000000000001, 'admin', '2023-12-06 10:47:14', NULL, 1);
INSERT INTO `gen_table` VALUES (159, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-12-06 10:43:53', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (160, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-12-06 10:43:53', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (161, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-06 10:43:53', 1000000000000000001, 'admin', '2023-12-06 11:05:29', NULL, 1);
INSERT INTO `gen_table` VALUES (162, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-06 10:43:53', 1000000000000000001, 'admin', '2023-12-06 11:15:22', NULL, 1);
INSERT INTO `gen_table` VALUES (163, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-06 10:43:53', 1000000000000000001, 'admin', '2023-12-06 11:19:38', NULL, 1);
INSERT INTO `gen_table` VALUES (164, 'sys_notify', '通知公告表', 'SysNotify', 'crud', 'com.mars.module.admin', 'admin', 'notify', '通知公告', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-06 10:43:53', 1000000000000000001, 'admin', '2023-12-06 11:21:36', NULL, 0);
INSERT INTO `gen_table` VALUES (165, 'sys_message', '消息表', 'SysMessage', 'crud', 'com.mars.module.admin', 'admin', 'message', '消息', 'mars', 2, 0, 0, 1171101653676327674, '{}', 1000000000000000001, 'admin', '2023-12-06 14:46:51', 1000000000000000001, 'admin', '2023-12-06 14:48:16', NULL, 0);
INSERT INTO `gen_table` VALUES (166, 'ap_test5', '测试表5', 'ApTest5', 'crud', 'com.mars.module.admin', 'admin', 'test5', '测试5', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:34', NULL, 0);
INSERT INTO `gen_table` VALUES (167, 'goods', '商品表', 'Goods', 'crud', 'com.mars.module.admin', 'admin', 'goods', '商品', 'mars', 2, 0, 0, 1171101653676327296, '{}', 1000000000000000001, 'admin', '2023-12-19 14:53:03', 1000000000000000001, 'admin', '2023-12-19 15:14:07', NULL, 1);
INSERT INTO `gen_table` VALUES (168, 'goods', '商品表', 'Goods', 'crud', 'com.mars.module.admin', 'admin', 'goods', '商品', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-19 15:30:02', 1000000000000000001, 'admin', '2023-12-19 15:30:29', NULL, 1);
INSERT INTO `gen_table` VALUES (169, 'tracking_state', '状态跟踪', 'TrackingState', 'crud', 'com.mars.module.admin', 'admin', 'state', '状态跟踪', 'mars', 2, 0, 0, 1000000000000003001, '{}', 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:32', NULL, 1);
INSERT INTO `gen_table` VALUES (170, 'ap_test3', '测试表3', 'ApTest3', 'crud', 'com.mars.module.admin', 'admin', 'test3', '测试3', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:42', NULL, 1);
INSERT INTO `gen_table` VALUES (171, 'ap_test4', '测试表4', 'ApTest4', 'crud', 'com.mars.module.admin', 'admin', 'test4', '测试4', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (172, 'ap_test6', '测试表6', 'ApTest6', 'crud', 'com.mars.module.admin', 'admin', 'test6', '测试6', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2023-12-22 16:21:44', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (173, 'ap_test9', '测试9表', 'ApTest9', 'crud', 'com.mars.module.admin', 'admin', 'test9', '测试9', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_table` VALUES (174, 'student', '学生表', 'Student', 'crud', 'com.mars.module.admin', 'admin', 'student', '学生', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', NULL, 1);
INSERT INTO `gen_table` VALUES (175, 'app_test10', '测试表10', 'AppTest10', 'crud', 'com.mars.module.admin', 'admin', 'test10', '测试10', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_table` VALUES (176, 'students', '学生表', 'Students', 'crud', 'com.mars.module.admin', 'admin', 'students', '学生', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_table` VALUES (177, 'course', '课程表', 'Course', 'crud', 'com.mars.module.admin', 'admin', 'course', '课程', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_table` VALUES (178, 'biz_leave', '请假表', 'BizLeave', 'crud', 'com.mars.module.admin', 'admin', 'leave', '请假', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2024-01-25 13:42:05', NULL, '', NULL, NULL, 1);
INSERT INTO `gen_table` VALUES (179, 'biz_leave', '请假表', 'BizLeave', 'crud', 'com.mars.module.admin', 'admin', 'leave', '请假', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2024-01-25 13:44:38', 1000000000000000001, 'admin', '2024-01-25 13:47:44', NULL, 0);
INSERT INTO `gen_table` VALUES (180, 'process_node_assignee', '流程审批节点人员关联表', 'ProcessNodeAssignee', 'crud', 'com.mars.module.workflow', 'admin', 'nodeAssignee', '审批人员', 'mars', 2, 0, 0, 1171101653676327914, '{}', 1000000000000000001, 'admin', '2024-01-25 17:21:48', 1000000000000000001, 'admin', '2024-01-25 17:24:46', NULL, 0);
INSERT INTO `gen_table` VALUES (181, 'ap_test8', '测试表8', 'ApTest8', 'crud', 'com.mars.module.admin', 'admin', 'test8', '测试8', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_table` VALUES (182, 'ap_user_test', '测试用户表', 'ApUserTest', 'crud', 'com.mars.module.admin', 'admin', 'userTest', '测试用户', 'mars', NULL, 0, 0, NULL, NULL, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, '', NULL, NULL, 0);
INSERT INTO `gen_table` VALUES (183, 'a_user', '用户表', 'AUser', 'crud', 'com.mars.module.admin', 'admin', 'user', '用户管理', 'caigou', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', NULL, 1);
INSERT INTO `gen_table` VALUES (184, 'a_user', '用户表', 'AUser', 'crud', 'com.mars.module.admin', 'admin', 'user', '用户', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', NULL, 1);
INSERT INTO `gen_table` VALUES (185, 'a_user', '用户表', 'AUser', 'crud', 'com.mars.module.admin', 'admin', 'user', '用户', 'mars', 1, 0, 0, NULL, '{}', 1000000000000000001, 'admin', '2024-02-05 04:49:00', 1000000000000000001, 'admin', '2024-02-05 04:49:56', NULL, 0);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建者id',
  `create_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新者id',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint NULL DEFAULT 0 COMMENT '逻辑删除 默认0 删除1 ',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1888 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1202, '125', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1203, '125', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1204, '125', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1205, '125', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1206, '125', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1207, '125', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1208, '125', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1209, '125', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1210, '125', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1211, '125', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 13:55:15', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1212, '126', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 13:58:22', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1213, '126', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 13:58:22', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1214, '126', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 13:58:22', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1215, '127', 'id', 'ID', 'int', 'Integer', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1216, '127', 'order_number', '订单号', 'varchar(255)', 'String', 'orderNumber', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1217, '127', 'user_id', 'user_id', 'bigint', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1218, '127', 'product_id', '商品id', 'int', 'Integer', 'productId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1219, '127', 'payment_time', '支付时间', 'datetime', 'Date', 'paymentTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1220, '127', 'amount', '金额', 'decimal(10,2)', 'BigDecimal', 'amount', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1221, '127', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1222, '127', 'instructions', '说明', 'varchar(2000)', 'String', 'instructions', '0', '0', NULL, '1', '1', '1', '1', NULL, 'textarea', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1223, '127', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1224, '127', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1225, '127', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1226, '127', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1227, '127', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 13, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1228, '127', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 14, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1229, '127', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 15, 1000000000000000001, 'admin', '2023-11-18 14:02:34', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1230, '128', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1231, '128', 'order_number', '订单号', 'varchar(255)', 'String', 'orderNumber', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1232, '128', 'user_id', 'user_id', 'bigint', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1233, '128', 'product_id', 'product_id', 'bigint', 'Long', 'productId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1234, '128', 'amount', '金额', 'decimal(10,2)', 'BigDecimal', 'amount', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1235, '128', 'number', '数量', 'decimal(10,2)', 'BigDecimal', 'number', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1236, '128', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1237, '128', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1238, '128', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1239, '128', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1240, '128', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 11, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1241, '128', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1242, '128', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 13, 1000000000000000001, 'admin', '2023-11-18 15:26:17', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1243, '129', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:43', 1);
INSERT INTO `gen_table_column` VALUES (1244, '129', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:43', 1);
INSERT INTO `gen_table_column` VALUES (1245, '129', 'type', '类型', 'varchar(100)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:43', 1);
INSERT INTO `gen_table_column` VALUES (1246, '129', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:43', 1);
INSERT INTO `gen_table_column` VALUES (1247, '129', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:43', 1);
INSERT INTO `gen_table_column` VALUES (1248, '129', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:44', 1);
INSERT INTO `gen_table_column` VALUES (1249, '129', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:44', 1);
INSERT INTO `gen_table_column` VALUES (1250, '129', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:44', 1);
INSERT INTO `gen_table_column` VALUES (1251, '129', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:44', 1);
INSERT INTO `gen_table_column` VALUES (1252, '129', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:44', 1);
INSERT INTO `gen_table_column` VALUES (1253, '129', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-18 16:12:44', 1000000000000000001, 'admin', '2023-11-18 16:13:44', 1);
INSERT INTO `gen_table_column` VALUES (1254, '130', 'id', '编码', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1255, '130', 'dict_sort', '字典排序', 'int', 'Integer', 'dictSort', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1256, '130', 'dict_label', '字典标签', 'varchar(100)', 'String', 'dictLabel', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1257, '130', 'dict_value', '字典键值', 'varchar(100)', 'String', 'dictValue', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1258, '130', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1259, '130', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1260, '130', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1261, '130', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1262, '130', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1263, '130', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1264, '130', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-18 16:34:20', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1265, '131', 'id', '编码', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:22', 1);
INSERT INTO `gen_table_column` VALUES (1266, '131', 'dict_type_id', '字典类型ID', 'bigint', 'Long', 'dictTypeId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:22', 1);
INSERT INTO `gen_table_column` VALUES (1267, '131', 'dict_sort', '字典排序', 'int', 'Integer', 'dictSort', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:22', 1);
INSERT INTO `gen_table_column` VALUES (1268, '131', 'dict_label', '字典标签', 'varchar(100)', 'String', 'dictLabel', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:22', 1);
INSERT INTO `gen_table_column` VALUES (1269, '131', 'dict_value', '字典键值', 'varchar(100)', 'String', 'dictValue', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:22', 1);
INSERT INTO `gen_table_column` VALUES (1270, '131', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:22', 1);
INSERT INTO `gen_table_column` VALUES (1271, '131', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:22', 1);
INSERT INTO `gen_table_column` VALUES (1272, '131', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:23', 1);
INSERT INTO `gen_table_column` VALUES (1273, '131', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:23', 1);
INSERT INTO `gen_table_column` VALUES (1274, '131', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:23', 1);
INSERT INTO `gen_table_column` VALUES (1275, '131', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:23', 1);
INSERT INTO `gen_table_column` VALUES (1276, '131', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2023-11-18 16:36:00', 1000000000000000001, 'admin', '2023-11-18 16:37:23', 1);
INSERT INTO `gen_table_column` VALUES (1277, '132', 'id', '编码', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1278, '132', 'dict_type_id', '字典类型ID', 'bigint', 'Long', 'dictTypeId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1279, '132', 'dict_sort', '字典排序', 'int', 'Integer', 'dictSort', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1280, '132', 'dict_label', '字典标签', 'varchar(100)', 'String', 'dictLabel', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1281, '132', 'dict_value', '字典键值', 'varchar(100)', 'String', 'dictValue', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1282, '132', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1283, '132', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1284, '132', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1285, '132', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1286, '132', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1287, '132', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1288, '132', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2023-11-18 16:47:48', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1289, '133', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:19', 1);
INSERT INTO `gen_table_column` VALUES (1290, '133', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:19', 1);
INSERT INTO `gen_table_column` VALUES (1291, '133', 'type', '类型', 'varchar(100)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:19', 1);
INSERT INTO `gen_table_column` VALUES (1292, '133', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:19', 1);
INSERT INTO `gen_table_column` VALUES (1293, '133', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:19', 1);
INSERT INTO `gen_table_column` VALUES (1294, '133', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:19', 1);
INSERT INTO `gen_table_column` VALUES (1295, '133', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:19', 1);
INSERT INTO `gen_table_column` VALUES (1296, '133', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:20', 1);
INSERT INTO `gen_table_column` VALUES (1297, '133', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:20', 1);
INSERT INTO `gen_table_column` VALUES (1298, '133', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:20', 1);
INSERT INTO `gen_table_column` VALUES (1299, '133', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-18 16:47:48', 1000000000000000001, 'admin', '2023-11-18 16:48:20', 1);
INSERT INTO `gen_table_column` VALUES (1300, '134', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1301, '134', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1302, '134', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1303, '134', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1304, '134', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1305, '134', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1306, '134', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1307, '134', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1308, '134', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1309, '134', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 14:18:37', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1310, '135', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:00', 1);
INSERT INTO `gen_table_column` VALUES (1311, '135', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:00', 1);
INSERT INTO `gen_table_column` VALUES (1312, '135', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:00', 1);
INSERT INTO `gen_table_column` VALUES (1313, '135', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:00', 1);
INSERT INTO `gen_table_column` VALUES (1314, '135', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:00', 1);
INSERT INTO `gen_table_column` VALUES (1315, '135', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:00', 1);
INSERT INTO `gen_table_column` VALUES (1316, '135', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:00', 1);
INSERT INTO `gen_table_column` VALUES (1317, '135', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:01', 1);
INSERT INTO `gen_table_column` VALUES (1318, '135', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:01', 1);
INSERT INTO `gen_table_column` VALUES (1319, '135', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 14:23:40', 1000000000000000001, 'admin', '2023-11-19 14:24:01', 1);
INSERT INTO `gen_table_column` VALUES (1320, '136', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:01', 1);
INSERT INTO `gen_table_column` VALUES (1321, '136', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:01', 1);
INSERT INTO `gen_table_column` VALUES (1322, '136', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1323, '136', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'sys_status', 4, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1324, '136', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1325, '136', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1326, '136', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1327, '136', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1328, '136', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1329, '136', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1330, '136', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-19 16:19:43', 1000000000000000001, 'admin', '2023-11-19 16:20:02', 1);
INSERT INTO `gen_table_column` VALUES (1331, '137', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1332, '137', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1333, '137', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1334, '137', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1335, '137', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1336, '137', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1337, '137', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1338, '137', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1339, '137', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1340, '137', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 17:45:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1341, '138', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:46', 1);
INSERT INTO `gen_table_column` VALUES (1342, '138', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:46', 1);
INSERT INTO `gen_table_column` VALUES (1343, '138', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:46', 1);
INSERT INTO `gen_table_column` VALUES (1344, '138', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:46', 1);
INSERT INTO `gen_table_column` VALUES (1345, '138', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:47', 1);
INSERT INTO `gen_table_column` VALUES (1346, '138', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:47', 1);
INSERT INTO `gen_table_column` VALUES (1347, '138', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:47', 1);
INSERT INTO `gen_table_column` VALUES (1348, '138', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:47', 1);
INSERT INTO `gen_table_column` VALUES (1349, '138', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:47', 1);
INSERT INTO `gen_table_column` VALUES (1350, '138', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 17:47:32', 1000000000000000001, 'admin', '2023-11-19 17:47:47', 1);
INSERT INTO `gen_table_column` VALUES (1351, '139', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1352, '139', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1353, '139', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1354, '139', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1355, '139', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1356, '139', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1357, '139', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1358, '139', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1359, '139', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1360, '139', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 18:05:19', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1361, '140', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1362, '140', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1363, '140', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1364, '140', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1365, '140', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1366, '140', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1367, '140', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1368, '140', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1369, '140', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1370, '140', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 18:09:56', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1371, '141', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1372, '141', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1373, '141', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'sys_status', 3, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1374, '141', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1375, '141', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1376, '141', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1377, '141', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1378, '141', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:39', 1);
INSERT INTO `gen_table_column` VALUES (1379, '141', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:40', 1);
INSERT INTO `gen_table_column` VALUES (1380, '141', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 18:13:22', 1000000000000000001, 'admin', '2023-11-19 18:13:40', 1);
INSERT INTO `gen_table_column` VALUES (1381, '142', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:11', 1);
INSERT INTO `gen_table_column` VALUES (1382, '142', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:11', 1);
INSERT INTO `gen_table_column` VALUES (1383, '142', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:11', 1);
INSERT INTO `gen_table_column` VALUES (1384, '142', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'sys_status', 4, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1385, '142', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1386, '142', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1387, '142', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1388, '142', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1389, '142', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1390, '142', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1391, '142', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-19 18:31:53', 1000000000000000001, 'admin', '2023-11-19 18:32:12', 1);
INSERT INTO `gen_table_column` VALUES (1392, '143', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:12', 1);
INSERT INTO `gen_table_column` VALUES (1393, '143', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:12', 1);
INSERT INTO `gen_table_column` VALUES (1394, '143', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1395, '143', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'sys_status', 4, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1396, '143', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1397, '143', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1398, '143', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1399, '143', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1400, '143', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1401, '143', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1402, '143', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-19 18:37:45', 1000000000000000001, 'admin', '2023-11-19 18:38:13', 1);
INSERT INTO `gen_table_column` VALUES (1403, '144', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:16', 1);
INSERT INTO `gen_table_column` VALUES (1404, '144', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:16', 1);
INSERT INTO `gen_table_column` VALUES (1405, '144', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:16', 1);
INSERT INTO `gen_table_column` VALUES (1406, '144', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'sys_status', 4, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:16', 1);
INSERT INTO `gen_table_column` VALUES (1407, '144', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:16', 1);
INSERT INTO `gen_table_column` VALUES (1408, '144', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:16', 1);
INSERT INTO `gen_table_column` VALUES (1409, '144', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:16', 1);
INSERT INTO `gen_table_column` VALUES (1410, '144', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:17', 1);
INSERT INTO `gen_table_column` VALUES (1411, '144', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:17', 1);
INSERT INTO `gen_table_column` VALUES (1412, '144', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:17', 1);
INSERT INTO `gen_table_column` VALUES (1413, '144', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-19 18:48:57', 1000000000000000001, 'admin', '2023-11-19 18:49:17', 1);
INSERT INTO `gen_table_column` VALUES (1414, '145', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1415, '145', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1416, '145', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1417, '145', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'image', NULL, 4, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1418, '145', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1419, '145', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1420, '145', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1421, '145', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:00', 1);
INSERT INTO `gen_table_column` VALUES (1422, '145', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:01', 1);
INSERT INTO `gen_table_column` VALUES (1423, '145', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:01', 1);
INSERT INTO `gen_table_column` VALUES (1424, '145', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-19 21:49:35', 1000000000000000001, 'admin', '2023-11-19 21:50:01', 1);
INSERT INTO `gen_table_column` VALUES (1425, '146', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:38', 1);
INSERT INTO `gen_table_column` VALUES (1426, '146', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1427, '146', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1428, '146', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'sys_status', 4, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1429, '146', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1430, '146', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1431, '146', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1432, '146', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1433, '146', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1434, '146', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:39', 1);
INSERT INTO `gen_table_column` VALUES (1435, '146', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-20 21:21:43', 1000000000000000001, 'admin', '2023-11-20 21:22:40', 1);
INSERT INTO `gen_table_column` VALUES (1436, '147', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:10', 1);
INSERT INTO `gen_table_column` VALUES (1437, '147', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1438, '147', 'configuration_values', '配置值', 'varchar(100)', 'String', 'configurationValues', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1439, '147', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', '1', '1', '1', '1', '1', NULL, 'select', 'sys_status', 4, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1440, '147', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1441, '147', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1442, '147', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1443, '147', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1444, '147', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1445, '147', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:11', 1);
INSERT INTO `gen_table_column` VALUES (1446, '147', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-20 21:39:57', 1000000000000000001, 'admin', '2023-11-20 21:42:12', 1);
INSERT INTO `gen_table_column` VALUES (1447, '148', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:41', 1);
INSERT INTO `gen_table_column` VALUES (1448, '148', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:41', 1);
INSERT INTO `gen_table_column` VALUES (1449, '148', 'field_key', 'field_key', 'varchar(100)', 'String', 'fieldKey', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:41', 1);
INSERT INTO `gen_table_column` VALUES (1450, '148', 'field_value', 'field_value', 'varchar(100)', 'String', 'fieldValue', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1451, '148', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1452, '148', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1453, '148', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1454, '148', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1455, '148', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1456, '148', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1457, '148', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-20 22:13:11', 1000000000000000001, 'admin', '2023-11-20 22:13:42', 1);
INSERT INTO `gen_table_column` VALUES (1458, '149', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1459, '149', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1460, '149', '端点', 'endpoint', 'varchar(100)', 'String', '端点', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1461, '149', 'access_key', 'access_key', 'varchar(100)', 'String', 'accessKey', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1462, '149', 'secret_key', 'secret_key', 'varchar(100)', 'String', 'secretKey', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1463, '149', 'bucket_name', 'bucket_Name', 'varchar(100)', 'String', 'bucketName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1464, '149', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1465, '149', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:48', 1);
INSERT INTO `gen_table_column` VALUES (1466, '149', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:49', 1);
INSERT INTO `gen_table_column` VALUES (1467, '149', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:49', 1);
INSERT INTO `gen_table_column` VALUES (1468, '149', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 11, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:49', 1);
INSERT INTO `gen_table_column` VALUES (1469, '149', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:49', 1);
INSERT INTO `gen_table_column` VALUES (1470, '149', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 13, 1000000000000000001, 'admin', '2023-11-20 22:43:23', 1000000000000000001, 'admin', '2023-11-20 22:43:49', 1);
INSERT INTO `gen_table_column` VALUES (1471, '150', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:51', 1);
INSERT INTO `gen_table_column` VALUES (1472, '150', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:51', 1);
INSERT INTO `gen_table_column` VALUES (1473, '150', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:51', 1);
INSERT INTO `gen_table_column` VALUES (1474, '150', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', '1', NULL, 'image', NULL, 4, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:51', 1);
INSERT INTO `gen_table_column` VALUES (1475, '150', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:51', 1);
INSERT INTO `gen_table_column` VALUES (1476, '150', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:51', 1);
INSERT INTO `gen_table_column` VALUES (1477, '150', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:52', 1);
INSERT INTO `gen_table_column` VALUES (1478, '150', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:52', 1);
INSERT INTO `gen_table_column` VALUES (1479, '150', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:52', 1);
INSERT INTO `gen_table_column` VALUES (1480, '150', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:52', 1);
INSERT INTO `gen_table_column` VALUES (1481, '150', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-21 17:29:32', 1000000000000000001, 'admin', '2023-11-21 17:29:52', 1);
INSERT INTO `gen_table_column` VALUES (1482, '151', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1483, '151', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1484, '151', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1485, '151', 'picture_1', '图片1', 'varchar(255)', 'String', 'picture1', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'image', NULL, 4, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1486, '151', 'picture_2', '图片2', 'varchar(255)', 'String', 'picture2', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'image', NULL, 5, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:16', 1);
INSERT INTO `gen_table_column` VALUES (1487, '151', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:16', 1);
INSERT INTO `gen_table_column` VALUES (1488, '151', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:16', 1);
INSERT INTO `gen_table_column` VALUES (1489, '151', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:16', 1);
INSERT INTO `gen_table_column` VALUES (1490, '151', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:16', 1);
INSERT INTO `gen_table_column` VALUES (1491, '151', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:16', 1);
INSERT INTO `gen_table_column` VALUES (1492, '151', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:16', 1);
INSERT INTO `gen_table_column` VALUES (1493, '151', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2023-11-21 17:46:51', 1000000000000000001, 'admin', '2023-11-21 17:47:17', 1);
INSERT INTO `gen_table_column` VALUES (1494, '152', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:49', 1);
INSERT INTO `gen_table_column` VALUES (1495, '152', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1496, '152', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1497, '152', 'head_portrait', '头像', 'varchar(255)', 'String', 'headPortrait', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'image', NULL, 4, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1498, '152', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'image', NULL, 5, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1499, '152', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1500, '152', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1501, '152', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1502, '152', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1503, '152', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1504, '152', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1505, '152', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2023-11-21 18:03:16', 1000000000000000001, 'admin', '2023-11-21 18:03:50', 1);
INSERT INTO `gen_table_column` VALUES (1506, '153', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:45', 1);
INSERT INTO `gen_table_column` VALUES (1507, '153', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:45', 1);
INSERT INTO `gen_table_column` VALUES (1508, '153', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', '1', NULL, 'image', NULL, 3, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:45', 1);
INSERT INTO `gen_table_column` VALUES (1509, '153', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:45', 1);
INSERT INTO `gen_table_column` VALUES (1510, '153', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:45', 1);
INSERT INTO `gen_table_column` VALUES (1511, '153', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:46', 1);
INSERT INTO `gen_table_column` VALUES (1512, '153', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:46', 1);
INSERT INTO `gen_table_column` VALUES (1513, '153', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:46', 1);
INSERT INTO `gen_table_column` VALUES (1514, '153', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:46', 1);
INSERT INTO `gen_table_column` VALUES (1515, '153', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:46', 1);
INSERT INTO `gen_table_column` VALUES (1516, '153', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-23 14:09:30', 1000000000000000001, 'admin', '2023-11-23 14:10:46', 1);
INSERT INTO `gen_table_column` VALUES (1517, '154', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:20', 0);
INSERT INTO `gen_table_column` VALUES (1518, '154', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1519, '154', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', '1', NULL, 'image', NULL, 3, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1520, '154', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'switch', NULL, 4, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1521, '154', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1522, '154', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1523, '154', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1524, '154', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1525, '154', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1526, '154', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:21', 0);
INSERT INTO `gen_table_column` VALUES (1527, '154', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-25 11:29:45', 1000000000000000001, 'admin', '2023-11-25 11:30:22', 0);
INSERT INTO `gen_table_column` VALUES (1528, '155', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1529, '155', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1530, '155', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1531, '155', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1532, '155', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1533, '155', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1534, '155', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1535, '155', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:08', 0);
INSERT INTO `gen_table_column` VALUES (1536, '155', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:09', 0);
INSERT INTO `gen_table_column` VALUES (1537, '155', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:09', 0);
INSERT INTO `gen_table_column` VALUES (1538, '155', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-27 18:23:17', 1000000000000000001, 'admin', '2024-01-09 13:57:09', 0);
INSERT INTO `gen_table_column` VALUES (1539, '156', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1540, '156', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1541, '156', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', NULL, 'textarea', NULL, 3, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1542, '156', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1543, '156', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1544, '156', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1545, '156', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1546, '156', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1547, '156', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1548, '156', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 09:47:47', 1000000000000000001, 'admin', '2023-12-06 09:48:10', 1);
INSERT INTO `gen_table_column` VALUES (1549, '157', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:18', 1);
INSERT INTO `gen_table_column` VALUES (1550, '157', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:18', 1);
INSERT INTO `gen_table_column` VALUES (1551, '157', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_notify_type', 3, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:18', 1);
INSERT INTO `gen_table_column` VALUES (1552, '157', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:19', 1);
INSERT INTO `gen_table_column` VALUES (1553, '157', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:19', 1);
INSERT INTO `gen_table_column` VALUES (1554, '157', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:19', 1);
INSERT INTO `gen_table_column` VALUES (1555, '157', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', '1', 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:19', 1);
INSERT INTO `gen_table_column` VALUES (1556, '157', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:19', 1);
INSERT INTO `gen_table_column` VALUES (1557, '157', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:19', 1);
INSERT INTO `gen_table_column` VALUES (1558, '157', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', '1', 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:19', 1);
INSERT INTO `gen_table_column` VALUES (1559, '157', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 09:55:42', 1000000000000000001, 'admin', '2023-12-06 09:57:20', 1);
INSERT INTO `gen_table_column` VALUES (1560, '158', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:14', 1);
INSERT INTO `gen_table_column` VALUES (1561, '158', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:14', 1);
INSERT INTO `gen_table_column` VALUES (1562, '158', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_notify_type', 3, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:14', 1);
INSERT INTO `gen_table_column` VALUES (1563, '158', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:14', 1);
INSERT INTO `gen_table_column` VALUES (1564, '158', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, NULL, '1', '1', NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:14', 1);
INSERT INTO `gen_table_column` VALUES (1565, '158', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:14', 1);
INSERT INTO `gen_table_column` VALUES (1566, '158', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:14', 1);
INSERT INTO `gen_table_column` VALUES (1567, '158', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', '1', 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1568, '158', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1569, '158', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1570, '158', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1571, '158', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 10:45:28', 1000000000000000001, 'admin', '2023-12-06 10:47:15', 1);
INSERT INTO `gen_table_column` VALUES (1572, '159', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1573, '159', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1574, '159', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', NULL, 3, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1575, '159', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1576, '159', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, NULL, '1', '1', NULL, 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1577, '159', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1578, '159', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1579, '159', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1580, '159', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1581, '159', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1582, '159', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1583, '159', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 10:56:32', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1584, '160', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1585, '160', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1586, '160', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 3, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1587, '160', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1588, '160', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, NULL, '1', '1', NULL, 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1589, '160', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1590, '160', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1591, '160', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1592, '160', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1593, '160', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1594, '160', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1595, '160', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 10:59:50', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1596, '161', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1597, '161', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1598, '161', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_notify_type', 3, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1599, '161', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1600, '161', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, NULL, '1', '1', NULL, 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1601, '161', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1602, '161', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1603, '161', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:29', 1);
INSERT INTO `gen_table_column` VALUES (1604, '161', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:30', 1);
INSERT INTO `gen_table_column` VALUES (1605, '161', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:30', 1);
INSERT INTO `gen_table_column` VALUES (1606, '161', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:30', 1);
INSERT INTO `gen_table_column` VALUES (1607, '161', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 11:04:39', 1000000000000000001, 'admin', '2023-12-06 11:05:30', 1);
INSERT INTO `gen_table_column` VALUES (1608, '162', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1609, '162', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1610, '162', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', NULL, 3, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1611, '162', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1612, '162', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, NULL, '1', '1', NULL, 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1613, '162', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1614, '162', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1615, '162', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:22', 1);
INSERT INTO `gen_table_column` VALUES (1616, '162', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:23', 1);
INSERT INTO `gen_table_column` VALUES (1617, '162', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:23', 1);
INSERT INTO `gen_table_column` VALUES (1618, '162', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:23', 1);
INSERT INTO `gen_table_column` VALUES (1619, '162', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 11:15:07', 1000000000000000001, 'admin', '2023-12-06 11:15:23', 1);
INSERT INTO `gen_table_column` VALUES (1620, '163', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:38', 1);
INSERT INTO `gen_table_column` VALUES (1621, '163', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:38', 1);
INSERT INTO `gen_table_column` VALUES (1622, '163', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_notify_type', 3, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:38', 1);
INSERT INTO `gen_table_column` VALUES (1623, '163', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:38', 1);
INSERT INTO `gen_table_column` VALUES (1624, '163', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, NULL, '1', '1', NULL, 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1625, '163', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1626, '163', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1627, '163', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1628, '163', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1629, '163', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1630, '163', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1631, '163', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 11:19:23', 1000000000000000001, 'admin', '2023-12-06 11:19:39', 1);
INSERT INTO `gen_table_column` VALUES (1632, '164', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1633, '164', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1634, '164', 'type', '类型', 'tinyint', 'Integer', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_notify_type', 3, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1635, '164', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1636, '164', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1637, '164', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1638, '164', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1639, '164', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:36', 0);
INSERT INTO `gen_table_column` VALUES (1640, '164', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:37', 0);
INSERT INTO `gen_table_column` VALUES (1641, '164', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:37', 0);
INSERT INTO `gen_table_column` VALUES (1642, '164', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:37', 0);
INSERT INTO `gen_table_column` VALUES (1643, '164', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 11:21:02', 1000000000000000001, 'admin', '2023-12-06 11:21:37', 0);
INSERT INTO `gen_table_column` VALUES (1644, '165', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:16', 0);
INSERT INTO `gen_table_column` VALUES (1645, '165', 'sender', '发送方', 'bigint', 'Long', 'sender', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:16', 0);
INSERT INTO `gen_table_column` VALUES (1646, '165', 'receiver', '接收方', 'bigint', 'Long', 'receiver', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1647, '165', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1648, '165', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1649, '165', 'status', '消息状态', 'tinyint', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'msg_read_type', 6, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1650, '165', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1651, '165', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1652, '165', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1653, '165', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 10, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1654, '165', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 11, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:17', 0);
INSERT INTO `gen_table_column` VALUES (1655, '165', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:18', 0);
INSERT INTO `gen_table_column` VALUES (1656, '165', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 13, 1000000000000000001, 'admin', '2023-12-06 14:46:52', 1000000000000000001, 'admin', '2023-12-06 14:48:18', 0);
INSERT INTO `gen_table_column` VALUES (1657, '166', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:34', 0);
INSERT INTO `gen_table_column` VALUES (1658, '166', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:34', 0);
INSERT INTO `gen_table_column` VALUES (1659, '166', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'image', NULL, 3, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:34', 0);
INSERT INTO `gen_table_column` VALUES (1660, '166', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:34', 0);
INSERT INTO `gen_table_column` VALUES (1661, '166', 'state', '状态', 'tinyint', 'Integer', 'state', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'switch', NULL, 5, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1662, '166', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1663, '166', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1664, '166', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1665, '166', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1666, '166', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1667, '166', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1668, '166', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-18 21:19:01', 1000000000000000001, 'admin', '2023-12-18 21:19:35', 0);
INSERT INTO `gen_table_column` VALUES (1669, '167', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-19 14:53:03', 1000000000000000001, 'admin', '2023-12-19 15:14:07', 1);
INSERT INTO `gen_table_column` VALUES (1670, '167', 'name_commodity', '商品名称', 'varchar(255)', 'String', 'nameCommodity', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-19 14:53:03', 1000000000000000001, 'admin', '2023-12-19 15:14:07', 1);
INSERT INTO `gen_table_column` VALUES (1671, '167', 'price', '价格', 'varchar(255)', 'String', 'price', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2023-12-19 14:53:03', 1000000000000000001, 'admin', '2023-12-19 15:14:07', 1);
INSERT INTO `gen_table_column` VALUES (1672, '168', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-19 15:30:02', 1000000000000000001, 'admin', '2023-12-19 15:30:29', 1);
INSERT INTO `gen_table_column` VALUES (1673, '168', 'name_commodity', '商品名称', 'varchar(255)', 'String', 'nameCommodity', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-19 15:30:02', 1000000000000000001, 'admin', '2023-12-19 15:30:29', 1);
INSERT INTO `gen_table_column` VALUES (1674, '168', 'price', '价格', 'varchar(255)', 'String', 'price', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2023-12-19 15:30:02', 1000000000000000001, 'admin', '2023-12-19 15:30:29', 1);
INSERT INTO `gen_table_column` VALUES (1675, '169', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:32', 1);
INSERT INTO `gen_table_column` VALUES (1676, '169', 'state', '状态', 'varchar(255)', 'String', 'state', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:32', 1);
INSERT INTO `gen_table_column` VALUES (1677, '169', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:32', 1);
INSERT INTO `gen_table_column` VALUES (1678, '169', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 4, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:32', 1);
INSERT INTO `gen_table_column` VALUES (1679, '169', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:33', 1);
INSERT INTO `gen_table_column` VALUES (1680, '169', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:33', 1);
INSERT INTO `gen_table_column` VALUES (1681, '169', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:33', 1);
INSERT INTO `gen_table_column` VALUES (1682, '169', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:33', 1);
INSERT INTO `gen_table_column` VALUES (1683, '169', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-19 15:37:41', 1000000000000000001, 'admin', '2023-12-19 15:45:33', 1);
INSERT INTO `gen_table_column` VALUES (1684, '170', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:42', 1);
INSERT INTO `gen_table_column` VALUES (1685, '170', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:42', 1);
INSERT INTO `gen_table_column` VALUES (1686, '170', 'name', 'name', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:42', 1);
INSERT INTO `gen_table_column` VALUES (1687, '170', 'age', 'age', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1688, '170', 'weather', '天气', 'varchar(255)', 'String', 'weather', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1689, '170', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1690, '170', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1691, '170', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1692, '170', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1693, '170', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1694, '170', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1695, '170', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2023-12-21 21:59:53', 1000000000000000001, 'admin', '2023-12-21 22:01:43', 1);
INSERT INTO `gen_table_column` VALUES (1696, '171', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1697, '171', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1698, '171', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1699, '171', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1700, '171', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1701, '171', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1702, '171', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1703, '171', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1704, '171', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1705, '171', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1706, '171', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 11, 1000000000000000001, 'admin', '2023-12-21 22:15:04', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1707, '172', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-12-22 16:21:44', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1708, '172', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-12-22 16:21:44', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1709, '172', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2023-12-22 16:21:44', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1710, '172', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2023-12-22 16:21:44', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1711, '173', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1712, '173', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1713, '173', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1714, '173', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1715, '173', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', NULL, NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1716, '173', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1717, '173', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1718, '173', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', NULL, NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1719, '173', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1720, '173', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 10, 1000000000000000001, 'admin', '2024-01-15 21:12:17', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1721, '174', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', 1);
INSERT INTO `gen_table_column` VALUES (1722, '174', 'student\'s_name', '姓名', 'varchar(255)', 'String', 'student\'sName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', 1);
INSERT INTO `gen_table_column` VALUES (1723, '174', 'students\'_gender', '性别', 'int', 'Integer', 'students\'Gender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', 1);
INSERT INTO `gen_table_column` VALUES (1724, '174', 'student_age', '年龄', 'int', 'Integer', 'studentAge', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', 1);
INSERT INTO `gen_table_column` VALUES (1725, '174', 'interests_and_hobbies', '兴趣爱好', 'varchar(255)', 'String', 'interestsAndHobbies', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', 1);
INSERT INTO `gen_table_column` VALUES (1726, '174', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', 1);
INSERT INTO `gen_table_column` VALUES (1727, '174', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:23', 1);
INSERT INTO `gen_table_column` VALUES (1728, '174', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:24', 1);
INSERT INTO `gen_table_column` VALUES (1729, '174', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:24', 1);
INSERT INTO `gen_table_column` VALUES (1730, '174', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:24', 1);
INSERT INTO `gen_table_column` VALUES (1731, '174', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:24', 1);
INSERT INTO `gen_table_column` VALUES (1732, '174', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2024-01-15 21:25:47', 1000000000000000001, 'admin', '2024-01-15 21:44:24', 1);
INSERT INTO `gen_table_column` VALUES (1733, '175', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1734, '175', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1735, '175', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1736, '175', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1737, '175', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1738, '175', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1739, '175', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1740, '175', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1741, '175', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1742, '175', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 10, 1000000000000000001, 'admin', '2024-01-15 21:37:29', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1743, '176', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1744, '176', 'name', '姓名', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1745, '176', 'gender', '性别', 'int', 'Integer', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1746, '176', 'age', '年龄', 'int', 'Integer', 'age', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 4, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1747, '176', 'hobby', '爱好', 'varchar(255)', 'String', 'hobby', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1748, '176', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1749, '176', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1750, '176', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1751, '176', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1752, '176', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1753, '176', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1754, '176', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2024-01-15 21:47:38', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1755, '177', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1756, '177', 'serial_number', '编号', 'int', 'Integer', 'serialNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1757, '177', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1758, '177', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1759, '177', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1760, '177', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1761, '177', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1762, '177', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 8, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1763, '177', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1764, '177', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 10, 1000000000000000001, 'admin', '2024-01-16 23:54:54', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1765, '178', 'id', '主键ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1766, '178', 'type', '请假类型', 'char(20)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'select', NULL, 2, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1767, '178', 'title', '标题', 'varchar(100)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1768, '178', 'reason', '原因', 'varchar(500)', 'String', 'reason', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 4, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1769, '178', 'start_time', '开始时间', 'datetime', 'LocalDateTime', 'startTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1770, '178', 'end_time', '结束时间', 'datetime', 'LocalDateTime', 'endTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1771, '178', 'total_time', '请假时长，单位秒', 'bigint', 'Long', 'totalTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1772, '178', 'apply_user', '申请人', 'varchar(64)', 'String', 'applyUser', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1773, '178', 'apply_time', '申请时间', 'datetime', 'LocalDateTime', 'applyTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1774, '178', 'reality_start_time', '实际开始时间', 'datetime', 'LocalDateTime', 'realityStartTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1775, '178', 'reality_end_time', '实际结束时间', 'datetime', 'LocalDateTime', 'realityEndTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 11, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1776, '178', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1777, '178', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 13, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1778, '178', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 14, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1779, '178', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 15, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1780, '178', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 16, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1781, '178', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 17, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1782, '178', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 18, 1000000000000000001, 'admin', '2024-01-25 13:42:06', NULL, NULL, NULL, 1);
INSERT INTO `gen_table_column` VALUES (1783, '179', 'id', '主键ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1784, '179', 'type', '请假类型', 'char(20)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'select', 'leave_type', 2, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1785, '179', 'title', '标题', 'varchar(100)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1786, '179', 'reason', '原因', 'varchar(500)', 'String', 'reason', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 4, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1787, '179', 'start_time', '开始时间', 'datetime', 'LocalDateTime', 'startTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 5, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1788, '179', 'end_time', '结束时间', 'datetime', 'LocalDateTime', 'endTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1789, '179', 'total_time', '请假时长', 'bigint', 'Long', 'totalTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1790, '179', 'apply_user', '申请人', 'varchar(64)', 'String', 'applyUser', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1791, '179', 'apply_time', '申请时间', 'datetime', 'LocalDateTime', 'applyTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:44', 0);
INSERT INTO `gen_table_column` VALUES (1792, '179', 'reality_start_time', '实际开始时间', 'datetime', 'LocalDateTime', 'realityStartTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1793, '179', 'reality_end_time', '实际结束时间', 'datetime', 'LocalDateTime', 'realityEndTime', '0', '0', NULL, '1', '1', '1', '1', NULL, 'datetime', NULL, 11, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1794, '179', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1795, '179', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 13, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1796, '179', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 14, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1797, '179', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 15, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1798, '179', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 16, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1799, '179', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 17, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1800, '179', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 18, 1000000000000000001, 'admin', '2024-01-25 13:44:39', 1000000000000000001, 'admin', '2024-01-25 13:47:45', 0);
INSERT INTO `gen_table_column` VALUES (1801, '180', 'id', '主键', 'bigint unsigned', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1802, '180', 'flow_key', '流程key', 'varchar(50)', 'String', 'flowKey', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1803, '180', 'node_id', '流程节点ID', 'varchar(50)', 'String', 'nodeId', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1804, '180', 'assignee', '关联ID', 'varchar(500)', 'String', 'assignee', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'textarea', NULL, 4, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1805, '180', 'assignee_type', '类别', 'tinyint', 'Integer', 'assigneeType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'approve_type', 5, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1806, '180', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1807, '180', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 7, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1808, '180', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1809, '180', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 9, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:47', 0);
INSERT INTO `gen_table_column` VALUES (1810, '180', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 10, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:48', 0);
INSERT INTO `gen_table_column` VALUES (1811, '180', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:48', 0);
INSERT INTO `gen_table_column` VALUES (1812, '180', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 12, 1000000000000000001, 'admin', '2024-01-25 17:21:49', 1000000000000000001, 'admin', '2024-01-25 17:24:48', 0);
INSERT INTO `gen_table_column` VALUES (1813, '181', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1814, '181', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1815, '181', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1816, '181', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1817, '181', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1818, '181', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1819, '181', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1820, '181', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1821, '181', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1822, '181', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1823, '181', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 11, 1000000000000000001, 'admin', '2024-01-27 00:53:32', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1824, '182', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1825, '182', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1826, '182', 'age', '年龄', 'varchar(255)', 'String', 'age', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1827, '182', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1828, '182', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1829, '182', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 6, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1830, '182', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1831, '182', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1832, '182', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1833, '182', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1834, '182', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 11, 1000000000000000001, 'admin', '2024-01-27 00:55:53', NULL, NULL, NULL, 0);
INSERT INTO `gen_table_column` VALUES (1835, '183', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', 1);
INSERT INTO `gen_table_column` VALUES (1836, '183', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', 1);
INSERT INTO `gen_table_column` VALUES (1837, '183', 'it_said', '呢称', 'varchar(255)', 'String', 'itSaid', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', 1);
INSERT INTO `gen_table_column` VALUES (1838, '183', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', 1);
INSERT INTO `gen_table_column` VALUES (1839, '183', 'mobile_phone_no.', '手机号', 'varchar(255)', 'String', 'mobilePhoneNo.', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', 1);
INSERT INTO `gen_table_column` VALUES (1840, '183', 'password', '密码', 'varchar(255)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', 1);
INSERT INTO `gen_table_column` VALUES (1841, '183', 'email', '邮箱', 'varchar(255)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:04', 1);
INSERT INTO `gen_table_column` VALUES (1842, '183', 'address', '地址', 'varchar(255)', 'String', 'address', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1843, '183', 'whether_for_the_players', '是否为球员', 'varchar(255)', 'String', 'whetherForThePlayers', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 9, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1844, '183', 'whether_for_coach', '是否为教练', 'varchar(255)', 'String', 'whetherForCoach', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1845, '183', 'whether_as_an_administrator', '是否为管理员', 'varchar(255)', 'String', 'whetherAsAnAdministrator', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 11, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1846, '183', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1847, '183', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 13, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1848, '183', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 14, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1849, '183', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 15, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1850, '183', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 16, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1851, '183', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 17, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1852, '183', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 18, 1000000000000000001, 'admin', '2024-02-05 04:09:22', 1000000000000000001, 'admin', '2024-02-05 04:12:05', 1);
INSERT INTO `gen_table_column` VALUES (1853, '184', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1854, '184', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1855, '184', 'it_said', '呢称', 'varchar(255)', 'String', 'itSaid', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1856, '184', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'select', 'user_sex', 4, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1857, '184', 'mobile_phone_no.', '手机号', 'varchar(255)', 'String', 'mobilePhoneNo.', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1858, '184', 'password', '密码', 'varchar(255)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1859, '184', 'email', '邮箱', 'varchar(255)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1860, '184', 'address', '地址', 'varchar(255)', 'String', 'address', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1861, '184', 'whether_for_the_players', '是否为球员', 'varchar(255)', 'String', 'whetherForThePlayers', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'switch', 'sys_status', 9, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1862, '184', 'whether_for_coach', '是否为教练', 'varchar(255)', 'String', 'whetherForCoach', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'switch', 'sys_status', 10, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:11', 1);
INSERT INTO `gen_table_column` VALUES (1863, '184', 'whether_as_an_administrator', '是否为管理员', 'varchar(255)', 'String', 'whetherAsAnAdministrator', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'switch', 'sys_status', 11, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1864, '184', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1865, '184', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 13, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1866, '184', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 14, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1867, '184', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 15, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1868, '184', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 16, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1869, '184', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 17, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1870, '184', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 18, 1000000000000000001, 'admin', '2024-02-05 04:24:00', 1000000000000000001, 'admin', '2024-02-05 04:34:12', 1);
INSERT INTO `gen_table_column` VALUES (1871, '185', 'id', 'ID', 'bigint', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:56', 0);
INSERT INTO `gen_table_column` VALUES (1872, '185', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:56', 0);
INSERT INTO `gen_table_column` VALUES (1873, '185', 'it_said', '呢称', 'varchar(255)', 'String', 'itSaid', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 3, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:56', 0);
INSERT INTO `gen_table_column` VALUES (1874, '185', 'gender', '性别', 'varchar(255)', 'String', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'select', 'user_sex', 4, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:56', 0);
INSERT INTO `gen_table_column` VALUES (1875, '185', 'mobile_phone_no', '手机号', 'varchar(255)', 'String', 'mobilePhoneNo', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 5, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:56', 0);
INSERT INTO `gen_table_column` VALUES (1876, '185', 'password', '密码', 'varchar(255)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 6, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1877, '185', 'email', '邮箱', 'varchar(255)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 7, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1878, '185', 'address', '地址', 'varchar(255)', 'String', 'address', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 8, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1879, '185', 'whether_for_the_players', '是否为球员', 'varchar(255)', 'String', 'whetherForThePlayers', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'switch', NULL, 9, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1880, '185', 'whether_for_coach', '是否为教练', 'varchar(255)', 'String', 'whetherForCoach', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'switch', NULL, 10, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1881, '185', 'whether_as_an_administrator', '是否为管理员', 'varchar(255)', 'String', 'whetherAsAnAdministrator', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'switch', NULL, 11, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1882, '185', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 12, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1883, '185', 'create_time', '创建时间', 'datetime', 'LocalDateTime', 'createTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 13, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1884, '185', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 14, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1885, '185', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 15, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1886, '185', 'update_time', '修改时间', 'datetime', 'LocalDateTime', 'updateTime', '0', '0', '1', NULL, NULL, '1', NULL, NULL, 'datetime', NULL, 16, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1887, '185', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, '1', NULL, 'LIKE', 'input', NULL, 17, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:57', 0);
INSERT INTO `gen_table_column` VALUES (1888, '185', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, 18, 1000000000000000001, 'admin', '2024-02-05 04:49:01', 1000000000000000001, 'admin', '2024-02-05 04:49:58', 0);

-- ----------------------------
-- Table structure for process_node_assignee
-- ----------------------------
DROP TABLE IF EXISTS `process_node_assignee`;
CREATE TABLE `process_node_assignee`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flow_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '流程key',
  `node_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '流程节点ID',
  `assignee` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '审批人',
  `assignee_variable` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审批人变量',
  `assignee_type` tinyint NULL DEFAULT 1 COMMENT '处理人类别：1-用户 2-角色',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 413 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程审批节点人员关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of process_node_assignee
-- ----------------------------
INSERT INTO `process_node_assignee` VALUES (408, 'leave_test', 'act_01', '1167805277462855680', 'personnelVerify', 1, 1000000000000000001, '2024-01-25 17:45:40', 'admin', 1170050787619897344, '2024-01-30 17:33:14', 'lisi', 0);
INSERT INTO `process_node_assignee` VALUES (409, 'leave_test', 'act_02', '1000000000000000001', 'leaderVerify', 1, 1000000000000000001, '2024-01-25 17:46:03', 'admin', 1170050787619897344, '2024-01-30 17:33:19', 'lisi', 0);
INSERT INTO `process_node_assignee` VALUES (410, 'test_leave_01', 'act_01', '1199019160365957122', 'assignee', 1, 1000000000000000001, '2024-02-02 18:26:12', 'admin', NULL, '2024-02-02 18:26:12', NULL, 0);
INSERT INTO `process_node_assignee` VALUES (411, 'test_leave_01', 'act_02', '1199019160365957123', 'assignee', 1, 1000000000000000001, '2024-02-02 18:27:00', 'admin', NULL, '2024-02-02 18:27:01', NULL, 0);
INSERT INTO `process_node_assignee` VALUES (412, 'test_leave_01', 'act_03', '1167805277462855680', 'assignee', 1, 1000000000000000001, '2024-02-02 18:27:53', 'admin', NULL, '2024-02-02 18:27:53', NULL, 0);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `field_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'field_key',
  `field_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'field_value',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '系统名称', 'sys_name', 'Easy-Admin', 1000000000000000001, '2023-11-20 22:18:58', 'admin', 1000000000000000001, '2023-11-29 16:22:00', 'admin', 1);
INSERT INTO `sys_config` VALUES (2, 'Copyright', 'sys_copyright', '源码字节', 1000000000000000001, '2023-11-20 22:19:58', 'admin', NULL, '2023-11-20 22:19:58', NULL, 0);
INSERT INTO `sys_config` VALUES (3, '图片允许上传数量', 'sys_upload_num', '5', 1000000000000000001, '2023-11-20 22:36:30', 'admin', NULL, '2023-11-20 22:36:30', NULL, 0);
INSERT INTO `sys_config` VALUES (4, '项目slogan', 'project_slogan', '中小企业快速开发脚手架', 1000000000000000001, '2023-11-27 16:42:38', 'admin', 1000000000000000001, '2023-12-27 11:16:58', 'admin', 0);
INSERT INTO `sys_config` VALUES (5, '系统名称', 'sys_name', 'Easy-Admin', 1000000000000000001, '2023-11-29 16:22:19', 'admin', 1000000000000000001, '2024-01-22 10:05:42', 'admin', 0);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '编码',
  `dict_type_id` bigint NULL DEFAULT NULL COMMENT '字典类型ID',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字典类型',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典键值',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 122 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (109, 4, 'sys_status', 3, '启用', '0', 1000000000000000001, '2023-11-19 14:12:47', 'admin', 1000000000000000001, '2023-12-24 12:46:02', 'admin', 0);
INSERT INTO `sys_dict_data` VALUES (110, 4, 'sys_status', 2, '禁用', '1', 1000000000000000001, '2023-11-19 14:13:02', 'admin', NULL, '2023-11-19 14:13:01', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (111, 5, 'sys_notify_type', 1, '公告', '1', 1000000000000000001, '2023-12-06 09:56:21', 'admin', NULL, '2023-12-06 09:56:21', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (112, 5, 'sys_notify_type', 2, '通知', '2', 1000000000000000001, '2023-12-06 09:56:43', 'admin', NULL, '2023-12-06 09:56:43', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (113, 11, 'msg_read_type', 1, '未读', '0', 1000000000000000001, '2023-12-06 14:45:59', 'admin', NULL, '2023-12-06 14:46:00', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (114, 11, 'msg_read_type', 2, '已读', '1', 1000000000000000001, '2023-12-06 14:46:27', 'admin', NULL, '2023-12-06 14:46:27', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (115, 12, 'leave_type', 2, '普通', '1', 1000000000000000001, '2024-01-25 13:46:29', 'admin', NULL, '2024-01-25 13:46:30', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (116, 12, 'leave_type', 2, '严重', '2', 1000000000000000001, '2024-01-25 13:46:46', 'admin', NULL, '2024-01-25 13:46:47', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (117, 13, 'approve_type', 2, '用户', '1', 1000000000000000001, '2024-01-25 17:24:17', 'admin', NULL, '2024-01-25 17:24:16', NULL, 0);
INSERT INTO `sys_dict_data` VALUES (118, 13, 'approve_type', 2, '角色', '2', 1000000000000000001, '2024-01-25 17:24:30', 'admin', NULL, '2024-01-25 17:24:30', NULL, 0);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型',
  `state` tinyint NULL DEFAULT 0 COMMENT '状态 0启动 1关闭',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典类型' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (4, '系统状态', 'sys_status', 0, 1000000000000000001, '2023-11-19 14:12:26', 'admin', 1000000000000000001, '2023-11-24 16:02:25', 'admin', 0);
INSERT INTO `sys_dict_type` VALUES (5, '通知类型', 'sys_notify_type', 0, 1000000000000000001, '2023-12-06 09:56:01', 'admin', NULL, '2023-12-06 09:56:01', NULL, 0);
INSERT INTO `sys_dict_type` VALUES (6, '消息阅读类型', 'message_read_type', 0, 1000000000000000001, '2023-12-06 14:43:20', 'admin', NULL, '2023-12-06 14:43:59', NULL, 1);
INSERT INTO `sys_dict_type` VALUES (7, '消息阅读类型', 'message_read_type', 0, 1000000000000000001, '2023-12-06 14:43:20', 'admin', NULL, '2023-12-06 14:44:02', NULL, 1);
INSERT INTO `sys_dict_type` VALUES (8, '消息阅读类型', 'message_read_type', 0, 1000000000000000001, '2023-12-06 14:43:20', 'admin', NULL, '2023-12-06 14:44:05', NULL, 1);
INSERT INTO `sys_dict_type` VALUES (9, '消息阅读类型', 'message_read_type', 0, 1000000000000000001, '2023-12-06 14:43:20', 'admin', NULL, '2023-12-06 14:44:12', NULL, 1);
INSERT INTO `sys_dict_type` VALUES (10, '消息阅读类型', 'message_read_type', 0, 1000000000000000001, '2023-12-06 14:43:20', 'admin', NULL, '2023-12-06 14:44:15', NULL, 1);
INSERT INTO `sys_dict_type` VALUES (11, '消息阅读类型', 'msg_read_type', 0, 1000000000000000001, '2023-12-06 14:43:53', 'admin', NULL, '2023-12-06 14:43:53', NULL, 0);
INSERT INTO `sys_dict_type` VALUES (12, '请假类型', 'leave_type', 0, 1000000000000000001, '2024-01-25 13:46:12', 'admin', NULL, '2024-01-25 13:46:13', NULL, 0);
INSERT INTO `sys_dict_type` VALUES (13, '审批类别', 'approve_type', 0, 1000000000000000001, '2024-01-25 17:24:04', 'admin', NULL, '2024-01-25 17:24:04', NULL, 0);

-- ----------------------------
-- Table structure for sys_login_record
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_record`;
CREATE TABLE `sys_login_record`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户账号',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户ID',
  `ipaddr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1369 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_login_record
-- ----------------------------
INSERT INTO `sys_login_record` VALUES (1317, 'admin', 1000000000000000001, '117.136.63.189', '四川省', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-05 10:57:35');
INSERT INTO `sys_login_record` VALUES (1318, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-06 10:59:51');
INSERT INTO `sys_login_record` VALUES (1319, 'admin', NULL, '182.139.102.138', '四川省遂宁市射洪市', 'Chrome 12', 'Windows 10', '0', '验证码已过期', '2024-02-09 22:36:26');
INSERT INTO `sys_login_record` VALUES (1320, 'admin', 1000000000000000001, '182.139.102.138', '四川省遂宁市射洪市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-09 22:36:33');
INSERT INTO `sys_login_record` VALUES (1321, 'admin', 1000000000000000001, '182.139.102.138', '四川省遂宁市射洪市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-10 17:45:10');
INSERT INTO `sys_login_record` VALUES (1322, 'admin', 1000000000000000001, '182.139.102.138', '四川省遂宁市射洪市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-10 17:49:54');
INSERT INTO `sys_login_record` VALUES (1323, 'admin', 1000000000000000001, '182.139.102.138', '四川省遂宁市射洪市', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-10 17:51:21');
INSERT INTO `sys_login_record` VALUES (1324, 'admin', 1000000000000000001, '182.139.102.138', '四川省遂宁市射洪市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-10 17:51:24');
INSERT INTO `sys_login_record` VALUES (1325, 'admin', NULL, '112.4.237.179', '江苏省连云港市连云区', 'Chrome 12', 'Windows 10', '0', '密码错误', '2024-02-18 13:41:40');
INSERT INTO `sys_login_record` VALUES (1326, 'admin', 1000000000000000001, '112.4.237.179', '江苏省连云港市连云区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-18 13:41:48');
INSERT INTO `sys_login_record` VALUES (1327, 'admin', 1000000000000000001, '117.174.18.121', '四川省成都市双流区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-18 22:11:42');
INSERT INTO `sys_login_record` VALUES (1328, 'admin', 1000000000000000001, '115.60.185.76', '河南省郑州市中原区', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-02-19 16:38:33');
INSERT INTO `sys_login_record` VALUES (1329, 'admin', 1000000000000000001, '115.60.187.144', '河南省郑州市中原区', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-02-20 17:22:11');
INSERT INTO `sys_login_record` VALUES (1330, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-21 09:16:34');
INSERT INTO `sys_login_record` VALUES (1331, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-21 10:37:12');
INSERT INTO `sys_login_record` VALUES (1332, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-22 17:53:12');
INSERT INTO `sys_login_record` VALUES (1333, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-26 10:54:31');
INSERT INTO `sys_login_record` VALUES (1334, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-26 10:57:36');
INSERT INTO `sys_login_record` VALUES (1335, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-26 14:23:27');
INSERT INTO `sys_login_record` VALUES (1336, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-26 14:51:10');
INSERT INTO `sys_login_record` VALUES (1337, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-26 14:53:51');
INSERT INTO `sys_login_record` VALUES (1338, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-26 14:57:34');
INSERT INTO `sys_login_record` VALUES (1339, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-26 14:59:43');
INSERT INTO `sys_login_record` VALUES (1340, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-26 15:00:00');
INSERT INTO `sys_login_record` VALUES (1341, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-26 15:18:58');
INSERT INTO `sys_login_record` VALUES (1342, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-26 15:19:01');
INSERT INTO `sys_login_record` VALUES (1343, 'admin', 1000000000000000001, '223.104.220.221', '四川省成都市', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-26 17:54:32');
INSERT INTO `sys_login_record` VALUES (1344, 'admin', 1000000000000000001, '223.104.9.192', '四川省', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-27 13:31:07');
INSERT INTO `sys_login_record` VALUES (1345, 'admin', 1000000000000000001, '223.104.9.192', '四川省', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-27 13:57:05');
INSERT INTO `sys_login_record` VALUES (1346, 'admin', 1000000000000000001, '223.104.9.192', '四川省', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-27 13:57:08');
INSERT INTO `sys_login_record` VALUES (1347, 'admin', 1000000000000000001, '223.104.9.192', '四川省', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-27 13:57:30');
INSERT INTO `sys_login_record` VALUES (1348, 'admin', 1000000000000000001, '223.104.9.192', '四川省', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-27 18:10:18');
INSERT INTO `sys_login_record` VALUES (1349, 'admin', 1000000000000000001, '117.174.18.10', '四川省成都市双流区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-27 23:12:48');
INSERT INTO `sys_login_record` VALUES (1350, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-28 10:21:33');
INSERT INTO `sys_login_record` VALUES (1351, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-28 14:32:46');
INSERT INTO `sys_login_record` VALUES (1352, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-28 14:33:27');
INSERT INTO `sys_login_record` VALUES (1353, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-28 14:33:32');
INSERT INTO `sys_login_record` VALUES (1354, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-02-28 14:33:35');
INSERT INTO `sys_login_record` VALUES (1355, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-28 14:38:24');
INSERT INTO `sys_login_record` VALUES (1356, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-02-29 14:05:02');
INSERT INTO `sys_login_record` VALUES (1357, 'admin', 1000000000000000001, '223.104.214.205', '四川省', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-03-04 16:34:58');
INSERT INTO `sys_login_record` VALUES (1358, 'admin', 1000000000000000001, '223.104.214.205', '四川省', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-04 16:35:03');
INSERT INTO `sys_login_record` VALUES (1359, 'admin', 1000000000000000001, '223.104.214.205', '四川省', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-03-04 16:35:08');
INSERT INTO `sys_login_record` VALUES (1360, 'admin', 1000000000000000001, '223.104.214.205', '四川省', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-04 16:47:49');
INSERT INTO `sys_login_record` VALUES (1361, 'admin', 1000000000000000001, '223.104.214.205', '四川省', 'Chrome 12', 'Windows 10', '0', '退出登录', '2024-03-04 16:48:42');
INSERT INTO `sys_login_record` VALUES (1362, 'admin', NULL, '223.104.214.205', '四川省', 'Chrome 12', 'Windows 10', '0', '验证码有误', '2024-03-04 16:48:48');
INSERT INTO `sys_login_record` VALUES (1363, 'admin', 1000000000000000001, '223.104.214.205', '四川省', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-04 16:48:52');
INSERT INTO `sys_login_record` VALUES (1364, 'admin', 1000000000000000001, '117.174.18.200', '四川省成都市双流区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-05 01:14:28');
INSERT INTO `sys_login_record` VALUES (1365, 'admin', NULL, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '验证码有误', '2024-03-05 13:17:30');
INSERT INTO `sys_login_record` VALUES (1366, 'admin', 1000000000000000001, '101.207.235.42', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-05 13:17:33');
INSERT INTO `sys_login_record` VALUES (1367, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-05 17:15:08');
INSERT INTO `sys_login_record` VALUES (1368, 'admin', 1000000000000000001, '182.150.44.245', '四川省成都市武侯区', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-05 17:41:38');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `menu_type` int NULL DEFAULT NULL COMMENT '1.目录 2 菜单 3 按钮',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'URL',
  `parent_id` bigint NOT NULL COMMENT '父ID',
  `sort` tinyint NOT NULL COMMENT '排序',
  `perms` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `table_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '基于哪个表生成',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号ID',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号ID',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1171101653676327965 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1000000000000003001, '系统管理', 1, '', 0, 2, NULL, 'el-icon-setting', NULL, NULL, '2022-12-14 15:46:25', '2023-10-28 16:59:40', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003002, '接口文档', 1, NULL, 0, 10, NULL, 'el-icon-s-order', NULL, NULL, '2022-12-14 15:46:25', '2023-11-08 11:05:24', NULL, NULL, 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1000000000000003003, '日志管理', 1, '', 0, 8, '', 'el-icon-document', '', NULL, '2022-12-14 15:46:25', '2023-11-08 11:06:33', NULL, NULL, 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1000000000000003011, '用户管理', 2, 'system/sysUser/user.html', 1000000000000003001, 1, NULL, 'el-icon-user-solid', NULL, NULL, '2022-12-14 15:46:25', '2023-11-27 16:59:15', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003012, '角色管理', 2, 'system/sysRole/role.html', 1000000000000003001, 2, NULL, 'el-icon-s-cooperation', NULL, NULL, '2022-12-14 15:46:25', '2023-11-27 16:59:28', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003013, '菜单管理', 2, 'system/sysMenu/menu.html', 1000000000000003001, 3, NULL, 'el-icon-s-order', NULL, NULL, '2022-12-14 15:46:25', '2023-11-27 16:59:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003021, '接口文档', 2, 'tool/api.html', 1000000000000003002, 1, NULL, 'el-icon-document', NULL, NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003031, '接口日志', 2, 'log/api.html', 1000000000000003003, 1, '', 'el-icon-document', '', NULL, '2022-12-14 15:46:25', '2023-11-17 10:17:13', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1000000000000003111, '查询', 3, 'sys/user/list', 1000000000000003011, 1, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003112, '新增', 3, 'sys/user/add', 1000000000000003011, 2, NULL, NULL, NULL, NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003113, '修改', 3, 'sys/user/update', 1000000000000003011, 3, NULL, NULL, NULL, NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003114, '删除', 3, 'sys/user/delete', 1000000000000003011, 4, NULL, NULL, NULL, NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003115, '导入', 3, 'sys/user/imports', 1000000000000003011, 5, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003116, '导出', 3, 'sys/user/export', 1000000000000003011, 6, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003211, '查询', 3, 'sys/role/list', 1000000000000003012, 1, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003212, '新增', 3, 'sys/role/add', 1000000000000003012, 2, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003213, '修改', 3, 'sys/role/update', 1000000000000003012, 3, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003214, '删除', 3, 'sys/role/delete', 1000000000000003012, 4, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003311, '查询', 3, 'sys/menu/list', 1000000000000003013, 1, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003312, '新增', 3, 'sys/menu/add', 1000000000000003013, 2, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003313, '修改', 3, 'sys/menu/update', 1000000000000003013, 3, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003314, '删除', 3, 'sys/menu/delete', 1000000000000003013, 4, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003421, '查询', 3, 'tool/api/list', 1000000000000003021, 1, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003422, '导出', 3, 'tool/api/export', 1000000000000003021, 2, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003521, '查询', 3, 'tool/table/list', 1000000000000003022, 1, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003522, '导出', 3, 'tool/table/export', 1000000000000003022, 2, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1143582605195608064, '首页', 1, '', 0, 1, '', 'el-icon-s-home', '', NULL, '2023-08-22 16:29:00', '2023-11-08 11:04:46', NULL, NULL, 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1143583807417352192, '首页概览', 2, 'home/home.html', 1143582605195608064, 1, '', 'el-icon-s-platform', '', NULL, '2023-08-22 16:33:46', '2023-08-22 16:33:46', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1164976341993390080, '数据采集', 1, '', 0, 5, '', 'el-icon-s-order', '', NULL, '2023-10-20 17:20:04', '2024-02-26 06:30:29', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1165064540677734400, '资源列表', 2, 'info/info.html', 1164976341993390080, 2, '', 'el-icon-s-order', '', NULL, '2023-10-20 23:10:32', '2023-10-20 23:22:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682368, '岗位管理', 2, 'system/sysPost/post.html', 1000000000000003001, 4, '', 'el-icon-s-ticket', '', NULL, '2023-11-03 23:22:52', '2023-11-27 17:00:39', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1170141074442682369, '查询', 3, 'sys/post/pageList', 1170141074442682368, 1, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682370, '新增', 3, 'sys/post/add', 1170141074442682368, 2, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682371, '修改', 3, 'sys/post/update', 1170141074442682368, 3, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682372, '删除', 3, 'sys/post/delete', 1170141074442682368, 4, '', '', '', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676326936, '代码部署', 1, '', 0, 12, '', 'el-icon-s-promotion', '', NULL, '2023-11-07 15:24:44', '2023-11-08 11:03:04', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676326938, '部署代码', 2, 'gen/import-table.html', 1171101653676326936, 2, '', 'el-icon-s-promotion', '', NULL, '2023-11-07 17:58:29', '2023-12-22 16:27:10', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327124, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327125, '教程列表', 2, 'starCource/starCource.html', 1171101653676327124, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327126, '教程添加', 3, 'admin/starCource/add', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327127, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327128, '教程修改', 3, 'admin/starCource/update', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327129, '教程删除', 3, 'admin/starCource/delete', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327136, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327137, '教程列表', 2, 'starCource/starCource.html', 1171101653676327136, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327138, '教程添加', 3, 'admin/starCource/add', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327139, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327140, '教程修改', 3, 'admin/starCource/update', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327141, '教程删除', 3, 'admin/starCource/delete', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327142, '班级管理', 1, '#', 0, 14, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327143, '班级列表', 2, 'classInfo/classInfo.html', 1171101653676327142, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327144, '班级添加', 3, 'admin/classInfo/add', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327145, '班级列表', 3, 'admin/classInfo/pageList', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327146, '班级修改', 3, 'admin/classInfo/update', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327147, '班级删除', 3, 'admin/classInfo/delete', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327148, '教程列表', 2, 'starCource/starCource.html', 1000000000000003001, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327149, '教程添加', 3, 'admin/starCource/add', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327150, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327151, '教程修改', 3, 'admin/starCource/update', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327152, '教程删除', 3, 'admin/starCource/delete', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327153, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327154, '教程列表', 2, 'starCource/starCource.html', 1171101653676327153, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327155, '教程添加', 3, 'admin/starCource/add', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327156, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327157, '教程修改', 3, 'admin/starCource/update', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327158, '教程删除', 3, 'admin/starCource/delete', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327159, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:43:41', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327160, '教程列表', 2, 'starCource/starCource.html', 1171101653676327159, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327161, '教程添加', 3, 'admin/starCource/add', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327162, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327163, '教程修改', 3, 'admin/starCource/update', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327164, '教程删除', 3, 'admin/starCource/delete', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327165, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:37:29', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327166, '教程列表', 2, 'starCource/starCource.html', 1171101653676327165, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:37:29', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327167, '教程添加', 3, 'admin/starCource/add', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327168, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327169, '教程修改', 3, 'admin/starCource/update', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327170, '教程删除', 3, 'admin/starCource/delete', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327171, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:40:59', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327172, '教程列表', 2, 'starCource/starCource.html', 1171101653676327171, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:40:59', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327173, '教程添加', 3, 'admin/starCource/add', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327174, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327175, '教程修改', 3, 'admin/starCource/update', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327176, '教程删除', 3, 'admin/starCource/delete', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327177, '快速建表', 2, 'gen/create-table.html', 1171101653676326936, 1, '', 'el-icon-s-order', '', NULL, '2023-11-13 21:46:23', '2023-11-13 21:46:22', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327178, '战队管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327179, '战队列表', 2, 'tTeam/tTeam.html', 1171101653676327178, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327180, '战队添加', 3, 'admin/tTeam/add', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327181, '战队列表', 3, 'admin/tTeam/pageList', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327182, '战队修改', 3, 'admin/tTeam/update', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327183, '战队删除', 3, 'admin/tTeam/delete', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327184, '战队管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:45:32', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327185, '战队列表', 2, 'apTeam/apTeam.html', 1171101653676327184, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:45:32', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327186, '战队添加', 3, 'admin/apTeam/add', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327187, '战队列表', 3, 'admin/apTeam/pageList', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327188, '战队修改', 3, 'admin/apTeam/update', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327189, '战队删除', 3, 'admin/apTeam/delete', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327190, '战队管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:51:23', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327191, '战队列表', 2, 'apTeam/apTeam.html', 1171101653676327190, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:51:23', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327192, '战队添加', 3, 'admin/apTeam/add', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327193, '战队列表', 3, 'admin/apTeam/pageList', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327194, '战队修改', 3, 'admin/apTeam/update', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327195, '战队删除', 3, 'admin/apTeam/delete', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327196, '用户管理', 1, '#', 0, 14, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327197, '用户列表', 2, 'apUser/apUser.html', 1171101653676327196, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327198, '用户添加', 3, 'admin/apUser/add', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327199, '用户列表', 3, 'admin/apUser/pageList', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327200, '用户修改', 3, 'admin/apUser/update', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327201, '用户删除', 3, 'admin/apUser/delete', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327202, '用户管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:11:00', '2023-11-15 09:17:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327203, '用户列表', 2, 'apUser/apUser.html', 1171101653676327202, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:11:00', '2023-11-15 09:17:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327204, '用户添加', 3, 'admin/apUser/add', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:11:00', '2023-11-15 09:17:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327205, '用户列表', 3, 'admin/apUser/pageList', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:11:00', '2023-11-15 09:17:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327206, '用户修改', 3, 'admin/apUser/update', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:11:00', '2023-11-15 09:17:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327207, '用户删除', 3, 'admin/apUser/delete', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:11:00', '2023-11-15 09:17:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327208, '战队管理', 1, '#', 0, 14, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:22:45', '2023-11-14 23:24:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327209, '战队列表', 2, 'apTeam/apTeam.html', 1171101653676327208, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:22:45', '2023-11-14 23:24:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327210, '战队添加', 3, 'admin/apTeam/add', 1171101653676327209, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:22:45', '2023-11-14 23:24:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327211, '战队列表', 3, 'admin/apTeam/pageList', 1171101653676327209, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:22:45', '2023-11-14 23:24:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327212, '战队修改', 3, 'admin/apTeam/update', 1171101653676327209, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:22:45', '2023-11-14 23:24:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327213, '战队删除', 3, 'admin/apTeam/delete', 1171101653676327209, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-14 23:22:45', '2023-11-14 23:24:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327214, '订单管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-15 16:26:04', '2023-11-15 16:27:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327215, '订单列表', 2, 'apOrder/apOrder.html', 1171101653676327214, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-15 16:26:04', '2023-11-15 16:27:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327216, '订单添加', 3, 'admin/apOrder/add', 1171101653676327215, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-15 16:26:04', '2023-11-15 16:27:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327217, '订单列表', 3, 'admin/apOrder/pageList', 1171101653676327215, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-15 16:26:04', '2023-11-15 16:27:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327218, '订单修改', 3, 'admin/apOrder/update', 1171101653676327215, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-15 16:26:04', '2023-11-15 16:27:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327219, '订单删除', 3, 'admin/apOrder/delete', 1171101653676327215, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-15 16:26:04', '2023-11-15 16:27:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327220, '订单管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:12:33', '2023-11-16 11:22:58', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327221, '订单列表', 2, 'apOrder/apOrder.html', 1171101653676327220, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:12:33', '2023-11-16 11:22:58', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327222, '订单添加', 3, 'admin/apOrder/add', 1171101653676327221, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:12:34', '2023-11-16 11:22:58', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327223, '订单列表', 3, 'admin/apOrder/pageList', 1171101653676327221, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:12:34', '2023-11-16 11:22:58', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327224, '订单修改', 3, 'admin/apOrder/update', 1171101653676327221, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:12:34', '2023-11-16 11:22:58', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327225, '订单删除', 3, 'admin/apOrder/delete', 1171101653676327221, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:12:34', '2023-11-16 11:22:58', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327226, '订单管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:23:22', '2023-11-16 13:32:49', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327227, '订单列表', 2, 'apOrder/apOrder.html', 1171101653676327226, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:23:22', '2023-11-16 13:32:49', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327228, '订单添加', 3, 'admin/apOrder/add', 1171101653676327227, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:23:22', '2023-11-16 13:32:49', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327229, '订单列表', 3, 'admin/apOrder/pageList', 1171101653676327227, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:23:22', '2023-11-16 13:32:49', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327230, '订单修改', 3, 'admin/apOrder/update', 1171101653676327227, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:23:22', '2023-11-16 13:32:49', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327231, '订单删除', 3, 'admin/apOrder/delete', 1171101653676327227, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 11:23:22', '2023-11-16 13:32:49', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327232, '订单管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 13:33:13', '2023-11-16 14:36:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327233, '订单列表', 2, 'apOrder/apOrder.html', 1171101653676327232, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 13:33:13', '2023-11-16 14:36:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327234, '订单添加', 3, 'admin/apOrder/add', 1171101653676327233, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 13:33:13', '2023-11-16 14:36:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327235, '订单列表', 3, 'admin/apOrder/pageList', 1171101653676327233, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 13:33:13', '2023-11-16 14:36:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327236, '订单修改', 3, 'admin/apOrder/update', 1171101653676327233, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 13:33:13', '2023-11-16 14:36:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327237, '订单删除', 3, 'admin/apOrder/delete', 1171101653676327233, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 13:33:13', '2023-11-16 14:36:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327238, '订单管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 14:37:16', '2023-11-17 23:17:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327239, '订单列表', 2, 'apOrder/apOrder.html', 1171101653676327238, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 14:37:16', '2023-11-17 23:17:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327240, '订单添加', 3, 'admin/apOrder/add', 1171101653676327239, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 14:37:17', '2023-11-17 23:17:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327241, '订单列表', 3, 'admin/apOrder/pageList', 1171101653676327239, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 14:37:17', '2023-11-17 23:17:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327242, '订单修改', 3, 'admin/apOrder/update', 1171101653676327239, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 14:37:17', '2023-11-17 23:17:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327243, '订单删除', 3, 'admin/apOrder/delete', 1171101653676327239, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-16 14:37:17', '2023-11-17 23:17:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327244, '登录日志', 2, 'system/sysLoginRecord/sysLoginRecord.html', 1000000000000003003, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:14:09', '2023-11-27 17:10:04', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327245, '系统访问记录添加', 3, 'admin/sysLoginRecord/add', 1171101653676327244, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:14:09', '2023-11-17 10:14:09', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327246, '系统访问记录列表', 3, 'admin/sysLoginRecord/pageList', 1171101653676327244, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:14:09', '2023-11-17 10:14:09', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327247, '系统访问记录修改', 3, 'admin/sysLoginRecord/update', 1171101653676327244, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:14:09', '2023-11-17 10:14:09', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327248, '系统访问记录删除', 3, 'admin/sysLoginRecord/delete', 1171101653676327244, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:14:09', '2023-11-17 10:14:09', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327249, '操作日志', 2, 'system/sysOperLog/sysOperLog.html', 1000000000000003003, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:15:44', '2023-11-27 17:10:11', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327250, '操作日志记录添加', 3, 'admin/sysOperLog/add', 1171101653676327249, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:15:44', '2023-11-17 10:15:44', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327251, '操作日志记录列表', 3, 'admin/sysOperLog/pageList', 1171101653676327249, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:15:44', '2023-11-17 10:15:44', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327252, '操作日志记录修改', 3, 'admin/sysOperLog/update', 1171101653676327249, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:15:44', '2023-11-17 10:15:44', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327253, '操作日志记录删除', 3, 'admin/sysOperLog/delete', 1171101653676327249, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 10:15:44', '2023-11-17 10:15:44', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327254, '订单管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:17:50', '2023-11-18 14:02:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327255, '订单列表', 2, 'apOrder/apOrder.html', 1171101653676327254, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:17:50', '2023-11-18 14:02:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327256, '订单添加', 3, 'admin/apOrder/add', 1171101653676327255, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:17:50', '2023-11-18 14:02:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327257, '订单列表', 3, 'admin/apOrder/pageList', 1171101653676327255, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:17:50', '2023-11-18 14:02:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327258, '订单修改', 3, 'admin/apOrder/update', 1171101653676327255, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:17:50', '2023-11-18 14:02:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327259, '订单删除', 3, 'admin/apOrder/delete', 1171101653676327255, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:17:50', '2023-11-18 14:02:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327260, '商品管理', 1, '#', 0, 14, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:21:34', '2023-11-18 14:04:33', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327261, '商品列表', 2, 'apProject/apProject.html', 1171101653676327260, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:21:34', '2023-11-17 23:21:35', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327262, '商品添加', 3, 'admin/apProject/add', 1171101653676327261, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:21:34', '2023-11-17 23:21:35', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327263, '商品列表', 3, 'admin/apProject/pageList', 1171101653676327261, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:21:34', '2023-11-17 23:21:35', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327264, '商品修改', 3, 'admin/apProject/update', 1171101653676327261, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:21:34', '2023-11-17 23:21:35', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327265, '商品删除', 3, 'admin/apProject/delete', 1171101653676327261, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:21:34', '2023-11-17 23:21:35', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327266, '会员管理', 1, '#', 0, 15, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:55:29', '2023-11-18 00:01:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327267, '会员列表', 2, 'apMember/apMember.html', 1171101653676327266, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:55:29', '2023-11-18 00:01:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327268, '会员添加', 3, 'admin/apMember/add', 1171101653676327267, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:55:29', '2023-11-18 00:01:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327269, '会员列表', 3, 'admin/apMember/pageList', 1171101653676327267, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:55:29', '2023-11-18 00:01:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327270, '会员修改', 3, 'admin/apMember/update', 1171101653676327267, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:55:29', '2023-11-18 00:01:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327271, '会员删除', 3, 'admin/apMember/delete', 1171101653676327267, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-17 23:55:29', '2023-11-18 00:01:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327272, '会员管理', 1, '#', 0, 15, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 00:07:03', '2023-11-18 00:08:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327273, '会员列表', 2, 'apMember/apMember.html', 1171101653676327272, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 00:07:03', '2023-11-18 00:08:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327274, '会员添加', 3, 'admin/apMember/add', 1171101653676327273, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 00:07:03', '2023-11-18 00:08:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327275, '会员列表', 3, 'admin/apMember/pageList', 1171101653676327273, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 00:07:03', '2023-11-18 00:08:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327276, '会员修改', 3, 'admin/apMember/update', 1171101653676327273, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 00:07:03', '2023-11-18 00:08:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327277, '会员删除', 3, 'admin/apMember/delete', 1171101653676327273, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 00:07:03', '2023-11-18 00:08:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327278, '会员管理', 1, '#', 0, 15, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:55:27', '2023-11-18 14:01:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327279, '会员列表', 2, 'apMember/apMember.html', 1171101653676327278, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:55:27', '2023-11-18 14:01:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327280, '会员添加', 3, 'admin/apMember/add', 1171101653676327279, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:55:27', '2023-11-18 14:01:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327281, '会员列表', 3, 'admin/apMember/pageList', 1171101653676327279, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:55:27', '2023-11-18 14:01:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327282, '会员修改', 3, 'admin/apMember/update', 1171101653676327279, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:55:27', '2023-11-18 14:01:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327283, '会员删除', 3, 'admin/apMember/delete', 1171101653676327279, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:55:27', '2023-11-18 14:01:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327284, '品牌管理', 1, '#', 0, 16, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:58:29', '2023-11-18 14:01:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327285, '品牌列表', 2, 'apBrand/apBrand.html', 1171101653676327284, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:58:29', '2023-11-18 14:01:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327286, '品牌添加', 3, 'admin/apBrand/add', 1171101653676327285, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:58:29', '2023-11-18 14:01:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327287, '品牌列表', 3, 'admin/apBrand/pageList', 1171101653676327285, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:58:29', '2023-11-18 14:01:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327288, '品牌修改', 3, 'admin/apBrand/update', 1171101653676327285, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:58:29', '2023-11-18 14:01:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327289, '品牌删除', 3, 'admin/apBrand/delete', 1171101653676327285, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 13:58:29', '2023-11-18 14:01:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327290, '订单管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 15:26:22', '2023-11-18 16:39:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327291, '订单列表', 2, 'apOrder/apOrder.html', 1171101653676327290, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 15:26:22', '2023-11-18 16:39:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327292, '订单添加', 3, 'admin/apOrder/add', 1171101653676327291, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 15:26:22', '2023-11-18 16:39:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327293, '订单列表', 3, 'admin/apOrder/pageList', 1171101653676327291, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 15:26:22', '2023-11-18 16:39:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327294, '订单修改', 3, 'admin/apOrder/update', 1171101653676327291, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 15:26:22', '2023-11-18 16:39:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327295, '订单删除', 3, 'admin/apOrder/delete', 1171101653676327291, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 15:26:22', '2023-11-18 16:39:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327296, '系统配置', 1, '', 0, 20, '', 'el-icon-warning', '', NULL, '2023-11-18 16:10:23', '2023-11-18 16:10:24', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327297, '字典类型列表', 2, 'system/sysDictType/sysDictType.html', 1171101653676327296, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:17:13', '2023-11-27 17:08:53', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327298, '字典类型添加', 3, 'admin/sysDictType/add', 1171101653676327297, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:17:13', '2023-11-18 16:39:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327299, '字典类型列表', 3, 'admin/sysDictType/pageList', 1171101653676327297, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:17:13', '2023-11-18 16:39:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327300, '字典类型修改', 3, 'admin/sysDictType/update', 1171101653676327297, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:17:13', '2023-11-18 16:39:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327301, '字典类型删除', 3, 'admin/sysDictType/delete', 1171101653676327297, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:17:13', '2023-11-18 16:39:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327302, '字典数据管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:37:27', '2023-11-18 16:39:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327303, '字典数据列表', 2, 'system/sysDictData/sysDictData.html', 1171101653676327302, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:37:27', '2023-11-27 17:09:51', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327304, '字典数据添加', 3, 'admin/sysDictData/add', 1171101653676327303, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:37:27', '2023-11-18 16:39:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327305, '字典数据列表', 3, 'admin/sysDictData/pageList', 1171101653676327303, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:37:27', '2023-11-18 16:39:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327306, '字典数据修改', 3, 'admin/sysDictData/update', 1171101653676327303, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:37:27', '2023-11-18 16:39:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327307, '字典数据删除', 3, 'admin/sysDictData/delete', 1171101653676327303, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:37:27', '2023-11-18 16:39:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327308, '字典类型管理', 2, 'system/sysDictType/sysDictType.html', 1171101653676327296, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:48:25', '2023-11-27 17:09:47', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327309, '字典类型添加', 3, 'admin/sysDictType/add', 1171101653676327308, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:48:26', '2023-11-18 16:48:26', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327310, '字典类型列表', 3, 'admin/sysDictType/pageList', 1171101653676327308, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:48:26', '2023-11-18 16:48:26', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327311, '字典类型修改', 3, 'admin/sysDictType/update', 1171101653676327308, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:48:26', '2023-11-18 16:48:26', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327312, '字典类型删除', 3, 'admin/sysDictType/delete', 1171101653676327308, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:48:26', '2023-11-18 16:48:26', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327313, '字典数据管理', 1, '#', 1171101653676327296, 21, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:51:50', '2023-11-18 16:55:16', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 1);
INSERT INTO `sys_menu` VALUES (1171101653676327314, '字典数据管理', 2, 'system/sysDictData/sysDictData.html', 1171101653676327296, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:51:50', '2023-11-27 17:09:42', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327315, '字典数据添加', 3, 'admin/sysDictData/add', 1171101653676327314, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:51:50', '2023-11-18 16:51:51', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327316, '字典数据列表', 3, 'admin/sysDictData/pageList', 1171101653676327314, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:51:50', '2023-11-18 16:51:51', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327317, '字典数据修改', 3, 'admin/sysDictData/update', 1171101653676327314, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:51:50', '2023-11-18 16:51:51', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327318, '字典数据删除', 3, 'admin/sysDictData/delete', 1171101653676327314, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-18 16:51:50', '2023-11-18 16:51:51', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327319, '测试 管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:18:41', '2023-11-19 14:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327320, '测试 列表', 2, 'apTest/apTest.html', 1171101653676327319, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:18:41', '2023-11-19 14:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327321, '测试 添加', 3, 'admin/apTest/add', 1171101653676327320, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:18:41', '2023-11-19 14:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327322, '测试 列表', 3, 'admin/apTest/pageList', 1171101653676327320, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:18:41', '2023-11-19 14:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327323, '测试 修改', 3, 'admin/apTest/update', 1171101653676327320, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:18:41', '2023-11-19 14:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327324, '测试 删除', 3, 'admin/apTest/delete', 1171101653676327320, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:18:41', '2023-11-19 14:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327325, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:26:05', '2023-11-19 14:29:56', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327326, '测试列表', 2, 'apTest/apTest.html', 1171101653676327325, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:26:06', '2023-11-19 14:29:56', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327327, '测试添加', 3, 'admin/apTest/add', 1171101653676327326, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:26:06', '2023-11-19 14:29:56', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327328, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327326, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:26:06', '2023-11-19 14:29:56', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327329, '测试修改', 3, 'admin/apTest/update', 1171101653676327326, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:26:06', '2023-11-19 14:29:56', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327330, '测试删除', 3, 'admin/apTest/delete', 1171101653676327326, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 14:26:06', '2023-11-19 14:29:56', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327331, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 16:20:06', '2023-11-19 17:19:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327332, '测试列表', 2, 'apTest/apTest.html', 1171101653676327331, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 16:20:06', '2023-11-19 17:19:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327333, '测试添加', 3, 'admin/apTest/add', 1171101653676327332, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 16:20:06', '2023-11-19 17:19:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327334, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327332, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 16:20:06', '2023-11-19 17:19:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327335, '测试修改', 3, 'admin/apTest/update', 1171101653676327332, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 16:20:06', '2023-11-19 17:19:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327336, '测试删除', 3, 'admin/apTest/delete', 1171101653676327332, 1, NULL, 'el-icon-notebook-2', NULL, NULL, '2023-11-19 16:20:06', '2023-11-19 17:19:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327337, '测试1管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 17:45:24', '2023-11-19 18:03:21', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327338, '测试1列表', 2, 'apTest1/apTest1.html', 1171101653676327337, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 17:45:24', '2023-11-19 18:03:21', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327339, '测试1添加', 3, 'admin/apTest1/add', 1171101653676327338, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 17:45:24', '2023-11-19 18:03:21', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327340, '测试1列表', 3, 'admin/apTest1/pageList', 1171101653676327338, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 17:45:24', '2023-11-19 18:03:21', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327341, '测试1修改', 3, 'admin/apTest1/update', 1171101653676327338, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 17:45:24', '2023-11-19 18:03:21', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327342, '测试1删除', 3, 'admin/apTest1/delete', 1171101653676327338, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 17:45:24', '2023-11-19 18:03:21', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327343, '测试2列表', 2, 'apTest2/apTest2.html', 1171101653676327337, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-19 17:47:51', '2023-11-19 18:01:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327344, '测试2添加', 3, 'admin/apTest2/add', 1171101653676327343, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-19 17:47:51', '2023-11-19 18:01:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327345, '测试2列表', 3, 'admin/apTest2/pageList', 1171101653676327343, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-19 17:47:51', '2023-11-19 18:01:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327346, '测试2修改', 3, 'admin/apTest2/update', 1171101653676327343, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-19 17:47:51', '2023-11-19 18:01:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327347, '测试2删除', 3, 'admin/apTest2/delete', 1171101653676327343, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-19 17:47:51', '2023-11-19 18:01:00', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327348, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:05:24', '2023-11-19 18:08:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327349, '测试列表', 2, 'apTest/apTest.html', 1171101653676327348, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:05:24', '2023-11-19 18:08:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327350, '测试添加', 3, 'admin/apTest/add', 1171101653676327349, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:05:24', '2023-11-19 18:08:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327351, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327349, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:05:24', '2023-11-19 18:08:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327352, '测试修改', 3, 'admin/apTest/update', 1171101653676327349, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:05:24', '2023-11-19 18:08:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327353, '测试删除', 3, 'admin/apTest/delete', 1171101653676327349, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:05:24', '2023-11-19 18:08:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327354, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:10:12', '2023-11-19 18:11:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327355, '测试列表', 2, 'apTest/apTest.html', 1171101653676327354, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:10:12', '2023-11-19 18:11:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327356, '测试添加', 3, 'admin/apTest/add', 1171101653676327355, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:10:12', '2023-11-19 18:11:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327357, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327355, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:10:12', '2023-11-19 18:11:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327358, '测试修改', 3, 'admin/apTest/update', 1171101653676327355, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:10:12', '2023-11-19 18:11:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327359, '测试删除', 3, 'admin/apTest/delete', 1171101653676327355, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:10:12', '2023-11-19 18:11:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327360, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:14:10', '2023-11-19 18:16:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327361, '测试列表', 2, 'apTest1/apTest1.html', 1171101653676327360, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:14:10', '2023-11-19 18:16:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327362, '测试添加', 3, 'admin/apTest1/add', 1171101653676327361, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:14:10', '2023-11-19 18:16:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327363, '测试列表', 3, 'admin/apTest1/pageList', 1171101653676327361, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:14:10', '2023-11-19 18:16:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327364, '测试修改', 3, 'admin/apTest1/update', 1171101653676327361, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:14:10', '2023-11-19 18:16:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327365, '测试删除', 3, 'admin/apTest1/delete', 1171101653676327361, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:14:10', '2023-11-19 18:16:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327366, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:17:03', '2023-11-19 18:26:30', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327367, '测试列表', 2, 'apTest1/apTest1.html', 1171101653676327366, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:17:03', '2023-11-19 18:26:30', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327368, '测试添加', 3, 'admin/apTest1/add', 1171101653676327367, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:17:03', '2023-11-19 18:26:30', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327369, '测试列表', 3, 'admin/apTest1/pageList', 1171101653676327367, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:17:03', '2023-11-19 18:26:30', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327370, '测试修改', 3, 'admin/apTest1/update', 1171101653676327367, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:17:03', '2023-11-19 18:26:30', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327371, '测试删除', 3, 'admin/apTest1/delete', 1171101653676327367, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-19 18:17:03', '2023-11-19 18:26:30', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327372, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:32:16', '2023-11-19 18:36:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327373, '测试列表', 2, 'apTest/apTest.html', 1171101653676327372, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:32:16', '2023-11-19 18:36:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327374, '测试添加', 3, 'admin/apTest/add', 1171101653676327373, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:32:16', '2023-11-19 18:36:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327375, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327373, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:32:16', '2023-11-19 18:36:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327376, '测试修改', 3, 'admin/apTest/update', 1171101653676327373, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:32:16', '2023-11-19 18:36:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327377, '测试删除', 3, 'admin/apTest/delete', 1171101653676327373, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:32:16', '2023-11-19 18:36:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327378, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:38:18', '2023-11-19 18:42:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327379, '测试列表', 2, 'apTest/apTest.html', 1171101653676327378, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:38:18', '2023-11-19 18:42:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327380, '测试添加', 3, 'admin/apTest/add', 1171101653676327379, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:38:19', '2023-11-19 18:42:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327381, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327379, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:38:19', '2023-11-19 18:42:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327382, '测试修改', 3, 'admin/apTest/update', 1171101653676327379, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:38:19', '2023-11-19 18:42:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327383, '测试删除', 3, 'admin/apTest/delete', 1171101653676327379, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:38:19', '2023-11-19 18:42:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327384, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:42:42', '2023-11-19 18:43:48', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327385, '测试列表', 2, 'apTest/apTest.html', 1171101653676327384, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:42:43', '2023-11-19 18:43:48', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327386, '测试添加', 3, 'admin/apTest/add', 1171101653676327385, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:42:43', '2023-11-19 18:43:48', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327387, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327385, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:42:43', '2023-11-19 18:43:48', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327388, '测试修改', 3, 'admin/apTest/update', 1171101653676327385, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:42:43', '2023-11-19 18:43:48', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327389, '测试删除', 3, 'admin/apTest/delete', 1171101653676327385, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:42:43', '2023-11-19 18:43:48', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327390, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:50:07', '2023-11-19 21:48:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327391, '测试列表', 2, 'apTest/apTest.html', 1171101653676327390, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:50:07', '2023-11-19 21:48:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327392, '测试添加', 3, 'admin/apTest/add', 1171101653676327391, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:50:07', '2023-11-19 21:48:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327393, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327391, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:50:07', '2023-11-19 21:48:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327394, '测试修改', 3, 'admin/apTest/update', 1171101653676327391, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:50:07', '2023-11-19 21:48:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327395, '测试删除', 3, 'admin/apTest/delete', 1171101653676327391, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 18:50:07', '2023-11-19 21:48:13', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327396, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 21:50:05', '2023-11-19 22:03:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327397, '测试列表', 2, 'apTest/apTest.html', 1171101653676327396, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 21:50:05', '2023-11-19 22:03:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327398, '测试添加', 3, 'admin/apTest/add', 1171101653676327397, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 21:50:05', '2023-11-19 22:03:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327399, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327397, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 21:50:05', '2023-11-19 22:03:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327400, '测试修改', 3, 'admin/apTest/update', 1171101653676327397, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 21:50:05', '2023-11-19 22:03:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327401, '测试删除', 3, 'admin/apTest/delete', 1171101653676327397, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 21:50:05', '2023-11-19 22:03:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327402, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:04:08', '2023-11-19 22:13:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327403, '测试列表', 2, 'apTest/apTest.html', 1171101653676327402, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:04:08', '2023-11-19 22:13:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327404, '测试添加', 3, 'admin/apTest/add', 1171101653676327403, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:04:08', '2023-11-19 22:13:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327405, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327403, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:04:08', '2023-11-19 22:13:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327406, '测试修改', 3, 'admin/apTest/update', 1171101653676327403, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:04:08', '2023-11-19 22:13:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327407, '测试删除', 3, 'admin/apTest/delete', 1171101653676327403, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:04:08', '2023-11-19 22:13:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327408, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:14:17', '2023-11-19 22:18:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327409, '测试列表', 2, 'apTest/apTest.html', 1171101653676327408, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:14:17', '2023-11-19 22:18:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327410, '测试添加', 3, 'admin/apTest/add', 1171101653676327409, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:14:18', '2023-11-19 22:18:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327411, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327409, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:14:18', '2023-11-19 22:18:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327412, '测试修改', 3, 'admin/apTest/update', 1171101653676327409, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:14:18', '2023-11-19 22:18:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327413, '测试删除', 3, 'admin/apTest/delete', 1171101653676327409, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:14:18', '2023-11-19 22:18:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327414, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:19:09', '2023-11-19 22:23:54', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327415, '测试列表', 2, 'apTest/apTest.html', 1171101653676327414, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:19:09', '2023-11-19 22:23:54', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327416, '测试添加', 3, 'admin/apTest/add', 1171101653676327415, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:19:09', '2023-11-19 22:23:54', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327417, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327415, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:19:09', '2023-11-19 22:23:54', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327418, '测试修改', 3, 'admin/apTest/update', 1171101653676327415, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:19:09', '2023-11-19 22:23:54', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327419, '测试删除', 3, 'admin/apTest/delete', 1171101653676327415, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:19:09', '2023-11-19 22:23:54', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327420, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:24:19', '2023-11-21 17:20:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327421, '测试列表', 2, 'apTest/apTest.html', 1171101653676327420, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:24:19', '2023-11-21 17:20:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327422, '测试添加', 3, 'admin/apTest/add', 1171101653676327421, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:24:19', '2023-11-21 17:20:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327423, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327421, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:24:19', '2023-11-21 17:20:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327424, '测试修改', 3, 'admin/apTest/update', 1171101653676327421, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:24:19', '2023-11-21 17:20:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327425, '测试删除', 3, 'admin/apTest/delete', 1171101653676327421, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-19 22:24:19', '2023-11-21 17:20:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327426, '测试1列表', 2, 'apTest1/apTest1.html', 1171101653676327420, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-20 21:22:49', '2023-11-20 22:20:23', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327427, '测试1添加', 3, 'admin/apTest1/add', 1171101653676327426, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-20 21:22:49', '2023-11-20 22:20:23', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327428, '测试1列表', 3, 'admin/apTest1/pageList', 1171101653676327426, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-20 21:22:49', '2023-11-20 22:20:23', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327429, '测试1修改', 3, 'admin/apTest1/update', 1171101653676327426, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-20 21:22:49', '2023-11-20 22:20:23', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327430, '测试1删除', 3, 'admin/apTest1/delete', 1171101653676327426, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-20 21:22:49', '2023-11-20 22:20:23', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327431, '系统配置列表', 2, 'system/sysConfig/sysConfig.html', 1171101653676327296, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 21:42:17', '2023-11-27 17:10:45', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327432, '系统配置添加', 3, 'admin/sysConfig/add', 1171101653676327431, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 21:42:17', '2023-11-20 22:08:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327433, '系统配置列表', 3, 'admin/sysConfig/pageList', 1171101653676327431, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 21:42:17', '2023-11-20 22:08:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327434, '系统配置修改', 3, 'admin/sysConfig/update', 1171101653676327431, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 21:42:17', '2023-11-20 22:08:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327435, '系统配置删除', 3, 'admin/sysConfig/delete', 1171101653676327431, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 21:42:17', '2023-11-20 22:08:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327436, '全局系统配置', 2, 'system/sysConfig/sysConfig.html', 1171101653676327296, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 22:17:34', '2023-11-27 17:10:31', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327437, '系统配置添加', 3, 'admin/sysConfig/add', 1171101653676327436, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 22:17:34', '2023-11-20 22:17:33', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327438, '系统配置列表', 3, 'admin/sysConfig/pageList', 1171101653676327436, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 22:17:34', '2023-11-20 22:17:33', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327439, '系统配置修改', 3, 'admin/sysConfig/update', 1171101653676327436, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 22:17:34', '2023-11-20 22:17:33', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327440, '系统配置删除', 3, 'admin/sysConfig/delete', 1171101653676327436, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_config', '2023-11-20 22:17:34', '2023-11-20 22:17:33', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327441, '文件存储配置', 2, 'system/sysOss/sysOss.html', 1171101653676327296, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_oss', '2023-11-20 22:43:56', '2023-11-27 17:10:36', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327442, '文件存储添加', 3, 'admin/sysOss/add', 1171101653676327441, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_oss', '2023-11-20 22:43:56', '2023-11-20 22:43:55', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327443, '文件存储列表', 3, 'admin/sysOss/pageList', 1171101653676327441, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_oss', '2023-11-20 22:43:56', '2023-11-20 22:43:55', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327444, '文件存储修改', 3, 'admin/sysOss/update', 1171101653676327441, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_oss', '2023-11-20 22:43:56', '2023-11-20 22:43:55', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327445, '文件存储删除', 3, 'admin/sysOss/delete', 1171101653676327441, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_oss', '2023-11-20 22:43:56', '2023-11-20 22:43:55', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327446, '代码卸载', 2, 'gen/code-remove.html', 1171101653676326936, 3, '', 'el-icon-delete-solid', '', NULL, '2023-11-21 13:43:11', '2023-11-21 13:43:20', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327447, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:30:29', '2023-11-21 17:33:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327448, '测试列表', 2, 'apTest/apTest.html', 1171101653676327447, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:30:29', '2023-11-21 17:33:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327449, '测试添加', 3, 'admin/apTest/add', 1171101653676327448, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:30:29', '2023-11-21 17:33:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327450, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327448, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:30:29', '2023-11-21 17:33:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327451, '测试修改', 3, 'admin/apTest/update', 1171101653676327448, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:30:29', '2023-11-21 17:33:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327452, '测试删除', 3, 'admin/apTest/delete', 1171101653676327448, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:30:29', '2023-11-21 17:33:47', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327453, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:34:40', '2023-11-21 17:41:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327454, '测试列表', 2, 'apTest/apTest.html', 1171101653676327453, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:34:40', '2023-11-21 17:41:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327455, '测试添加', 3, 'admin/apTest/add', 1171101653676327454, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:34:40', '2023-11-21 17:41:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327456, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327454, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:34:40', '2023-11-21 17:41:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327457, '测试修改', 3, 'admin/apTest/update', 1171101653676327454, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:34:40', '2023-11-21 17:41:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327458, '测试删除', 3, 'admin/apTest/delete', 1171101653676327454, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:34:40', '2023-11-21 17:41:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327459, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:42:16', '2023-11-21 17:59:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327460, '测试列表', 2, 'apTest/apTest.html', 1171101653676327459, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:42:16', '2023-11-21 17:56:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327461, '测试添加', 3, 'admin/apTest/add', 1171101653676327460, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:42:16', '2023-11-21 17:56:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327462, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327460, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:42:16', '2023-11-21 17:56:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327463, '测试修改', 3, 'admin/apTest/update', 1171101653676327460, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:42:16', '2023-11-21 17:56:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327464, '测试删除', 3, 'admin/apTest/delete', 1171101653676327460, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:42:16', '2023-11-21 17:56:19', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327465, '测试2列表', 2, 'apTest2/apTest2.html', 1171101653676327459, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-21 17:47:33', '2023-11-21 17:56:25', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327466, '测试2添加', 3, 'admin/apTest2/add', 1171101653676327465, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-21 17:47:33', '2023-11-21 17:56:25', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327467, '测试2列表', 3, 'admin/apTest2/pageList', 1171101653676327465, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-21 17:47:33', '2023-11-21 17:56:25', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327468, '测试2修改', 3, 'admin/apTest2/update', 1171101653676327465, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-21 17:47:33', '2023-11-21 17:56:25', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327469, '测试2删除', 3, 'admin/apTest2/delete', 1171101653676327465, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test2', '2023-11-21 17:47:33', '2023-11-21 17:56:25', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327470, '测试管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:58:14', '2023-11-21 21:14:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327471, '测试列表', 2, 'apTest/apTest.html', 1171101653676327470, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:58:14', '2023-11-21 21:14:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327472, '测试添加', 3, 'admin/apTest/add', 1171101653676327471, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:58:16', '2023-11-21 21:14:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327473, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327471, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:58:16', '2023-11-21 21:14:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327474, '测试修改', 3, 'admin/apTest/update', 1171101653676327471, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:58:16', '2023-11-21 21:14:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327475, '测试删除', 3, 'admin/apTest/delete', 1171101653676327471, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-21 17:58:16', '2023-11-21 21:14:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327476, '测试2管理', 1, '#', 0, 23, NULL, 'el-icon-notebook-2', NULL, 'ap_test3', '2023-11-21 18:03:55', '2023-11-21 21:14:18', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327477, '测试2列表', 2, 'apTest3/apTest3.html', 1171101653676327476, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test3', '2023-11-21 18:03:55', '2023-11-21 21:14:18', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327478, '测试2添加', 3, 'admin/apTest3/add', 1171101653676327477, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test3', '2023-11-21 18:03:55', '2023-11-21 21:14:18', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327479, '测试2列表', 3, 'admin/apTest3/pageList', 1171101653676327477, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test3', '2023-11-21 18:03:55', '2023-11-21 21:14:18', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327480, '测试2修改', 3, 'admin/apTest3/update', 1171101653676327477, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test3', '2023-11-21 18:03:55', '2023-11-21 21:14:18', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327481, '测试2删除', 3, 'admin/apTest3/delete', 1171101653676327477, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test3', '2023-11-21 18:03:55', '2023-11-21 21:14:18', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327482, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:10:50', '2023-11-23 14:14:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327483, '测试列表', 2, 'apTest/apTest.html', 1171101653676327482, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:10:50', '2023-11-23 14:14:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327484, '测试添加', 3, 'admin/apTest/add', 1171101653676327483, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:10:50', '2023-11-23 14:14:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327485, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327483, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:10:50', '2023-11-23 14:14:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327486, '测试修改', 3, 'admin/apTest/update', 1171101653676327483, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:10:50', '2023-11-23 14:14:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327487, '测试删除', 3, 'admin/apTest/delete', 1171101653676327483, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:10:50', '2023-11-23 14:14:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327488, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:14:42', '2023-11-23 14:19:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327489, '测试列表', 2, 'apTest/apTest.html', 1171101653676327488, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:14:42', '2023-11-23 14:19:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327490, '测试添加', 3, 'admin/apTest/add', 1171101653676327489, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:14:42', '2023-11-23 14:19:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327491, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327489, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:14:42', '2023-11-23 14:19:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327492, '测试修改', 3, 'admin/apTest/update', 1171101653676327489, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:14:42', '2023-11-23 14:19:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327493, '测试删除', 3, 'admin/apTest/delete', 1171101653676327489, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:14:42', '2023-11-23 14:19:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327494, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:20:14', '2023-11-23 15:38:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327495, '测试列表', 2, 'apTest/apTest.html', 1171101653676327494, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:20:14', '2023-11-23 15:38:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327496, '测试添加', 3, 'admin/apTest/add', 1171101653676327495, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:20:14', '2023-11-23 15:38:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327497, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327495, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:20:14', '2023-11-23 15:38:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327498, '测试修改', 3, 'admin/apTest/update', 1171101653676327495, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:20:14', '2023-11-23 15:38:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327499, '测试删除', 3, 'admin/apTest/delete', 1171101653676327495, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 14:20:14', '2023-11-23 15:38:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327500, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:40:21', '2023-11-23 15:53:33', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327501, '测试列表', 2, 'apTest/apTest.html', 1171101653676327500, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:40:21', '2023-11-23 15:53:33', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327502, '测试添加', 3, 'admin/apTest/add', 1171101653676327501, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:40:21', '2023-11-23 15:53:33', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327503, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327501, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:40:21', '2023-11-23 15:53:33', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327504, '测试修改', 3, 'admin/apTest/update', 1171101653676327501, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:40:21', '2023-11-23 15:53:33', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327505, '测试删除', 3, 'admin/apTest/delete', 1171101653676327501, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:40:21', '2023-11-23 15:53:33', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327506, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:54:07', '2023-11-23 19:23:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327507, '测试列表', 2, 'apTest/apTest.html', 1171101653676327506, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:54:07', '2023-11-23 19:23:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327508, '测试添加', 3, 'admin/apTest/add', 1171101653676327507, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:54:07', '2023-11-23 19:23:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327509, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327507, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:54:07', '2023-11-23 19:23:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327510, '测试修改', 3, 'admin/apTest/update', 1171101653676327507, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:54:07', '2023-11-23 19:23:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327511, '测试删除', 3, 'admin/apTest/delete', 1171101653676327507, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-23 15:54:07', '2023-11-23 19:23:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327512, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:30:25', '2023-11-25 11:37:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327513, '测试列表', 2, 'apTest/apTest.html', 1171101653676327512, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:30:25', '2023-11-25 11:37:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327514, '测试添加', 3, 'admin/apTest/add', 1171101653676327513, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:30:25', '2023-11-25 11:37:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327515, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327513, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:30:25', '2023-11-25 11:37:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327516, '测试修改', 3, 'admin/apTest/update', 1171101653676327513, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:30:25', '2023-11-25 11:37:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327517, '测试删除', 3, 'admin/apTest/delete', 1171101653676327513, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:30:25', '2023-11-25 11:37:41', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327518, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:38:25', '2023-11-27 17:26:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327519, '测试列表', 2, 'apTest/apTest.html', 1171101653676327518, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:38:25', '2023-11-27 17:26:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327520, '测试添加', 3, 'admin/apTest/add', 1171101653676327519, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:38:25', '2023-11-27 17:26:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327521, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327519, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:38:25', '2023-11-27 17:26:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327522, '测试修改', 3, 'admin/apTest/update', 1171101653676327519, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:38:25', '2023-11-27 17:26:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327523, '测试删除', 3, 'admin/apTest/delete', 1171101653676327519, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-25 11:38:25', '2023-11-27 17:26:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327524, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:27:58', '2023-11-27 17:33:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327525, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327524, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:27:58', '2023-11-27 17:33:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327526, '测试添加', 3, 'admin/apTest/add', 1171101653676327525, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:27:58', '2023-11-27 17:33:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327527, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327525, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:27:58', '2023-11-27 17:33:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327528, '测试修改', 3, 'admin/apTest/update', 1171101653676327525, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:27:58', '2023-11-27 17:33:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327529, '测试删除', 3, 'admin/apTest/delete', 1171101653676327525, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:27:58', '2023-11-27 17:33:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327530, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:34:03', '2023-11-27 17:35:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327531, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327530, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:34:04', '2023-11-27 17:35:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327532, '测试添加', 3, 'admin/apTest/add', 1171101653676327531, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:34:04', '2023-11-27 17:35:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327533, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327531, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:34:04', '2023-11-27 17:35:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327534, '测试修改', 3, 'admin/apTest/update', 1171101653676327531, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:34:04', '2023-11-27 17:35:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327535, '测试删除', 3, 'admin/apTest/delete', 1171101653676327531, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:34:04', '2023-11-27 17:35:44', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327536, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:36:07', '2023-11-27 17:39:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327537, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327536, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:36:07', '2023-11-27 17:39:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327538, '测试添加', 3, 'admin/apTest/add', 1171101653676327537, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:36:07', '2023-11-27 17:39:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327539, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327537, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:36:07', '2023-11-27 17:39:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327540, '测试修改', 3, 'admin/apTest/update', 1171101653676327537, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:36:07', '2023-11-27 17:39:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327541, '测试删除', 3, 'admin/apTest/delete', 1171101653676327537, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:36:07', '2023-11-27 17:39:06', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327542, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:39:59', '2023-11-27 17:42:11', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327543, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327542, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:40:00', '2023-11-27 17:42:11', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327544, '测试添加', 3, 'admin/apTest/add', 1171101653676327543, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:40:00', '2023-11-27 17:42:11', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327545, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327543, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:40:00', '2023-11-27 17:42:11', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327546, '测试修改', 3, 'admin/apTest/update', 1171101653676327543, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:40:00', '2023-11-27 17:42:11', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327547, '测试删除', 3, 'admin/apTest/delete', 1171101653676327543, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:40:00', '2023-11-27 17:42:11', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327548, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:42:34', '2023-11-27 17:44:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327549, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327548, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:42:34', '2023-11-27 17:44:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327550, '测试添加', 3, 'admin/apTest/add', 1171101653676327549, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:42:34', '2023-11-27 17:44:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327551, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327549, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:42:34', '2023-11-27 17:44:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327552, '测试修改', 3, 'admin/apTest/update', 1171101653676327549, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:42:34', '2023-11-27 17:44:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327553, '测试删除', 3, 'admin/apTest/delete', 1171101653676327549, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:42:34', '2023-11-27 17:44:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327554, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:45:25', '2023-11-27 21:19:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327555, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327554, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:45:25', '2023-11-27 21:19:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327556, '测试添加', 3, 'admin/apTest/add', 1171101653676327555, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:45:25', '2023-11-27 21:19:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327557, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327555, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:45:25', '2023-11-27 21:19:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327558, '测试修改', 3, 'admin/apTest/update', 1171101653676327555, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:45:25', '2023-11-27 21:19:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327559, '测试删除', 3, 'admin/apTest/delete', 1171101653676327555, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 17:45:25', '2023-11-27 21:19:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327560, '测试1管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-27 18:23:23', '2023-11-27 18:29:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327561, '测试1列表', 2, 'admin/apTest1/apTest1.html', 1171101653676327560, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-27 18:23:23', '2023-11-27 18:29:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327562, '测试1添加', 3, 'admin/apTest1/add', 1171101653676327561, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-27 18:23:23', '2023-11-27 18:29:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327563, '测试1列表', 3, 'admin/apTest1/pageList', 1171101653676327561, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-27 18:23:23', '2023-11-27 18:29:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327564, '测试1修改', 3, 'admin/apTest1/update', 1171101653676327561, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-27 18:23:23', '2023-11-27 18:29:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327565, '测试1删除', 3, 'admin/apTest1/delete', 1171101653676327561, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test1', '2023-11-27 18:23:23', '2023-11-27 18:29:42', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327566, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:20:47', '2023-11-27 21:49:36', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327567, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327566, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:20:47', '2023-11-27 21:49:36', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327568, '测试添加', 3, 'admin/apTest/add', 1171101653676327567, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:20:47', '2023-11-27 21:49:36', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327569, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327567, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:20:47', '2023-11-27 21:49:36', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327570, '测试修改', 3, 'admin/apTest/update', 1171101653676327567, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:20:47', '2023-11-27 21:49:36', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327571, '测试删除', 3, 'admin/apTest/delete', 1171101653676327567, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:20:47', '2023-11-27 21:49:36', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327572, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:50:49', '2023-11-28 11:40:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327573, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327572, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:50:49', '2023-11-28 11:40:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327574, '测试添加', 3, 'admin/apTest/add', 1171101653676327573, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:50:50', '2023-11-28 11:40:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327575, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327573, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:50:50', '2023-11-28 11:40:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327576, '测试修改', 3, 'admin/apTest/update', 1171101653676327573, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:50:50', '2023-11-28 11:40:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327577, '测试删除', 3, 'admin/apTest/delete', 1171101653676327573, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-27 21:50:50', '2023-11-28 11:40:17', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327578, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-28 13:21:55', '2023-12-05 17:46:26', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327579, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327578, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-28 13:21:56', '2023-12-05 17:46:26', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327580, '测试添加', 3, 'admin/apTest/add', 1171101653676327579, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-28 13:21:56', '2023-12-05 17:46:26', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327581, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327579, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-28 13:21:56', '2023-12-05 17:46:26', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327582, '测试修改', 3, 'admin/apTest/update', 1171101653676327579, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-28 13:21:56', '2023-12-05 17:46:26', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327583, '测试删除', 3, 'admin/apTest/delete', 1171101653676327579, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-11-28 13:21:56', '2023-12-05 17:46:26', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327584, '测试管理', 1, '#', 0, 21, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-12-05 18:34:12', '2023-12-08 14:18:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327585, '测试列表', 2, 'admin/apTest/apTest.html', 1171101653676327584, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-12-05 18:34:12', '2023-12-08 14:18:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327586, '测试添加', 3, 'admin/apTest/add', 1171101653676327585, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-12-05 18:34:12', '2023-12-08 14:18:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327587, '测试列表', 3, 'admin/apTest/pageList', 1171101653676327585, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-12-05 18:34:12', '2023-12-08 14:18:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327588, '测试修改', 3, 'admin/apTest/update', 1171101653676327585, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-12-05 18:34:12', '2023-12-08 14:18:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327589, '测试删除', 3, 'admin/apTest/delete', 1171101653676327585, 1, NULL, 'el-icon-notebook-2', NULL, 'ap_test', '2023-12-05 18:34:12', '2023-12-08 14:18:14', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327590, '公告管理管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:48:15', '2023-12-06 09:52:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327591, '公告管理列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327590, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:48:15', '2023-12-06 09:52:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327592, '公告管理添加', 3, 'admin/sysNotify/add', 1171101653676327591, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:48:15', '2023-12-06 09:52:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327593, '公告管理列表', 3, 'admin/sysNotify/pageList', 1171101653676327591, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:48:15', '2023-12-06 09:52:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327594, '公告管理修改', 3, 'admin/sysNotify/update', 1171101653676327591, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:48:15', '2023-12-06 09:52:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327595, '公告管理删除', 3, 'admin/sysNotify/delete', 1171101653676327591, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:48:15', '2023-12-06 09:52:57', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327596, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:57:39', '2023-12-06 10:01:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327597, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327596, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:57:39', '2023-12-06 10:01:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327598, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327597, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:57:39', '2023-12-06 10:01:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327599, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327597, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:57:39', '2023-12-06 10:01:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327600, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327597, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:57:39', '2023-12-06 10:01:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327601, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327597, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 09:57:39', '2023-12-06 10:01:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327602, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:02:07', '2023-12-06 10:10:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327603, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327602, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:02:07', '2023-12-06 10:10:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327604, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327603, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:02:07', '2023-12-06 10:10:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327605, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327603, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:02:07', '2023-12-06 10:10:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327606, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327603, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:02:07', '2023-12-06 10:10:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327607, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327603, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:02:07', '2023-12-06 10:10:59', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327608, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:11:28', '2023-12-06 10:19:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327609, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327608, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:11:28', '2023-12-06 10:19:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327610, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327609, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:11:28', '2023-12-06 10:19:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327611, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327609, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:11:28', '2023-12-06 10:19:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327612, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327609, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:11:28', '2023-12-06 10:19:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327613, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327609, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:11:28', '2023-12-06 10:19:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327614, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:20:22', '2023-12-06 10:36:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327615, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327614, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:20:22', '2023-12-06 10:36:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327616, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327615, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:20:23', '2023-12-06 10:36:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327617, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327615, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:20:23', '2023-12-06 10:36:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327618, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327615, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:20:23', '2023-12-06 10:36:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327619, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327615, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:20:23', '2023-12-06 10:36:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327620, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:36:59', '2023-12-06 10:42:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327621, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327620, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:36:59', '2023-12-06 10:42:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327622, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327621, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:36:59', '2023-12-06 10:42:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327623, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327621, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:36:59', '2023-12-06 10:42:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327624, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327621, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:36:59', '2023-12-06 10:42:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327625, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327621, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:36:59', '2023-12-06 10:42:29', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327626, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:47:18', '2023-12-06 10:55:46', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327627, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327626, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:47:18', '2023-12-06 10:55:46', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327628, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327627, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:47:18', '2023-12-06 10:55:46', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327629, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327627, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:47:18', '2023-12-06 10:55:46', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327630, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327627, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:47:18', '2023-12-06 10:55:46', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327631, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327627, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 10:47:18', '2023-12-06 10:55:46', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327632, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:05:52', '2023-12-06 11:10:45', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327633, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327632, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:05:52', '2023-12-06 11:10:45', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327634, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327633, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:05:52', '2023-12-06 11:10:45', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327635, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327633, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:05:52', '2023-12-06 11:10:45', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327636, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327633, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:05:52', '2023-12-06 11:10:45', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327637, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327633, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:05:52', '2023-12-06 11:10:45', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327638, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:11:13', '2023-12-06 11:14:28', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327639, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327638, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:11:14', '2023-12-06 11:14:28', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327640, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327639, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:11:14', '2023-12-06 11:14:28', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327641, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327639, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:11:14', '2023-12-06 11:14:28', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327642, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327639, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:11:14', '2023-12-06 11:14:28', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327643, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327639, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:11:14', '2023-12-06 11:14:28', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327644, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:15:26', '2023-12-06 11:18:53', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327645, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327644, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:15:26', '2023-12-06 11:18:53', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327646, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327645, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:15:27', '2023-12-06 11:18:53', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327647, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327645, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:15:27', '2023-12-06 11:18:53', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327648, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327645, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:15:27', '2023-12-06 11:18:53', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327649, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327645, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:15:27', '2023-12-06 11:18:53', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327650, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:19:51', '2023-12-06 11:20:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327651, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327650, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:19:51', '2023-12-06 11:20:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327652, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327651, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:19:51', '2023-12-06 11:20:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327653, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327651, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:19:51', '2023-12-06 11:20:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327654, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327651, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:19:51', '2023-12-06 11:20:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327655, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327651, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:19:51', '2023-12-06 11:20:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327656, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:21:42', '2023-12-06 11:25:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327657, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327656, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:21:42', '2023-12-06 11:25:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327658, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327657, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:21:42', '2023-12-06 11:25:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327659, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327657, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:21:42', '2023-12-06 11:25:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327660, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327657, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:21:42', '2023-12-06 11:25:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327661, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327657, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:21:42', '2023-12-06 11:25:16', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327662, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:30:51', '2023-12-06 11:47:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327663, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327662, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:30:51', '2023-12-06 11:47:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327664, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327663, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:30:51', '2023-12-06 11:47:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327665, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327663, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:30:51', '2023-12-06 11:47:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327666, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327663, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:30:51', '2023-12-06 11:47:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327667, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327663, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:30:51', '2023-12-06 11:47:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327668, '通知公告管理', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:48:48', '2023-12-06 11:52:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327669, '通知公告列表', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327668, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:48:48', '2023-12-06 11:52:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327670, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327669, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:48:48', '2023-12-06 11:52:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327671, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327669, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:48:48', '2023-12-06 11:52:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327672, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327669, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:48:48', '2023-12-06 11:52:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327673, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327669, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:48:48', '2023-12-06 11:52:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327674, '通知公告', 1, '#', 0, 22, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:52:29', '2023-12-06 18:31:11', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327675, '通知公告', 2, 'admin/sysNotify/sysNotify.html', 1171101653676327674, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:52:29', '2023-12-06 18:31:20', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327676, '通知公告添加', 3, 'admin/sysNotify/add', 1171101653676327675, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:52:30', '2023-12-06 11:52:30', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327677, '通知公告列表', 3, 'admin/sysNotify/pageList', 1171101653676327675, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:52:30', '2023-12-06 11:52:30', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327678, '通知公告修改', 3, 'admin/sysNotify/update', 1171101653676327675, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:52:30', '2023-12-06 11:52:30', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327679, '通知公告删除', 3, 'admin/sysNotify/delete', 1171101653676327675, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_notify', '2023-12-06 11:52:30', '2023-12-06 11:52:30', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327680, '消息列表', 2, 'admin/sysMessage/sysMessage.html', 1171101653676327674, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_message', '2023-12-06 14:48:22', '2023-12-06 14:48:22', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327681, '消息添加', 3, 'admin/sysMessage/add', 1171101653676327680, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_message', '2023-12-06 14:48:22', '2023-12-06 14:48:22', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327682, '消息列表', 3, 'admin/sysMessage/pageList', 1171101653676327680, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_message', '2023-12-06 14:48:22', '2023-12-06 14:48:22', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327683, '消息修改', 3, 'admin/sysMessage/update', 1171101653676327680, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_message', '2023-12-06 14:48:22', '2023-12-06 14:48:22', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327684, '消息删除', 3, 'admin/sysMessage/delete', 1171101653676327680, 1, NULL, 'el-icon-notebook-2', NULL, 'sys_message', '2023-12-06 14:48:22', '2023-12-06 14:48:22', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327685, '系统监控', 1, '', 0, 20, '', 'el-icon-s-platform', '', NULL, '2023-12-07 09:54:58', '2023-12-07 09:54:58', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327686, '在线用户', 2, 'system/sysMonitor/sysOnLineUser.html', 1171101653676327685, 1, '', 'el-icon-video-camera', '', NULL, '2023-12-07 09:55:48', '2023-12-07 09:56:54', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327841, '重置密码', 3, 'sys/user/resetPwd', 1000000000000003011, 1, '', 'el-icon-s-cooperation', '', NULL, '2023-12-27 17:34:39', '2023-12-27 17:34:38', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327941, '用户管理管理', 1, '#', 0, 32, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:12:13', '2024-02-05 04:22:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327942, '用户管理列表', 2, 'admin/aUser/aUser.html', 1171101653676327941, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:12:13', '2024-02-05 04:22:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327943, '用户管理添加', 3, 'admin/aUser/add', 1171101653676327942, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:12:13', '2024-02-05 04:22:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327944, '用户管理列表', 3, 'admin/aUser/pageList', 1171101653676327942, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:12:13', '2024-02-05 04:22:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327945, '用户管理修改', 3, 'admin/aUser/update', 1171101653676327942, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:12:13', '2024-02-05 04:22:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327946, '用户管理删除', 3, 'admin/aUser/delete', 1171101653676327942, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:12:13', '2024-02-05 04:22:37', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327947, '用户管理', 1, '#', 0, 32, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:30:36', '2024-02-05 04:33:34', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327948, '用户列表', 2, 'admin/aUser/aUser.html', 1171101653676327947, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:30:36', '2024-02-05 04:33:34', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327949, '用户添加', 3, 'admin/aUser/add', 1171101653676327948, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:30:36', '2024-02-05 04:33:34', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327950, '用户列表', 3, 'admin/aUser/pageList', 1171101653676327948, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:30:36', '2024-02-05 04:33:34', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327951, '用户修改', 3, 'admin/aUser/update', 1171101653676327948, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:30:36', '2024-02-05 04:33:34', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327952, '用户删除', 3, 'admin/aUser/delete', 1171101653676327948, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:30:36', '2024-02-05 04:33:34', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327953, '用户管理', 1, '#', 0, 32, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:34:15', '2024-02-05 04:48:40', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327954, '用户列表', 2, 'admin/aUser/aUser.html', 1171101653676327953, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:34:15', '2024-02-05 04:48:40', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327955, '用户添加', 3, 'admin/aUser/add', 1171101653676327954, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:34:16', '2024-02-05 04:48:40', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327956, '用户列表', 3, 'admin/aUser/pageList', 1171101653676327954, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:34:16', '2024-02-05 04:48:40', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327957, '用户修改', 3, 'admin/aUser/update', 1171101653676327954, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:34:16', '2024-02-05 04:48:40', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327958, '用户删除', 3, 'admin/aUser/delete', 1171101653676327954, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:34:16', '2024-02-05 04:48:40', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327959, '用户管理', 1, '#', 0, 32, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:52:00', '2024-02-05 10:58:25', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327960, '用户列表', 2, 'admin/aUser/aUser.html', 1171101653676327959, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:52:00', '2024-02-05 10:58:03', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327961, '用户添加', 3, 'admin/aUser/add', 1171101653676327960, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:52:01', '2024-02-05 04:51:59', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327962, '用户列表', 3, 'admin/aUser/pageList', 1171101653676327960, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:52:01', '2024-02-05 04:51:59', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327963, '用户修改', 3, 'admin/aUser/update', 1171101653676327960, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:52:01', '2024-02-05 04:51:59', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327964, '用户删除', 3, 'admin/aUser/delete', 1171101653676327960, 1, NULL, 'el-icon-notebook-2', NULL, 'a_user', '2024-02-05 04:52:01', '2024-02-05 04:51:59', 1000000000000000001, 'admin', NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_message
-- ----------------------------
DROP TABLE IF EXISTS `sys_message`;
CREATE TABLE `sys_message`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sender` bigint NULL DEFAULT NULL COMMENT '发送方',
  `sender_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发送方名称',
  `receiver` bigint NULL DEFAULT NULL COMMENT '接收方',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '内容',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标题',
  `status` tinyint NULL DEFAULT 0 COMMENT '0 未读  1已读',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 695 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_message
-- ----------------------------
INSERT INTO `sys_message` VALUES (461, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-08 14:13:53', 'admin', 0);
INSERT INTO `sys_message` VALUES (462, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:13:33', 'admin', 1167805277462855680, '2023-12-08 14:14:32', 'mars', 0);
INSERT INTO `sys_message` VALUES (463, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:13:33', 'admin', 1170050787619897344, '2024-02-01 23:04:01', 'lisi', 0);
INSERT INTO `sys_message` VALUES (464, 1167805277462855680, 'mars', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, NULL, '2023-12-08 14:17:30', NULL, 1167805277462855680, '2023-12-08 14:17:38', 'mars', 0);
INSERT INTO `sys_message` VALUES (465, 1167805277462855680, 'mars', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-08 14:53:58', 'admin', 0);
INSERT INTO `sys_message` VALUES (466, 1167805277462855680, 'mars', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1167805277462855680, '2023-12-08 14:17:30', 'mars', 1170050787619897344, '2024-02-01 23:04:01', 'lisi', 0);
INSERT INTO `sys_message` VALUES (467, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-08 14:53:58', 'admin', 0);
INSERT INTO `sys_message` VALUES (468, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:53:37', 'admin', 1167805277462855680, '2023-12-22 23:51:39', 'mars', 0);
INSERT INTO `sys_message` VALUES (469, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:53:37', 'admin', 1170050787619897344, '2024-02-01 23:04:01', 'lisi', 0);
INSERT INTO `sys_message` VALUES (470, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-08 14:53:57', 'admin', 0);
INSERT INTO `sys_message` VALUES (471, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:53:50', 'admin', 1167805277462855680, '2023-12-22 23:51:39', 'mars', 0);
INSERT INTO `sys_message` VALUES (472, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:53:50', 'admin', 1170050787619897344, '2024-02-01 23:04:01', 'lisi', 0);
INSERT INTO `sys_message` VALUES (473, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-08 14:53:57', 'admin', 0);
INSERT INTO `sys_message` VALUES (474, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:53:51', 'admin', 1167805277462855680, '2023-12-22 23:51:39', 'mars', 0);
INSERT INTO `sys_message` VALUES (475, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-08 14:53:51', 'admin', 1170050787619897344, '2024-02-01 23:04:00', 'lisi', 0);
INSERT INTO `sys_message` VALUES (476, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:47', 'admin', 0);
INSERT INTO `sys_message` VALUES (477, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:05', 'admin', 1167805277462855680, '2023-12-22 23:51:39', 'mars', 0);
INSERT INTO `sys_message` VALUES (478, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:05', 'admin', 1170050787619897344, '2024-02-01 23:04:00', 'lisi', 0);
INSERT INTO `sys_message` VALUES (479, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:47', 'admin', 0);
INSERT INTO `sys_message` VALUES (480, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:06', 'admin', 1167805277462855680, '2023-12-22 23:51:38', 'mars', 0);
INSERT INTO `sys_message` VALUES (481, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:06', 'admin', 1170050787619897344, '2024-02-01 23:04:00', 'lisi', 0);
INSERT INTO `sys_message` VALUES (482, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:47', 'admin', 0);
INSERT INTO `sys_message` VALUES (483, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:14', 'admin', 1167805277462855680, '2023-12-22 23:51:38', 'mars', 0);
INSERT INTO `sys_message` VALUES (484, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:14', 'admin', 1170050787619897344, '2024-02-01 23:04:00', 'lisi', 0);
INSERT INTO `sys_message` VALUES (485, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:46', 'admin', 0);
INSERT INTO `sys_message` VALUES (486, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:15', 'admin', 1167805277462855680, '2023-12-22 23:51:38', 'mars', 0);
INSERT INTO `sys_message` VALUES (487, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:15', 'admin', 1170050787619897344, '2024-02-01 23:04:00', 'lisi', 0);
INSERT INTO `sys_message` VALUES (488, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:46', 'admin', 0);
INSERT INTO `sys_message` VALUES (489, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:24', 'admin', 1167805277462855680, '2023-12-22 23:51:38', 'mars', 0);
INSERT INTO `sys_message` VALUES (490, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:24', 'admin', 1170050787619897344, '2024-02-01 23:04:00', 'lisi', 0);
INSERT INTO `sys_message` VALUES (491, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:46', 'admin', 0);
INSERT INTO `sys_message` VALUES (492, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:26', 'admin', 1167805277462855680, '2023-12-22 23:51:38', 'mars', 0);
INSERT INTO `sys_message` VALUES (493, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:26', 'admin', 1170050787619897344, '2024-02-01 23:03:59', 'lisi', 0);
INSERT INTO `sys_message` VALUES (494, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:46', 'admin', 0);
INSERT INTO `sys_message` VALUES (495, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:28', 'admin', 1167805277462855680, '2023-12-22 23:51:38', 'mars', 0);
INSERT INTO `sys_message` VALUES (496, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:28', 'admin', 1170050787619897344, '2024-02-01 23:03:59', 'lisi', 0);
INSERT INTO `sys_message` VALUES (497, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', 1000000000000000001, '2023-12-09 01:07:46', 'admin', 0);
INSERT INTO `sys_message` VALUES (498, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:41', 'admin', 1167805277462855680, '2023-12-22 23:51:37', 'mars', 0);
INSERT INTO `sys_message` VALUES (499, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-09 01:07:41', 'admin', 1170050787619897344, '2024-02-01 23:03:59', 'lisi', 0);
INSERT INTO `sys_message` VALUES (500, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', NULL, '2023-12-13 13:05:24', NULL, 0);
INSERT INTO `sys_message` VALUES (501, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:25', 'admin', 1167805277462855680, '2023-12-22 23:51:37', 'mars', 0);
INSERT INTO `sys_message` VALUES (502, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:25', 'admin', 1170050787619897344, '2024-02-01 23:03:59', 'lisi', 0);
INSERT INTO `sys_message` VALUES (503, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', NULL, '2023-12-13 13:05:27', NULL, 0);
INSERT INTO `sys_message` VALUES (504, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:28', 'admin', 1167805277462855680, '2023-12-22 23:51:37', 'mars', 0);
INSERT INTO `sys_message` VALUES (505, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:28', 'admin', 1170050787619897344, '2024-02-01 23:03:59', 'lisi', 0);
INSERT INTO `sys_message` VALUES (506, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:29', 'admin', 1167805277462855680, '2023-12-22 23:51:37', 'mars', 0);
INSERT INTO `sys_message` VALUES (507, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:29', 'admin', 1170050787619897344, '2024-02-01 23:03:59', 'lisi', 0);
INSERT INTO `sys_message` VALUES (508, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', NULL, '2023-12-13 13:05:29', NULL, 0);
INSERT INTO `sys_message` VALUES (509, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', NULL, '2023-12-13 13:05:29', NULL, 0);
INSERT INTO `sys_message` VALUES (510, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:30', 'admin', 1167805277462855680, '2023-12-22 23:51:37', 'mars', 0);
INSERT INTO `sys_message` VALUES (511, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:30', 'admin', 1170050787619897344, '2024-02-01 23:03:58', 'lisi', 0);
INSERT INTO `sys_message` VALUES (512, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-18 23:31:27', 'admin', NULL, '2023-12-13 13:05:30', NULL, 0);
INSERT INTO `sys_message` VALUES (513, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:31', 'admin', 1167805277462855680, '2023-12-22 23:51:37', 'mars', 0);
INSERT INTO `sys_message` VALUES (514, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-13 13:05:31', 'admin', 1170050787619897344, '2024-02-01 23:03:58', 'lisi', 0);
INSERT INTO `sys_message` VALUES (515, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:41', 'admin', 1000000000000000001, '2023-12-21 16:51:40', 'admin', 0);
INSERT INTO `sys_message` VALUES (516, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:41', 'admin', 1167805277462855680, '2023-12-22 23:51:36', 'mars', 0);
INSERT INTO `sys_message` VALUES (517, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:41', 'admin', 1170050787619897344, '2024-02-01 23:03:58', 'lisi', 0);
INSERT INTO `sys_message` VALUES (518, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:44', 'admin', 1000000000000000001, '2023-12-21 16:51:40', 'admin', 0);
INSERT INTO `sys_message` VALUES (519, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:44', 'admin', 1167805277462855680, '2023-12-22 23:51:36', 'mars', 0);
INSERT INTO `sys_message` VALUES (520, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:44', 'admin', 1170050787619897344, '2024-02-01 23:03:58', 'lisi', 0);
INSERT INTO `sys_message` VALUES (521, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:47', 'admin', 1000000000000000001, '2023-12-21 16:51:40', 'admin', 0);
INSERT INTO `sys_message` VALUES (522, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:47', 'admin', 1167805277462855680, '2023-12-22 23:51:36', 'mars', 0);
INSERT INTO `sys_message` VALUES (523, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:47', 'admin', 1170050787619897344, '2024-02-01 23:03:58', 'lisi', 0);
INSERT INTO `sys_message` VALUES (524, 1000000000000000001, 'admin', 1000000000000000001, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:48', 'admin', 1000000000000000001, '2023-12-21 16:51:40', 'admin', 0);
INSERT INTO `sys_message` VALUES (525, 1000000000000000001, 'admin', 1167805277462855680, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:48', 'admin', 1167805277462855680, '2023-12-22 23:51:36', 'mars', 0);
INSERT INTO `sys_message` VALUES (526, 1000000000000000001, 'admin', 1170050787619897344, '今天晚上聚餐大家早点', '111', 1, 1000000000000000001, '2023-12-20 18:24:48', 'admin', 1170050787619897344, '2024-02-01 23:03:58', 'lisi', 0);
INSERT INTO `sys_message` VALUES (527, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-22 23:51:19', 'admin', 1000000000000000001, '2023-12-22 23:52:13', 'admin', 0);
INSERT INTO `sys_message` VALUES (528, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-22 23:51:19', 'admin', 1167805277462855680, '2023-12-22 23:51:36', 'mars', 0);
INSERT INTO `sys_message` VALUES (529, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-22 23:51:19', 'admin', 1170050787619897344, '2024-02-01 23:03:57', 'lisi', 0);
INSERT INTO `sys_message` VALUES (530, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-22 23:52:21', 'admin', 1000000000000000001, '2023-12-22 23:52:25', 'admin', 0);
INSERT INTO `sys_message` VALUES (531, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-22 23:52:21', 'admin', 1167805277462855680, '2023-12-23 00:01:00', 'mars', 0);
INSERT INTO `sys_message` VALUES (532, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-22 23:52:21', 'admin', 1170050787619897344, '2024-02-01 23:03:57', 'lisi', 0);
INSERT INTO `sys_message` VALUES (533, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-23 00:00:50', 'admin', 1167805277462855680, '2023-12-23 00:01:00', 'mars', 0);
INSERT INTO `sys_message` VALUES (534, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-23 00:00:50', 'admin', 1000000000000000001, '2023-12-25 21:52:30', 'admin', 0);
INSERT INTO `sys_message` VALUES (535, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-23 00:00:50', 'admin', 1170050787619897344, '2024-02-01 23:03:57', 'lisi', 0);
INSERT INTO `sys_message` VALUES (536, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-23 00:00:54', 'admin', 1167805277462855680, '2023-12-23 00:01:00', 'mars', 0);
INSERT INTO `sys_message` VALUES (537, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-23 00:00:54', 'admin', 1000000000000000001, '2023-12-25 21:52:30', 'admin', 0);
INSERT INTO `sys_message` VALUES (538, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-23 00:00:54', 'admin', 1170050787619897344, '2024-02-01 23:03:57', 'lisi', 0);
INSERT INTO `sys_message` VALUES (539, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-27 16:01:17', 'admin', 1000000000000000001, '2023-12-27 16:01:30', 'admin', 0);
INSERT INTO `sys_message` VALUES (540, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-27 16:01:17', 'admin', 1167805277462855680, '2024-02-01 22:58:57', 'mars', 0);
INSERT INTO `sys_message` VALUES (541, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-27 16:01:17', 'admin', 1170050787619897344, '2024-02-01 23:03:57', 'lisi', 0);
INSERT INTO `sys_message` VALUES (542, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-27 16:01:21', 'admin', 1000000000000000001, '2023-12-27 16:01:30', 'admin', 0);
INSERT INTO `sys_message` VALUES (543, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-27 16:01:21', 'admin', 1167805277462855680, '2024-02-01 22:58:57', 'mars', 0);
INSERT INTO `sys_message` VALUES (544, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2023-12-27 16:01:21', 'admin', 1170050787619897344, '2024-02-01 23:03:57', 'lisi', 0);
INSERT INTO `sys_message` VALUES (545, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-08 16:52:24', 'admin', 1000000000000000001, '2024-01-08 16:52:30', 'admin', 0);
INSERT INTO `sys_message` VALUES (546, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-08 16:52:24', 'admin', 1167805277462855680, '2024-02-01 22:58:56', 'mars', 0);
INSERT INTO `sys_message` VALUES (547, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-08 16:52:24', 'admin', 1170050787619897344, '2024-02-01 23:03:57', 'lisi', 0);
INSERT INTO `sys_message` VALUES (548, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:47', 'admin', 1000000000000000001, '2024-01-26 10:39:49', 'admin', 0);
INSERT INTO `sys_message` VALUES (549, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:48', 'admin', 1167805277462855680, '2024-02-01 22:58:56', 'mars', 0);
INSERT INTO `sys_message` VALUES (550, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:48', 'admin', 1170050787619897344, '2024-02-01 23:03:56', 'lisi', 0);
INSERT INTO `sys_message` VALUES (551, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:50', 'admin', 1000000000000000001, '2024-01-26 10:39:49', 'admin', 0);
INSERT INTO `sys_message` VALUES (552, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:50', 'admin', 1167805277462855680, '2024-02-01 22:58:56', 'mars', 0);
INSERT INTO `sys_message` VALUES (553, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:50', 'admin', 1170050787619897344, '2024-02-01 23:03:56', 'lisi', 0);
INSERT INTO `sys_message` VALUES (554, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:51', 'admin', 1000000000000000001, '2024-01-26 10:39:48', 'admin', 0);
INSERT INTO `sys_message` VALUES (555, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:51', 'admin', 1167805277462855680, '2024-02-01 22:58:56', 'mars', 0);
INSERT INTO `sys_message` VALUES (556, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:51', 'admin', 1170050787619897344, '2024-02-01 23:03:56', 'lisi', 0);
INSERT INTO `sys_message` VALUES (557, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:53', 'admin', 1000000000000000001, '2024-01-26 10:39:48', 'admin', 0);
INSERT INTO `sys_message` VALUES (558, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:53', 'admin', 1167805277462855680, '2024-02-01 22:58:56', 'mars', 0);
INSERT INTO `sys_message` VALUES (559, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-21 20:08:53', 'admin', 1170050787619897344, '2024-02-01 23:03:56', 'lisi', 0);
INSERT INTO `sys_message` VALUES (560, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:32', NULL, 1000000000000000001, '2024-01-26 10:39:48', 'admin', 0);
INSERT INTO `sys_message` VALUES (561, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:32', NULL, 1167805277462855680, '2024-02-01 22:58:56', 'mars', 0);
INSERT INTO `sys_message` VALUES (562, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:32', NULL, 1170050787619897344, '2024-02-01 23:03:56', 'lisi', 0);
INSERT INTO `sys_message` VALUES (563, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:39', NULL, 1000000000000000001, '2024-01-26 10:39:48', 'admin', 0);
INSERT INTO `sys_message` VALUES (564, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:39', NULL, 1167805277462855680, '2024-02-01 22:58:55', 'mars', 0);
INSERT INTO `sys_message` VALUES (565, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:39', NULL, 1170050787619897344, '2024-02-01 23:03:56', 'lisi', 0);
INSERT INTO `sys_message` VALUES (566, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:40', NULL, 1000000000000000001, '2024-01-26 10:39:48', 'admin', 0);
INSERT INTO `sys_message` VALUES (567, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:41', NULL, 1167805277462855680, '2024-02-01 22:58:55', 'mars', 0);
INSERT INTO `sys_message` VALUES (568, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:41', NULL, 1170050787619897344, '2024-02-01 23:03:55', 'lisi', 0);
INSERT INTO `sys_message` VALUES (569, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:48', NULL, 1000000000000000001, '2024-01-26 10:39:48', 'admin', 0);
INSERT INTO `sys_message` VALUES (570, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:48', NULL, 1167805277462855680, '2024-02-01 22:58:55', 'mars', 0);
INSERT INTO `sys_message` VALUES (571, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:48', NULL, 1170050787619897344, '2024-02-01 23:03:55', 'lisi', 0);
INSERT INTO `sys_message` VALUES (572, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:55', NULL, 1000000000000000001, '2024-01-26 10:39:48', 'admin', 0);
INSERT INTO `sys_message` VALUES (573, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:56', NULL, 1167805277462855680, '2024-02-01 22:58:55', 'mars', 0);
INSERT INTO `sys_message` VALUES (574, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, NULL, '2024-01-22 22:16:56', NULL, 1170050787619897344, '2024-02-01 23:03:55', 'lisi', 0);
INSERT INTO `sys_message` VALUES (575, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, NULL, '2024-01-31 23:04:59', NULL, 1000000000000000001, '2024-01-31 23:05:36', 'admin', 0);
INSERT INTO `sys_message` VALUES (576, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:00', 'admin', 1167805277462855680, '2024-02-01 22:58:55', 'mars', 0);
INSERT INTO `sys_message` VALUES (577, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:00', 'admin', 1170050787619897344, '2024-02-01 23:03:55', 'lisi', 0);
INSERT INTO `sys_message` VALUES (578, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:00', 'admin', NULL, '2024-01-31 23:05:00', NULL, 0);
INSERT INTO `sys_message` VALUES (579, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:00', 'admin', NULL, '2024-01-31 23:05:00', NULL, 0);
INSERT INTO `sys_message` VALUES (580, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:00', 'admin', NULL, '2024-01-31 23:05:00', NULL, 0);
INSERT INTO `sys_message` VALUES (581, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:00', 'admin', NULL, '2024-01-31 23:05:00', NULL, 0);
INSERT INTO `sys_message` VALUES (582, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:04', 'admin', 1000000000000000001, '2024-01-31 23:05:36', 'admin', 0);
INSERT INTO `sys_message` VALUES (583, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:04', 'admin', 1167805277462855680, '2024-02-01 22:58:55', 'mars', 0);
INSERT INTO `sys_message` VALUES (584, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:04', 'admin', 1170050787619897344, '2024-02-01 23:03:55', 'lisi', 0);
INSERT INTO `sys_message` VALUES (585, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:04', 'admin', NULL, '2024-01-31 23:05:04', NULL, 0);
INSERT INTO `sys_message` VALUES (586, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:04', 'admin', NULL, '2024-01-31 23:05:04', NULL, 0);
INSERT INTO `sys_message` VALUES (587, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:04', 'admin', NULL, '2024-01-31 23:05:04', NULL, 0);
INSERT INTO `sys_message` VALUES (588, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:04', 'admin', NULL, '2024-01-31 23:05:04', NULL, 0);
INSERT INTO `sys_message` VALUES (589, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:06', 'admin', 1000000000000000001, '2024-01-31 23:05:35', 'admin', 0);
INSERT INTO `sys_message` VALUES (590, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, NULL, '2024-01-31 23:05:06', NULL, 1167805277462855680, '2024-02-01 22:58:54', 'mars', 0);
INSERT INTO `sys_message` VALUES (591, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, NULL, '2024-01-31 23:05:06', NULL, 1170050787619897344, '2024-02-01 23:03:55', 'lisi', 0);
INSERT INTO `sys_message` VALUES (592, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:06', NULL, NULL, '2024-01-31 23:05:07', NULL, 0);
INSERT INTO `sys_message` VALUES (593, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:06', NULL, NULL, '2024-01-31 23:05:07', NULL, 0);
INSERT INTO `sys_message` VALUES (594, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:06', NULL, NULL, '2024-01-31 23:05:07', NULL, 0);
INSERT INTO `sys_message` VALUES (595, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:06', NULL, NULL, '2024-01-31 23:05:07', NULL, 0);
INSERT INTO `sys_message` VALUES (596, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:14', 'admin', 1000000000000000001, '2024-01-31 23:05:35', 'admin', 0);
INSERT INTO `sys_message` VALUES (597, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, NULL, '2024-01-31 23:05:14', NULL, 1167805277462855680, '2024-02-01 22:58:54', 'mars', 0);
INSERT INTO `sys_message` VALUES (598, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, NULL, '2024-01-31 23:05:14', NULL, 1170050787619897344, '2024-02-01 23:03:55', 'lisi', 0);
INSERT INTO `sys_message` VALUES (599, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:14', NULL, NULL, '2024-01-31 23:05:14', NULL, 0);
INSERT INTO `sys_message` VALUES (600, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:14', NULL, NULL, '2024-01-31 23:05:14', NULL, 0);
INSERT INTO `sys_message` VALUES (601, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:14', NULL, NULL, '2024-01-31 23:05:14', NULL, 0);
INSERT INTO `sys_message` VALUES (602, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, NULL, '2024-01-31 23:05:14', NULL, NULL, '2024-01-31 23:05:14', NULL, 0);
INSERT INTO `sys_message` VALUES (603, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, NULL, '2024-01-31 23:05:15', NULL, 1000000000000000001, '2024-01-31 23:05:35', 'admin', 0);
INSERT INTO `sys_message` VALUES (604, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:15', 'admin', 1167805277462855680, '2024-02-01 22:58:54', 'mars', 0);
INSERT INTO `sys_message` VALUES (605, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:15', 'admin', 1170050787619897344, '2024-02-01 23:03:54', 'lisi', 0);
INSERT INTO `sys_message` VALUES (606, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:15', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (607, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:15', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (608, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:15', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (609, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:15', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (610, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:16', 'admin', 1000000000000000001, '2024-01-31 23:05:35', 'admin', 0);
INSERT INTO `sys_message` VALUES (611, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:16', 'admin', 1167805277462855680, '2024-02-01 22:58:54', 'mars', 0);
INSERT INTO `sys_message` VALUES (612, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:16', 'admin', 1170050787619897344, '2024-02-01 23:03:54', 'lisi', 0);
INSERT INTO `sys_message` VALUES (613, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:16', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (614, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:16', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (615, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:16', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (616, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:16', 'admin', NULL, '2024-01-31 23:05:16', NULL, 0);
INSERT INTO `sys_message` VALUES (617, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:25', 'admin', 1000000000000000001, '2024-01-31 23:05:35', 'admin', 0);
INSERT INTO `sys_message` VALUES (618, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:25', 'admin', 1167805277462855680, '2024-02-01 22:58:54', 'mars', 0);
INSERT INTO `sys_message` VALUES (619, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:25', 'admin', 1170050787619897344, '2024-02-01 23:03:54', 'lisi', 0);
INSERT INTO `sys_message` VALUES (620, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:25', 'admin', NULL, '2024-01-31 23:05:25', NULL, 0);
INSERT INTO `sys_message` VALUES (621, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:25', 'admin', NULL, '2024-01-31 23:05:25', NULL, 0);
INSERT INTO `sys_message` VALUES (622, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:25', 'admin', NULL, '2024-01-31 23:05:25', NULL, 0);
INSERT INTO `sys_message` VALUES (623, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:25', 'admin', NULL, '2024-01-31 23:05:25', NULL, 0);
INSERT INTO `sys_message` VALUES (624, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:28', 'admin', 1000000000000000001, '2024-01-31 23:05:35', 'admin', 0);
INSERT INTO `sys_message` VALUES (625, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:28', 'admin', 1167805277462855680, '2024-02-01 22:58:54', 'mars', 0);
INSERT INTO `sys_message` VALUES (626, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:28', 'admin', 1170050787619897344, '2024-02-01 23:03:54', 'lisi', 0);
INSERT INTO `sys_message` VALUES (627, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:28', 'admin', NULL, '2024-01-31 23:05:28', NULL, 0);
INSERT INTO `sys_message` VALUES (628, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:28', 'admin', NULL, '2024-01-31 23:05:28', NULL, 0);
INSERT INTO `sys_message` VALUES (629, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:28', 'admin', NULL, '2024-01-31 23:05:28', NULL, 0);
INSERT INTO `sys_message` VALUES (630, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:28', 'admin', NULL, '2024-01-31 23:05:28', NULL, 0);
INSERT INTO `sys_message` VALUES (631, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:29', 'admin', 1000000000000000001, '2024-01-31 23:05:35', 'admin', 0);
INSERT INTO `sys_message` VALUES (632, 1000000000000000001, 'admin', 1167805277462855680, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:29', 'admin', 1167805277462855680, '2024-02-01 22:58:53', 'mars', 0);
INSERT INTO `sys_message` VALUES (633, 1000000000000000001, 'admin', 1170050787619897344, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-01-31 23:05:29', 'admin', 1170050787619897344, '2024-02-01 23:03:54', 'lisi', 0);
INSERT INTO `sys_message` VALUES (634, 1000000000000000001, 'admin', 1199019160365957121, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:29', 'admin', NULL, '2024-01-31 23:05:29', NULL, 0);
INSERT INTO `sys_message` VALUES (635, 1000000000000000001, 'admin', 1199019160365957122, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:29', 'admin', NULL, '2024-01-31 23:05:29', NULL, 0);
INSERT INTO `sys_message` VALUES (636, 1000000000000000001, 'admin', 1199019160365957123, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:29', 'admin', NULL, '2024-01-31 23:05:29', NULL, 0);
INSERT INTO `sys_message` VALUES (637, 1000000000000000001, 'admin', 1199019160365957124, '大家都不用上班了以后', '111', 0, 1000000000000000001, '2024-01-31 23:05:29', 'admin', NULL, '2024-01-31 23:05:29', NULL, 0);
INSERT INTO `sys_message` VALUES (638, 1167805277462855680, 'mars', 1000000000000000001, '请假表单代办', '请假表单代办', 1, 1167805277462855680, '2024-02-01 22:42:53', 'mars', 1000000000000000001, '2024-02-04 09:24:02', 'admin', 0);
INSERT INTO `sys_message` VALUES (639, 1167805277462855680, 'mars', 1167805277462855680, '请假表单代办', '请假表单代办', 1, 1167805277462855680, '2024-02-01 22:42:58', 'mars', 1167805277462855680, '2024-02-01 22:58:53', 'mars', 0);
INSERT INTO `sys_message` VALUES (640, 1167805277462855680, 'mars', 1170050787619897344, '请假表单代办', '请假表单代办', 1, 1167805277462855680, '2024-02-01 22:42:58', 'mars', 1170050787619897344, '2024-02-01 23:03:54', 'lisi', 0);
INSERT INTO `sys_message` VALUES (641, 1167805277462855680, 'mars', 1199019160365957121, '请假表单代办', '请假表单代办', 0, 1167805277462855680, '2024-02-01 22:42:58', 'mars', NULL, '2024-02-01 22:42:59', NULL, 0);
INSERT INTO `sys_message` VALUES (642, 1167805277462855680, 'mars', 1199019160365957122, '请假表单代办', '请假表单代办', 0, 1167805277462855680, '2024-02-01 22:42:58', 'mars', NULL, '2024-02-01 22:42:59', NULL, 0);
INSERT INTO `sys_message` VALUES (643, 1167805277462855680, 'mars', 1199019160365957123, '请假表单代办', '请假表单代办', 0, 1167805277462855680, '2024-02-01 22:42:58', 'mars', NULL, '2024-02-01 22:42:59', NULL, 0);
INSERT INTO `sys_message` VALUES (644, 1167805277462855680, 'mars', 1199019160365957124, '请假表单代办', '请假表单代办', 0, 1167805277462855680, '2024-02-01 22:42:58', 'mars', NULL, '2024-02-01 22:42:59', NULL, 0);
INSERT INTO `sys_message` VALUES (645, 1170050787619897344, 'lisi', 1000000000000000001, '请假表单代办', '请假表单代办', 1, 1170050787619897344, '2024-02-02 09:35:54', 'lisi', 1000000000000000001, '2024-02-04 09:24:02', 'admin', 0);
INSERT INTO `sys_message` VALUES (646, 1170050787619897344, 'lisi', 1170050787619897344, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:35:54', 'lisi', NULL, '2024-02-02 09:35:54', NULL, 0);
INSERT INTO `sys_message` VALUES (647, 1170050787619897344, 'lisi', 1199019160365957121, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:35:54', 'lisi', NULL, '2024-02-02 09:35:54', NULL, 0);
INSERT INTO `sys_message` VALUES (648, 1170050787619897344, 'lisi', 1199019160365957122, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:35:54', 'lisi', NULL, '2024-02-02 09:35:54', NULL, 0);
INSERT INTO `sys_message` VALUES (649, 1170050787619897344, 'lisi', 1199019160365957123, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:35:54', 'lisi', NULL, '2024-02-02 09:35:54', NULL, 0);
INSERT INTO `sys_message` VALUES (650, 1170050787619897344, 'lisi', 1199019160365957124, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:35:54', 'lisi', NULL, '2024-02-02 09:35:54', NULL, 0);
INSERT INTO `sys_message` VALUES (651, 1170050787619897344, 'lisi', 1000000000000000001, '请假表单代办', '请假表单代办', 1, 1170050787619897344, '2024-02-02 09:39:06', 'lisi', 1000000000000000001, '2024-02-04 09:24:02', 'admin', 0);
INSERT INTO `sys_message` VALUES (652, 1170050787619897344, 'lisi', 1170050787619897344, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:39:06', 'lisi', NULL, '2024-02-02 09:39:06', NULL, 0);
INSERT INTO `sys_message` VALUES (653, 1170050787619897344, 'lisi', 1199019160365957121, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:39:06', 'lisi', NULL, '2024-02-02 09:39:06', NULL, 0);
INSERT INTO `sys_message` VALUES (654, 1170050787619897344, 'lisi', 1199019160365957122, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:39:06', 'lisi', NULL, '2024-02-02 09:39:06', NULL, 0);
INSERT INTO `sys_message` VALUES (655, 1170050787619897344, 'lisi', 1199019160365957123, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:39:06', 'lisi', NULL, '2024-02-02 09:39:06', NULL, 0);
INSERT INTO `sys_message` VALUES (656, 1170050787619897344, 'lisi', 1199019160365957124, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:39:06', 'lisi', NULL, '2024-02-02 09:39:06', NULL, 0);
INSERT INTO `sys_message` VALUES (657, 1170050787619897344, 'lisi', 1000000000000000001, '请假表单代办', '请假表单代办', 1, 1170050787619897344, '2024-02-02 09:44:30', 'lisi', 1000000000000000001, '2024-02-04 09:24:01', 'admin', 0);
INSERT INTO `sys_message` VALUES (658, 1170050787619897344, 'lisi', 1170050787619897344, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:44:30', 'lisi', NULL, '2024-02-02 09:44:30', NULL, 0);
INSERT INTO `sys_message` VALUES (659, 1170050787619897344, 'lisi', 1199019160365957121, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:44:30', 'lisi', NULL, '2024-02-02 09:44:30', NULL, 0);
INSERT INTO `sys_message` VALUES (660, 1170050787619897344, 'lisi', 1199019160365957122, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:44:30', 'lisi', NULL, '2024-02-02 09:44:30', NULL, 0);
INSERT INTO `sys_message` VALUES (661, 1170050787619897344, 'lisi', 1199019160365957123, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:44:30', 'lisi', NULL, '2024-02-02 09:44:30', NULL, 0);
INSERT INTO `sys_message` VALUES (662, 1170050787619897344, 'lisi', 1199019160365957124, '请假表单代办', '请假表单代办', 0, 1170050787619897344, '2024-02-02 09:44:30', 'lisi', NULL, '2024-02-02 09:44:30', NULL, 0);
INSERT INTO `sys_message` VALUES (663, 1170050787619897344, 'lisi', 1167805277462855680, '请假表单代办', '请假表单代办', 1, 1170050787619897344, '2024-02-02 09:45:55', 'lisi', 1167805277462855680, '2024-02-02 09:58:01', 'mars', 0);
INSERT INTO `sys_message` VALUES (664, 1170050787619897344, 'lisi', 1167805277462855680, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 10:01:23', 'lisi', NULL, '2024-02-02 10:01:23', NULL, 0);
INSERT INTO `sys_message` VALUES (665, 1167805277462855680, 'mars', 1000000000000000001, '请假表单', '【代办】:请假表单', 1, 1167805277462855680, '2024-02-02 10:02:51', 'mars', 1000000000000000001, '2024-02-04 09:24:01', 'admin', 0);
INSERT INTO `sys_message` VALUES (666, 1000000000000000001, 'admin', 1170050787619897344, '请假表单审批已完成', '【通知】:请假表单审批已完成', 0, 1000000000000000001, '2024-02-02 10:04:06', 'admin', NULL, '2024-02-02 10:04:06', NULL, 0);
INSERT INTO `sys_message` VALUES (667, 1199019160365957122, 'wangwu1', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1199019160365957122, '2024-02-02 18:34:44', 'wangwu1', NULL, '2024-02-02 18:34:45', NULL, 0);
INSERT INTO `sys_message` VALUES (668, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 18:38:02', 'lisi', NULL, '2024-02-02 18:38:02', NULL, 0);
INSERT INTO `sys_message` VALUES (669, 1199019160365957122, 'wangwu1', 1199019160365957123, '请假表单', '【代办】:请假表单', 0, 1199019160365957122, '2024-02-02 18:39:00', 'wangwu1', NULL, '2024-02-02 18:39:01', NULL, 0);
INSERT INTO `sys_message` VALUES (670, 1199019160365957123, 'wangwu2', 1167805277462855680, '请假表单', '【代办】:请假表单', 0, 1199019160365957123, '2024-02-02 18:40:06', 'wangwu2', NULL, '2024-02-02 18:40:07', NULL, 0);
INSERT INTO `sys_message` VALUES (671, 1167805277462855680, 'mars', 1170050787619897344, '请假表单审批已完成', '【通知】:请假表单审批已完成', 0, 1167805277462855680, '2024-02-02 18:40:50', 'mars', NULL, '2024-02-02 18:40:51', NULL, 0);
INSERT INTO `sys_message` VALUES (672, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 18:42:40', 'lisi', NULL, '2024-02-02 18:42:41', NULL, 0);
INSERT INTO `sys_message` VALUES (673, 1199019160365957122, 'wangwu1', 1199019160365957123, '请假表单', '【代办】:请假表单', 0, 1199019160365957122, '2024-02-02 18:43:32', 'wangwu1', NULL, '2024-02-02 18:43:32', NULL, 0);
INSERT INTO `sys_message` VALUES (674, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 23:04:21', 'lisi', NULL, '2024-02-02 23:04:21', NULL, 0);
INSERT INTO `sys_message` VALUES (675, 1199019160365957122, 'wangwu1', 1199019160365957123, '请假表单', '【代办】:请假表单', 0, 1199019160365957122, '2024-02-02 23:08:11', 'wangwu1', NULL, '2024-02-02 23:08:11', NULL, 0);
INSERT INTO `sys_message` VALUES (676, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 23:13:21', 'lisi', NULL, '2024-02-02 23:13:21', NULL, 0);
INSERT INTO `sys_message` VALUES (677, 1199019160365957122, 'wangwu1', 1199019160365957123, '请假表单', '【代办】:请假表单', 0, 1199019160365957122, '2024-02-02 23:17:59', 'wangwu1', NULL, '2024-02-02 23:17:59', NULL, 0);
INSERT INTO `sys_message` VALUES (678, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 23:21:11', 'lisi', NULL, '2024-02-02 23:21:11', NULL, 0);
INSERT INTO `sys_message` VALUES (679, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 23:47:29', 'lisi', NULL, '2024-02-02 23:47:29', NULL, 0);
INSERT INTO `sys_message` VALUES (680, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-02 23:59:10', 'lisi', NULL, '2024-02-02 23:59:10', NULL, 0);
INSERT INTO `sys_message` VALUES (681, 1199019160365957122, 'wangwu1', 1199019160365957123, '请假表单', '【代办】:请假表单', 0, 1199019160365957122, '2024-02-03 00:01:12', 'wangwu1', NULL, '2024-02-03 00:01:12', NULL, 0);
INSERT INTO `sys_message` VALUES (682, 1199019160365957123, 'wangwu2', 1167805277462855680, '请假表单', '【代办】:请假表单', 0, 1199019160365957123, '2024-02-03 00:02:23', 'wangwu2', NULL, '2024-02-03 00:02:23', NULL, 0);
INSERT INTO `sys_message` VALUES (683, 1167805277462855680, 'mars', 1170050787619897344, '请假表单审批已完成', '【通知】:请假表单审批已完成', 0, 1167805277462855680, '2024-02-03 00:03:16', 'mars', NULL, '2024-02-03 00:03:16', NULL, 0);
INSERT INTO `sys_message` VALUES (684, 1170050787619897344, 'lisi', 1199019160365957122, '请假审批表单', '【代办】:请假审批表单', 0, 1170050787619897344, '2024-02-03 00:04:21', 'lisi', NULL, '2024-02-03 00:04:21', NULL, 0);
INSERT INTO `sys_message` VALUES (685, 1199019160365957122, 'wangwu1', 1167805277462855680, '请假表单', '【代办】:请假表单', 0, 1199019160365957122, '2024-02-03 00:04:54', 'wangwu1', NULL, '2024-02-03 00:04:54', NULL, 0);
INSERT INTO `sys_message` VALUES (686, 1167805277462855680, 'mars', 1170050787619897344, '请假表单审批已完成', '【通知】:请假表单审批已完成', 0, 1167805277462855680, '2024-02-03 00:05:38', 'mars', NULL, '2024-02-03 00:05:38', NULL, 0);
INSERT INTO `sys_message` VALUES (687, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-02-04 09:22:57', 'admin', 1000000000000000001, '2024-02-04 09:24:01', 'admin', 0);
INSERT INTO `sys_message` VALUES (688, 1000000000000000001, 'admin', 1000000000000000001, '大家都不用上班了以后', '111', 1, 1000000000000000001, '2024-02-04 09:22:59', 'admin', 1000000000000000001, '2024-02-04 09:24:01', 'admin', 0);
INSERT INTO `sys_message` VALUES (689, 1000000000000000001, 'admin', 1000000000000000001, '放年假', '放年假', 1, 1000000000000000001, '2024-02-04 09:23:42', 'admin', 1000000000000000001, '2024-02-04 09:24:01', 'admin', 0);
INSERT INTO `sys_message` VALUES (690, 1000000000000000001, 'admin', 1000000000000000001, '放年假', '放年假', 1, 1000000000000000001, '2024-02-04 09:23:46', 'admin', 1000000000000000001, '2024-02-04 09:24:01', 'admin', 0);
INSERT INTO `sys_message` VALUES (691, 1000000000000000001, 'admin', 1000000000000000001, '放年假', '放年假', 1, 1000000000000000001, '2024-02-04 09:23:47', 'admin', 1000000000000000001, '2024-02-04 09:24:00', 'admin', 0);
INSERT INTO `sys_message` VALUES (692, 1000000000000000001, 'admin', 1000000000000000001, '放年假', '放年假', 1, 1000000000000000001, '2024-02-04 09:23:50', 'admin', 1000000000000000001, '2024-02-04 09:24:00', 'admin', 0);
INSERT INTO `sys_message` VALUES (693, 1000000000000000001, 'admin', 1000000000000000001, '放年假', '放年假', 1, 1000000000000000001, '2024-02-04 09:24:07', 'admin', 1000000000000000001, '2024-02-05 03:24:40', 'admin', 0);
INSERT INTO `sys_message` VALUES (694, 1000000000000000001, 'admin', 1000000000000000001, '放年假', '放年假', 1, 1000000000000000001, '2024-02-04 09:24:10', 'admin', 1000000000000000001, '2024-02-05 03:24:39', 'admin', 0);

-- ----------------------------
-- Table structure for sys_notify
-- ----------------------------
DROP TABLE IF EXISTS `sys_notify`;
CREATE TABLE `sys_notify`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标题',
  `type` tinyint NULL DEFAULT NULL COMMENT '类型',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '内容',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notify
-- ----------------------------
INSERT INTO `sys_notify` VALUES (1, '测试标题', 2, '测试内容1122211', NULL, 1000000000000000001, '2023-12-06 10:12:09', 'admin', 1000000000000000001, '2023-12-06 13:12:17', 'admin', 1);
INSERT INTO `sys_notify` VALUES (2, '12', 1, '2222', NULL, NULL, '2023-12-06 10:21:18', NULL, NULL, '2023-12-06 13:12:13', NULL, 1);
INSERT INTO `sys_notify` VALUES (3, '111', 1, '222', '2323', 1000000000000000001, '2023-12-06 13:12:05', 'admin', NULL, '2023-12-06 13:12:06', NULL, 0);
INSERT INTO `sys_notify` VALUES (4, '111', 1, '222', '2323', 1000000000000000001, '2023-12-06 13:12:05', 'admin', NULL, '2023-12-06 13:12:21', NULL, 1);
INSERT INTO `sys_notify` VALUES (5, '111', 2, '大家都不用上班了以后', '通知内容', 1000000000000000001, '2023-12-06 13:40:20', 'admin', 1000000000000000001, '2023-12-22 23:51:09', NULL, 0);
INSERT INTO `sys_notify` VALUES (6, '放年假', 2, '放年假', '放年假', 1000000000000000001, '2024-02-04 09:23:26', 'admin', 1000000000000000001, '2024-02-04 09:23:38', NULL, 0);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2087 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (2069, '菜单删除', 3, 'com.mars.module.system.controller.SysMenuController.delete()', 'POST', 1, 'admin', '', '/sys/menu/delete/1171101653676327960', '117.136.63.189', '四川省', '1171101653676327960', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-02-05T10:58:03.062\"}', 0, '', '2024-02-05 10:58:04');
INSERT INTO `sys_oper_log` VALUES (2070, '菜单删除', 3, 'com.mars.module.system.controller.SysMenuController.delete()', 'POST', 1, 'admin', '', '/sys/menu/delete/1171101653676327959', '117.136.63.189', '四川省', '1171101653676327959', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-02-05T10:58:25.497\"}', 0, '', '2024-02-05 10:58:26');
INSERT INTO `sys_oper_log` VALUES (2071, '菜单删除', 3, 'com.mars.module.system.controller.SysMenuController.delete()', 'POST', 1, 'admin', '', '/sys/menu/delete/1164976341993390080', '223.104.220.221', '四川省成都市', '1164976341993390080', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-02-26T14:30:29.732\"}', 0, '', '2024-02-26 06:30:31');
INSERT INTO `sys_oper_log` VALUES (2072, '修改文件存储', 2, 'com.mars.module.admin.controller.SysOssController.edit()', 'POST', 1, 'admin', '', '/admin/sysOss/update', '182.150.44.245', '四川省成都市武侯区', '{\"bucketName\":\"mars\",\"endpoint\":\"oss-cn-beijing.aliyuncs.com\",\"secretKey\":\"XKdCrtUAAZNwEU3HFUZZEZoYgxKvhv\",\"accessKey\":\"LTAI5tNLNQ3GnhvTs7YMEq8Q\",\"createTime\":\"2023-11-20T22:46:41\",\"pageNo\":1,\"name\":\"alioss\",\"pageSize\":10,\"id\":1}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-02-29T14:05:16.486\"}', 0, '', '2024-02-29 06:05:16');
INSERT INTO `sys_oper_log` VALUES (2073, '修改文件存储', 2, 'com.mars.module.admin.controller.SysOssController.edit()', 'POST', 1, 'admin', '', '/admin/sysOss/update', '182.150.44.245', '四川省成都市武侯区', '{\"bucketName\":\"mars-factory\",\"endpoint\":\"oss-cn-beijing.aliyuncs.com\",\"secretKey\":\"XKdCrtUAAZNwEU3HFUZZEZoYgxKvhv\",\"accessKey\":\"LTAI5tNLNQ3GnhvTs7YMEq8Q\",\"createTime\":\"2023-11-20T22:46:41\",\"pageNo\":1,\"name\":\"alioss\",\"pageSize\":10,\"id\":1}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-02-29T14:05:30.261\"}', 0, '', '2024-02-29 06:05:30');
INSERT INTO `sys_oper_log` VALUES (2074, '新增文件存储', 1, 'com.mars.module.admin.controller.SysOssController.add()', 'POST', 1, 'admin', '', '/admin/sysOss/add', '182.150.44.245', '四川省成都市武侯区', '{\"bucketName\":\"mars\",\"endpoint\":\"http://211.101.239.172:9000\",\"secretKey\":\"ERAwMU9WHIstkcfMcT1p3lrrOE9kEBYffH3g4bfQ\",\"accessKey\":\"BbvpqWDBrMXyrcTfR1Nc\",\"pageNo\":1,\"name\":\"minio\",\"pageSize\":10}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-02-29T14:06:02.759\"}', 0, '', '2024-02-29 06:06:03');
INSERT INTO `sys_oper_log` VALUES (2075, '菜单删除', 3, 'com.mars.module.system.controller.SysMenuController.delete()', 'POST', 1, 'admin', '', '/sys/menu/delete/1171101653676327914', '223.104.214.205', '四川省', '1171101653676327914', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-04T16:30:05.444\"}', 0, '', '2024-03-04 08:30:06');
INSERT INTO `sys_oper_log` VALUES (2076, '菜单删除', 3, 'com.mars.module.system.controller.SysMenuController.delete()', 'POST', 1, 'admin', '', '/sys/menu/delete/1171101653676327917', '223.104.214.205', '四川省', '1171101653676327917', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-04T16:30:09.071\"}', 0, '', '2024-03-04 08:30:09');
INSERT INTO `sys_oper_log` VALUES (2077, '修改文件存储', 2, 'com.mars.module.admin.controller.SysOssController.edit()', 'POST', 1, 'admin', '', '/admin/sysOss/update', '223.104.214.205', '四川省', '{\"bucketName\":\"mars\",\"endpoint\":\"oss-cn-beijing.aliyuncs.com\",\"secretKey\":\"XKdCrtUAAZNwEU3HFUZZEZoYgxKvhv\",\"accessKey\":\"LTAI5tNLNQ3GnhvTs7YMEq8Q\",\"createTime\":\"2023-11-20T22:46:41\",\"pageNo\":1,\"name\":\"alioss\",\"pageSize\":10,\"id\":1}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-04T16:33:17.465\"}', 0, '', '2024-03-04 08:33:18');
INSERT INTO `sys_oper_log` VALUES (2078, '角色修改', 2, 'com.mars.module.system.controller.SysRoleController.update()', 'POST', 1, 'admin', '', '/sys/role/update', '101.207.235.42', '四川省成都市武侯区', '{\"roleName\":\"超级管理员\",\"menuId\":[1143582605195608064,1143583807417352192,1000000000000003001,1000000000000003011,1171101653676327841,1000000000000003111,1000000000000003112,1000000000000003113,1000000000000003114,1000000000000003115,1000000000000003116,1000000000000003012,1000000000000003211,1000000000000003212,1000000000000003213,1000000000000003214,1000000000000003013,1000000000000003311,1000000000000003312,1000000000000003313,1000000000000003314,1170141074442682368,1170141074442682369,1170141074442682370,1170141074442682371,1170141074442682372,1000000000000003003,1171101653676327249,1171101653676327252,1171101653676327251,1171101653676327244,1171101653676327247,1171101653676327246,1000000000000003002,1000000000000003021,1000000000000003421,1000000000000003422,1171101653676326936,1171101653676327177,1171101653676326938,1171101653676327446,1171101653676327296,1171101653676327441,1171101653676327442,1171101653676327443,1171101653676327444,1171101653676327445,1171101653676327436,1171101653676327440,1171101653676327439,1171101653676327438,1171101653676327437,1171101653676327314,1171101653676327316,1171101653676327315,1171101653676327317,1171101653676327318,1171101653676327308,1171101653676327312,1171101653676327311,1171101653676327310,1171101653676327309,1171101653676327674,1171101653676327680,1171101653676327681,1171101653676327682,1171101653676327683,1171101653676327684,1171101653676327675,1171101653676327679,1171101653676327678,1171101653676327677,1171101653676327676],\"remark\":\"拥有所有权限\",\"id\":1052613170658541568}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-05T13:18:01.006\"}', 0, '', '2024-03-05 05:18:01');
INSERT INTO `sys_oper_log` VALUES (2079, '用户修改', 2, 'com.mars.module.system.controller.SysUserController.update()', 'POST', 1, 'admin', '', '/sys/user/update', '182.150.44.245', '四川省成都市武侯区', '{\"address\":\"成都市\",\"roleId\":[1052613170658541568],\"sex\":2,\"avatar\":\"https://mars-factory.oss-cn-beijing.aliyuncs.com/2023/12/27/fdf5a0c21dc64830b16f84607d631e9dlogo.jpg\",\"postId\":1,\"userName\":\"admin\",\"birthDate\":\"2014-12-15\",\"realName\":\"管理员\",\"phone\":\"18888888888\",\"id\":1000000000000000001}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-05T17:42:07.229\"}', 0, '', '2024-03-05 09:42:07');
INSERT INTO `sys_oper_log` VALUES (2080, '用户修改', 2, 'com.mars.module.system.controller.SysUserController.update()', 'POST', 1, 'admin', '', '/sys/user/update', '182.150.44.245', '四川省成都市武侯区', '{\"address\":\"成都市\",\"roleId\":[1052613170658541568],\"sex\":2,\"avatar\":\"https://mars-factory.oss-cn-beijing.aliyuncs.com/2023/12/27/fdf5a0c21dc64830b16f84607d631e9dlogo.jpg\",\"postId\":1,\"userName\":\"admin\",\"birthDate\":\"2014-12-15\",\"realName\":\"管理员\",\"phone\":\"18888888888\",\"id\":1000000000000000001}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-05T17:46:00.094\"}', 0, '', '2024-03-05 09:46:00');
INSERT INTO `sys_oper_log` VALUES (2081, '用户修改', 2, 'com.mars.module.system.controller.SysUserController.update()', 'POST', 1, 'admin', '', '/sys/user/update', '101.207.235.42', '四川省成都市武侯区', '{\"address\":\"成都市\",\"roleId\":[1052613170658541568,1052613468324102144],\"sex\":2,\"avatar\":\"https://mars-factory.oss-cn-beijing.aliyuncs.com/2023/12/27/fdf5a0c21dc64830b16f84607d631e9dlogo.jpg\",\"postId\":1,\"userName\":\"admin\",\"birthDate\":\"2014-12-15\",\"realName\":\"管理员\",\"phone\":\"18888888888\",\"id\":1000000000000000001}', 'null', 1, '', '2024-03-05 09:48:46');
INSERT INTO `sys_oper_log` VALUES (2082, '用户修改', 2, 'com.mars.module.system.controller.SysUserController.update()', 'POST', 1, 'admin', '', '/sys/user/update', '101.207.235.42', '四川省成都市武侯区', '{\"address\":\"成都市\",\"roleId\":[1052613170658541568,1052613468324102144],\"sex\":2,\"avatar\":\"https://mars-factory.oss-cn-beijing.aliyuncs.com/2023/12/27/fdf5a0c21dc64830b16f84607d631e9dlogo.jpg\",\"postId\":1,\"userName\":\"admin\",\"birthDate\":\"2014-12-15\",\"realName\":\"管理员\",\"phone\":\"18888888888\",\"id\":1000000000000000001}', 'null', 1, '', '2024-03-05 09:48:49');
INSERT INTO `sys_oper_log` VALUES (2083, '用户修改', 2, 'com.mars.module.system.controller.SysUserController.update()', 'POST', 1, 'admin', '', '/sys/user/update', '101.207.235.42', '四川省成都市武侯区', '{\"address\":\"成都市\",\"roleId\":[1052613170658541568],\"sex\":2,\"avatar\":\"https://mars-factory.oss-cn-beijing.aliyuncs.com/2023/12/27/fdf5a0c21dc64830b16f84607d631e9dlogo.jpg\",\"postId\":1,\"userName\":\"admin\",\"birthDate\":\"2014-12-15\",\"realName\":\"管理员\",\"phone\":\"18888888888\",\"id\":1000000000000000001}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-05T17:48:52.162\"}', 0, '', '2024-03-05 09:48:52');
INSERT INTO `sys_oper_log` VALUES (2084, '角色修改', 2, 'com.mars.module.system.controller.SysRoleController.update()', 'POST', 1, 'admin', '', '/sys/role/update', '101.207.235.42', '四川省成都市武侯区', '{\"roleName\":\"超级管理员\",\"menuId\":[1143582605195608064,1143583807417352192,1000000000000003001,1000000000000003011,1171101653676327841,1000000000000003111,1000000000000003112,1000000000000003113,1000000000000003114,1000000000000003115,1000000000000003116,1000000000000003012,1000000000000003211,1000000000000003212,1000000000000003213,1000000000000003214,1000000000000003013,1000000000000003311,1000000000000003312,1000000000000003313,1000000000000003314,1170141074442682368,1170141074442682369,1170141074442682370,1170141074442682371,1170141074442682372,1000000000000003003,1171101653676327249,1171101653676327253,1171101653676327252,1171101653676327251,1171101653676327250,1171101653676327244,1171101653676327248,1171101653676327247,1171101653676327246,1171101653676327245,1000000000000003002,1000000000000003021,1000000000000003421,1000000000000003422,1171101653676326936,1171101653676327177,1171101653676326938,1171101653676327446,1171101653676327296,1171101653676327441,1171101653676327442,1171101653676327443,1171101653676327444,1171101653676327445,1171101653676327436,1171101653676327440,1171101653676327439,1171101653676327438,1171101653676327437,1171101653676327314,1171101653676327316,1171101653676327315,1171101653676327317,1171101653676327318,1171101653676327308,1171101653676327312,1171101653676327311,1171101653676327310,1171101653676327309,1171101653676327674,1171101653676327680,1171101653676327681,1171101653676327682,1171101653676327683,1171101653676327684,1171101653676327675,1171101653676327679,1171101653676327678,1171101653676327677,1171101653676327676],\"remark\":\"拥有所有权限\",\"id\":1052613170658541568}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-05T17:49:10.109\"}', 0, '', '2024-03-05 09:49:10');
INSERT INTO `sys_oper_log` VALUES (2085, '角色修改', 2, 'com.mars.module.system.controller.SysRoleController.update()', 'POST', 1, 'admin', '', '/sys/role/update', '101.207.235.42', '四川省成都市武侯区', '{\"roleName\":\"超级管理员\",\"menuId\":[1143582605195608064,1143583807417352192,1000000000000003001,1000000000000003011,1171101653676327841,1000000000000003111,1000000000000003112,1000000000000003113,1000000000000003114,1000000000000003115,1000000000000003116,1000000000000003012,1000000000000003211,1000000000000003212,1000000000000003213,1000000000000003214,1000000000000003013,1000000000000003311,1000000000000003312,1000000000000003313,1000000000000003314,1170141074442682368,1170141074442682369,1170141074442682370,1170141074442682371,1170141074442682372,1000000000000003002,1000000000000003021,1000000000000003421,1000000000000003422,1171101653676326936,1171101653676327177,1171101653676326938,1171101653676327446,1171101653676327296,1171101653676327441,1171101653676327442,1171101653676327443,1171101653676327444,1171101653676327445,1171101653676327436,1171101653676327440,1171101653676327439,1171101653676327438,1171101653676327437,1171101653676327314,1171101653676327316,1171101653676327315,1171101653676327317,1171101653676327318,1171101653676327308,1171101653676327312,1171101653676327311,1171101653676327310,1171101653676327309,1171101653676327674,1171101653676327680,1171101653676327681,1171101653676327682,1171101653676327683,1171101653676327684,1171101653676327675,1171101653676327679,1171101653676327678,1171101653676327677,1171101653676327676],\"remark\":\"拥有所有权限\",\"id\":1052613170658541568}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-05T17:49:21.188\"}', 0, '', '2024-03-05 09:49:21');
INSERT INTO `sys_oper_log` VALUES (2086, '角色修改', 2, 'com.mars.module.system.controller.SysRoleController.update()', 'POST', 1, 'admin', '', '/sys/role/update', '101.207.235.42', '四川省成都市武侯区', '{\"roleName\":\"超级管理员\",\"menuId\":[1143582605195608064,1143583807417352192,1000000000000003001,1000000000000003011,1171101653676327841,1000000000000003111,1000000000000003112,1000000000000003113,1000000000000003114,1000000000000003115,1000000000000003116,1000000000000003012,1000000000000003211,1000000000000003212,1000000000000003213,1000000000000003214,1000000000000003013,1000000000000003311,1000000000000003312,1000000000000003313,1000000000000003314,1170141074442682368,1170141074442682369,1170141074442682370,1170141074442682371,1170141074442682372,1000000000000003003,1171101653676327249,1171101653676327253,1171101653676327252,1171101653676327251,1171101653676327250,1171101653676327244,1171101653676327248,1171101653676327247,1171101653676327246,1171101653676327245,1000000000000003002,1000000000000003021,1000000000000003421,1000000000000003422,1171101653676326936,1171101653676327177,1171101653676326938,1171101653676327446,1171101653676327685,1171101653676327686,1171101653676327296,1171101653676327441,1171101653676327442,1171101653676327443,1171101653676327444,1171101653676327445,1171101653676327436,1171101653676327440,1171101653676327439,1171101653676327438,1171101653676327437,1171101653676327314,1171101653676327316,1171101653676327315,1171101653676327317,1171101653676327318,1171101653676327308,1171101653676327312,1171101653676327311,1171101653676327310,1171101653676327309,1171101653676327674,1171101653676327680,1171101653676327681,1171101653676327682,1171101653676327683,1171101653676327684,1171101653676327675,1171101653676327679,1171101653676327678,1171101653676327677,1171101653676327676],\"remark\":\"拥有所有权限\",\"id\":1052613170658541568}', '{\"code\":\"200\",\"message\":\"成功\",\"timestamp\":\"2024-03-05T17:49:34.065\"}', 0, '', '2024-03-05 09:49:34');

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `endpoint` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'endpoint',
  `access_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'access_key',
  `secret_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'secret_key',
  `bucket_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'bucket_Name',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文件存储表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------
INSERT INTO `sys_oss` VALUES (1, 'alioss', 'oss-cn-beijing.aliyuncs.com', 'LTAI5tNLNQ3GnhvTs7YMEq8Q', 'XKdCrtUAAZNwEU3HFUZZEZoYgxKvhv', 'mars', 1000000000000000001, '2023-11-20 22:46:41', 'admin', 1000000000000000001, '2024-03-04 16:33:17', 'admin', 0);
INSERT INTO `sys_oss` VALUES (2, 'minio-oss', 'http://43.139.86.39:9000', 'Ro3FE0JH7A0ke1LJ26li', 'BmeOH5QmtrFW2ew08QDb25rDh0lA4LJkVmRovU9j', 'mars', 1000000000000000001, '2023-11-20 22:47:38', 'admin', NULL, '2023-11-21 14:55:06', NULL, 1);
INSERT INTO `sys_oss` VALUES (3, 'minio', 'http://211.101.239.172:9000', 'BbvpqWDBrMXyrcTfR1Nc', 'ERAwMU9WHIstkcfMcT1p3lrrOE9kEBYffH3g4bfQ', 'mars', 1000000000000000001, '2024-02-29 06:06:02', 'admin', NULL, '2024-02-29 06:06:02', NULL, 0);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `post_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
  `post_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '岗位编码',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, '董事长', '00002', '董事长', NULL, NULL, '2023-11-03 23:43:36', 1000000000000000001, 'admin', '2023-11-07 16:02:27', 0);
INSERT INTO `sys_post` VALUES (2, '技术总监', 'yuanmazijie-002', NULL, 1000000000000000001, 'admin', '2023-11-03 23:54:38', NULL, NULL, '2023-11-03 23:58:30', 1);
INSERT INTO `sys_post` VALUES (3, '测试岗位', 'yuanmazjie-003', NULL, 1000000000000000001, 'admin', '2023-11-03 23:56:16', NULL, NULL, '2023-11-03 23:58:26', 1);
INSERT INTO `sys_post` VALUES (4, '总经理', 'yuanmajie-002', '总经理111', 1000000000000000001, 'admin', '2023-11-03 23:59:06', 1000000000000000001, 'admin', '2023-11-04 00:09:11', 1);
INSERT INTO `sys_post` VALUES (5, '测试岗位', '0001', '11111', 1000000000000000001, 'admin', '2023-11-04 00:13:34', 1000000000000000001, 'admin', '2023-12-08 10:50:27', 0);
INSERT INTO `sys_post` VALUES (6, '22', '111', '21212', 1000000000000000001, 'admin', '2023-11-04 21:45:38', NULL, NULL, '2023-11-04 21:45:43', 1);
INSERT INTO `sys_post` VALUES (7, '1212', '1212', '2222', 1000000000000000001, 'admin', '2023-11-04 21:46:04', NULL, NULL, '2023-11-04 21:46:29', 1);
INSERT INTO `sys_post` VALUES (8, '2122', '212', '1212', 1000000000000000001, 'admin', '2023-11-04 21:46:23', NULL, NULL, '2023-11-04 21:46:32', 1);
INSERT INTO `sys_post` VALUES (9, '23', '12', '23', 1000000000000000001, 'admin', '2024-01-09 13:31:39', NULL, NULL, '2024-01-09 13:32:06', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `role_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色key',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1203042721313325057 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1052613170658541568, '超级管理员', 'super_admin', '拥有所有权限', NULL, NULL, '2022-12-14 15:48:56', 1000000000000000001, 'admin', '2024-03-05 17:49:34', 0);
INSERT INTO `sys_role` VALUES (1052613468324102144, '普通用户', 'common', '查看数据采集和日志', NULL, NULL, '2022-12-14 15:50:07', 1000000000000000001, 'admin', '2024-01-31 14:34:39', 0);
INSERT INTO `sys_role` VALUES (1170027725386153984, '测试角色', NULL, '111', NULL, NULL, '2023-11-03 15:52:28', NULL, NULL, '2023-11-03 15:52:28', 1);
INSERT INTO `sys_role` VALUES (1170029865148088320, '21212', NULL, '21212', NULL, NULL, '2023-11-03 16:00:58', NULL, NULL, '2023-11-03 16:00:58', 1);
INSERT INTO `sys_role` VALUES (1170034943355518976, '1212222', NULL, '212', 1000000000000000001, 'admin', '2023-11-03 16:21:09', 1000000000000000001, 'admin', '2023-11-03 16:21:09', 1);
INSERT INTO `sys_role` VALUES (1170048879480012800, '测试角色', NULL, '测试角色', 1000000000000000001, 'admin', '2023-11-03 17:16:31', NULL, NULL, '2023-11-03 17:16:31', 1);
INSERT INTO `sys_role` VALUES (1170050863721349120, '测试角色', NULL, '测试人员', 1000000000000000001, 'admin', '2023-11-03 17:24:24', 1000000000000000001, 'admin', '2024-01-30 22:16:47', 0);
INSERT INTO `sys_role` VALUES (1200109719943905280, '人事', NULL, '人事', 1000000000000000001, 'admin', '2024-01-25 16:07:34', 1000000000000000001, 'admin', '2024-01-25 18:44:50', 0);
INSERT INTO `sys_role` VALUES (1200132066797158400, '总经理', NULL, '总经理', 1000000000000000001, 'admin', '2024-01-25 17:36:22', NULL, NULL, '2024-01-25 17:36:22', 1);
INSERT INTO `sys_role` VALUES (1200132085600223232, '总经理', NULL, '总经理', 1000000000000000001, 'admin', '2024-01-25 17:36:27', NULL, NULL, '2024-01-25 17:36:27', 1);
INSERT INTO `sys_role` VALUES (1200132103870611456, '总经理', NULL, '总经理', 1000000000000000001, 'admin', '2024-01-25 17:36:31', NULL, NULL, '2024-01-25 17:36:31', 1);
INSERT INTO `sys_role` VALUES (1200132108165578752, '总经理', NULL, '总经理', 1000000000000000001, 'admin', '2024-01-25 17:36:32', NULL, NULL, '2024-01-25 17:36:32', 1);
INSERT INTO `sys_role` VALUES (1200132108530483200, '总经理', NULL, '总经理', 1000000000000000001, 'admin', '2024-01-25 17:36:32', NULL, NULL, '2024-01-25 17:36:32', 1);
INSERT INTO `sys_role` VALUES (1200132109495173120, '总经理', NULL, '总经理', 1000000000000000001, 'admin', '2024-01-25 17:36:32', NULL, NULL, '2024-01-25 17:36:32', 1);
INSERT INTO `sys_role` VALUES (1200132110178844672, '总经理', NULL, '总经理', 1000000000000000001, 'admin', '2024-01-25 17:36:32', 1000000000000000001, 'admin', '2024-01-25 18:44:55', 0);
INSERT INTO `sys_role` VALUES (1203042721313325056, '项目经理', NULL, '项目经理', 1000000000000000001, 'admin', '2024-02-02 18:22:16', NULL, NULL, '2024-02-02 18:22:16', 0);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` bigint UNSIGNED NOT NULL COMMENT '角色ID',
  `menu_id` bigint UNSIGNED NOT NULL COMMENT '菜单ID',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1203042751902384472 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色关联菜单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1200132162918023437, 1200109719943905280, 1000000000000003001, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023438, 1200109719943905280, 1000000000000003011, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023439, 1200109719943905280, 1171101653676327841, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023440, 1200109719943905280, 1000000000000003111, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023441, 1200109719943905280, 1000000000000003112, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023442, 1200109719943905280, 1000000000000003113, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023443, 1200109719943905280, 1000000000000003114, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023444, 1200109719943905280, 1000000000000003115, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023445, 1200109719943905280, 1000000000000003116, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023446, 1200109719943905280, 1000000000000003012, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023447, 1200109719943905280, 1000000000000003211, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023448, 1200109719943905280, 1000000000000003212, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023449, 1200109719943905280, 1000000000000003213, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023450, 1200109719943905280, 1000000000000003214, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023451, 1200109719943905280, 1000000000000003013, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023452, 1200109719943905280, 1000000000000003311, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023453, 1200109719943905280, 1000000000000003312, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023454, 1200109719943905280, 1000000000000003313, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023455, 1200109719943905280, 1000000000000003314, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023456, 1200109719943905280, 1170141074442682368, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023457, 1200109719943905280, 1170141074442682369, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023458, 1200109719943905280, 1170141074442682370, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023459, 1200109719943905280, 1170141074442682371, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023460, 1200109719943905280, 1170141074442682372, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023461, 1200109719943905280, 1000000000000003003, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023462, 1200109719943905280, 1171101653676327249, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023463, 1200109719943905280, 1171101653676327250, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023464, 1200109719943905280, 1171101653676327251, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023465, 1200109719943905280, 1171101653676327252, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023466, 1200109719943905280, 1171101653676327253, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023467, 1200109719943905280, 1171101653676327244, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023468, 1200109719943905280, 1171101653676327245, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023469, 1200109719943905280, 1171101653676327246, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023470, 1200109719943905280, 1171101653676327247, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023471, 1200109719943905280, 1171101653676327248, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023472, 1200109719943905280, 1171101653676327296, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023473, 1200109719943905280, 1171101653676327723, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023474, 1200109719943905280, 1171101653676327724, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023475, 1200109719943905280, 1171101653676327725, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023476, 1200109719943905280, 1171101653676327726, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023477, 1200109719943905280, 1171101653676327727, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023478, 1200109719943905280, 1171101653676327441, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023479, 1200109719943905280, 1171101653676327442, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023480, 1200109719943905280, 1171101653676327443, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023481, 1200109719943905280, 1171101653676327444, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023482, 1200109719943905280, 1171101653676327445, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023483, 1200109719943905280, 1171101653676327436, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023484, 1200109719943905280, 1171101653676327437, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023485, 1200109719943905280, 1171101653676327438, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023486, 1200109719943905280, 1171101653676327439, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023487, 1200109719943905280, 1171101653676327440, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023488, 1200109719943905280, 1171101653676327314, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023489, 1200109719943905280, 1171101653676327315, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023490, 1200109719943905280, 1171101653676327316, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023491, 1200109719943905280, 1171101653676327317, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023492, 1200109719943905280, 1171101653676327318, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023493, 1200109719943905280, 1171101653676327308, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023494, 1200109719943905280, 1171101653676327309, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023495, 1200109719943905280, 1171101653676327310, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023496, 1200109719943905280, 1171101653676327311, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023497, 1200109719943905280, 1171101653676327312, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023498, 1200109719943905280, 1171101653676327674, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023499, 1200109719943905280, 1171101653676327680, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023500, 1200109719943905280, 1171101653676327681, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023501, 1200109719943905280, 1171101653676327682, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023502, 1200109719943905280, 1171101653676327683, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023503, 1200109719943905280, 1171101653676327684, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023504, 1200109719943905280, 1171101653676327675, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023505, 1200109719943905280, 1171101653676327676, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023506, 1200109719943905280, 1171101653676327677, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023507, 1200109719943905280, 1171101653676327678, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023508, 1200109719943905280, 1171101653676327679, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023509, 1200109719943905280, 1171101653676327917, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023510, 1200109719943905280, 1171101653676327918, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023511, 1200109719943905280, 1171101653676327919, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023512, 1200109719943905280, 1171101653676327920, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023513, 1200109719943905280, 1171101653676327921, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023514, 1200109719943905280, 1171101653676327922, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023515, 1200109719943905280, 1171101653676327928, '2024-01-25 18:44:50', '2024-01-25 18:44:50');
INSERT INTO `sys_role_menu` VALUES (1200132162918023516, 1200132110178844672, 1143582605195608064, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023517, 1200132110178844672, 1143583807417352192, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023518, 1200132110178844672, 1000000000000003001, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023519, 1200132110178844672, 1000000000000003011, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023520, 1200132110178844672, 1171101653676327841, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023521, 1200132110178844672, 1000000000000003111, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023522, 1200132110178844672, 1000000000000003112, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023523, 1200132110178844672, 1000000000000003113, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023524, 1200132110178844672, 1000000000000003114, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023525, 1200132110178844672, 1000000000000003115, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023526, 1200132110178844672, 1000000000000003116, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023527, 1200132110178844672, 1000000000000003012, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023528, 1200132110178844672, 1000000000000003211, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023529, 1200132110178844672, 1000000000000003212, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023530, 1200132110178844672, 1000000000000003213, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023531, 1200132110178844672, 1000000000000003214, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023532, 1200132110178844672, 1000000000000003013, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023533, 1200132110178844672, 1000000000000003311, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023534, 1200132110178844672, 1000000000000003312, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023535, 1200132110178844672, 1000000000000003313, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023536, 1200132110178844672, 1000000000000003314, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023537, 1200132110178844672, 1170141074442682368, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023538, 1200132110178844672, 1170141074442682369, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023539, 1200132110178844672, 1170141074442682370, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023540, 1200132110178844672, 1170141074442682371, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023541, 1200132110178844672, 1170141074442682372, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023542, 1200132110178844672, 1164976341993390080, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023543, 1200132110178844672, 1165064540677734400, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023544, 1200132110178844672, 1000000000000003003, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023545, 1200132110178844672, 1171101653676327249, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023546, 1200132110178844672, 1171101653676327250, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023547, 1200132110178844672, 1171101653676327251, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023548, 1200132110178844672, 1171101653676327252, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023549, 1200132110178844672, 1171101653676327253, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023550, 1200132110178844672, 1171101653676327244, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023551, 1200132110178844672, 1171101653676327245, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023552, 1200132110178844672, 1171101653676327246, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023553, 1200132110178844672, 1171101653676327247, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023554, 1200132110178844672, 1171101653676327248, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023555, 1200132110178844672, 1000000000000003002, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023556, 1200132110178844672, 1000000000000003021, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023557, 1200132110178844672, 1000000000000003421, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023558, 1200132110178844672, 1000000000000003422, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023559, 1200132110178844672, 1171101653676326936, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023560, 1200132110178844672, 1171101653676327177, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023561, 1200132110178844672, 1171101653676326938, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023562, 1200132110178844672, 1171101653676327446, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023563, 1200132110178844672, 1171101653676327685, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023564, 1200132110178844672, 1171101653676327686, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023565, 1200132110178844672, 1171101653676327296, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023566, 1200132110178844672, 1171101653676327723, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023567, 1200132110178844672, 1171101653676327724, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023568, 1200132110178844672, 1171101653676327725, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023569, 1200132110178844672, 1171101653676327726, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023570, 1200132110178844672, 1171101653676327727, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023571, 1200132110178844672, 1171101653676327441, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023572, 1200132110178844672, 1171101653676327442, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023573, 1200132110178844672, 1171101653676327443, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023574, 1200132110178844672, 1171101653676327444, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023575, 1200132110178844672, 1171101653676327445, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023576, 1200132110178844672, 1171101653676327436, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023577, 1200132110178844672, 1171101653676327437, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023578, 1200132110178844672, 1171101653676327438, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023579, 1200132110178844672, 1171101653676327439, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023580, 1200132110178844672, 1171101653676327440, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023581, 1200132110178844672, 1171101653676327314, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023582, 1200132110178844672, 1171101653676327315, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023583, 1200132110178844672, 1171101653676327316, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023584, 1200132110178844672, 1171101653676327317, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023585, 1200132110178844672, 1171101653676327318, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023586, 1200132110178844672, 1171101653676327308, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023587, 1200132110178844672, 1171101653676327309, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023588, 1200132110178844672, 1171101653676327310, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023589, 1200132110178844672, 1171101653676327311, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023590, 1200132110178844672, 1171101653676327312, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023591, 1200132110178844672, 1171101653676327674, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023592, 1200132110178844672, 1171101653676327680, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023593, 1200132110178844672, 1171101653676327681, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023594, 1200132110178844672, 1171101653676327682, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023595, 1200132110178844672, 1171101653676327683, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023596, 1200132110178844672, 1171101653676327684, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023597, 1200132110178844672, 1171101653676327675, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023598, 1200132110178844672, 1171101653676327676, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023599, 1200132110178844672, 1171101653676327677, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023600, 1200132110178844672, 1171101653676327678, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023601, 1200132110178844672, 1171101653676327679, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023602, 1200132110178844672, 1171101653676327914, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023603, 1200132110178844672, 1171101653676327915, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023604, 1200132110178844672, 1171101653676327916, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023605, 1200132110178844672, 1171101653676327923, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023606, 1200132110178844672, 1171101653676327924, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023607, 1200132110178844672, 1171101653676327925, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023608, 1200132110178844672, 1171101653676327926, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023609, 1200132110178844672, 1171101653676327927, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023610, 1200132110178844672, 1171101653676327917, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023611, 1200132110178844672, 1171101653676327918, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023612, 1200132110178844672, 1171101653676327919, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023613, 1200132110178844672, 1171101653676327920, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023614, 1200132110178844672, 1171101653676327921, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023615, 1200132110178844672, 1171101653676327922, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023616, 1200132110178844672, 1171101653676327928, '2024-01-25 18:44:56', '2024-01-25 18:44:56');
INSERT INTO `sys_role_menu` VALUES (1200132162918023617, 1200132110178844672, 1171101653676327929, '2024-01-27 01:00:27', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023618, 1200132110178844672, 1171101653676327930, '2024-01-27 01:00:27', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023619, 1200132110178844672, 1171101653676327931, '2024-01-27 01:00:27', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023620, 1200132110178844672, 1171101653676327932, '2024-01-27 01:00:27', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023621, 1200132110178844672, 1171101653676327933, '2024-01-27 01:00:27', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023622, 1200132110178844672, 1171101653676327934, '2024-01-27 01:00:27', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023629, 1200132110178844672, 1171101653676327935, '2024-01-27 01:00:28', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023630, 1200132110178844672, 1171101653676327936, '2024-01-27 01:00:28', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023631, 1200132110178844672, 1171101653676327937, '2024-01-27 01:00:28', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023632, 1200132110178844672, 1171101653676327938, '2024-01-27 01:00:28', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023633, 1200132110178844672, 1171101653676327939, '2024-01-27 01:00:28', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023634, 1200132110178844672, 1171101653676327940, '2024-01-27 01:00:28', NULL);
INSERT INTO `sys_role_menu` VALUES (1200132162918023641, 1170050863721349120, 1000000000000003001, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023642, 1170050863721349120, 1000000000000003011, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023643, 1170050863721349120, 1000000000000003111, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023644, 1170050863721349120, 1000000000000003112, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023645, 1170050863721349120, 1000000000000003113, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023646, 1170050863721349120, 1000000000000003114, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023647, 1170050863721349120, 1000000000000003115, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023648, 1170050863721349120, 1000000000000003116, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023649, 1170050863721349120, 1000000000000003012, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023650, 1170050863721349120, 1000000000000003211, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023651, 1170050863721349120, 1000000000000003212, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023652, 1170050863721349120, 1000000000000003213, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023653, 1170050863721349120, 1000000000000003214, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023654, 1170050863721349120, 1000000000000003013, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023655, 1170050863721349120, 1000000000000003311, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023656, 1170050863721349120, 1000000000000003312, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023657, 1170050863721349120, 1000000000000003313, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023658, 1170050863721349120, 1000000000000003314, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023659, 1170050863721349120, 1170141074442682368, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023660, 1170050863721349120, 1170141074442682369, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023661, 1170050863721349120, 1170141074442682370, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023662, 1170050863721349120, 1170141074442682371, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023663, 1170050863721349120, 1170141074442682372, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023664, 1170050863721349120, 1164976341993390080, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023665, 1170050863721349120, 1165064540677734400, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023666, 1170050863721349120, 1171101653676327917, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023667, 1170050863721349120, 1171101653676327918, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023668, 1170050863721349120, 1171101653676327919, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023669, 1170050863721349120, 1171101653676327920, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023670, 1170050863721349120, 1171101653676327921, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023671, 1170050863721349120, 1171101653676327922, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023672, 1170050863721349120, 1171101653676327928, '2024-01-30 22:16:47', '2024-01-30 22:16:47');
INSERT INTO `sys_role_menu` VALUES (1200132162918023673, 1052613468324102144, 1143582605195608064, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023674, 1052613468324102144, 1143583807417352192, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023675, 1052613468324102144, 1164976341993390080, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023676, 1052613468324102144, 1165064540677734400, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023677, 1052613468324102144, 1000000000000003003, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023678, 1052613468324102144, 1171101653676327249, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023679, 1052613468324102144, 1171101653676327250, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023680, 1052613468324102144, 1171101653676327251, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023681, 1052613468324102144, 1171101653676327252, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023682, 1052613468324102144, 1171101653676327253, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023683, 1052613468324102144, 1171101653676327244, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023684, 1052613468324102144, 1171101653676327245, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023685, 1052613468324102144, 1171101653676327246, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023686, 1052613468324102144, 1171101653676327247, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023687, 1052613468324102144, 1171101653676327248, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023688, 1052613468324102144, 1000000000000003002, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023689, 1052613468324102144, 1000000000000003021, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023690, 1052613468324102144, 1000000000000003421, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023691, 1052613468324102144, 1000000000000003422, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023692, 1052613468324102144, 1171101653676326936, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023693, 1052613468324102144, 1171101653676327177, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023694, 1052613468324102144, 1171101653676326938, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023695, 1052613468324102144, 1171101653676327446, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023696, 1052613468324102144, 1171101653676327685, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023697, 1052613468324102144, 1171101653676327686, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023698, 1052613468324102144, 1171101653676327674, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023699, 1052613468324102144, 1171101653676327680, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023700, 1052613468324102144, 1171101653676327681, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023701, 1052613468324102144, 1171101653676327682, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023702, 1052613468324102144, 1171101653676327683, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023703, 1052613468324102144, 1171101653676327684, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023704, 1052613468324102144, 1171101653676327675, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023705, 1052613468324102144, 1171101653676327676, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023706, 1052613468324102144, 1171101653676327677, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023707, 1052613468324102144, 1171101653676327678, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023708, 1052613468324102144, 1171101653676327679, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023709, 1052613468324102144, 1171101653676327917, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023710, 1052613468324102144, 1171101653676327918, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023711, 1052613468324102144, 1171101653676327919, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023712, 1052613468324102144, 1171101653676327920, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023713, 1052613468324102144, 1171101653676327921, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023714, 1052613468324102144, 1171101653676327922, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1200132162918023715, 1052613468324102144, 1171101653676327928, '2024-01-31 14:34:39', '2024-01-31 14:34:39');
INSERT INTO `sys_role_menu` VALUES (1203042721770504192, 1203042721313325056, 1143582605195608064, '2024-02-02 18:22:16', '2024-02-02 18:22:16');
INSERT INTO `sys_role_menu` VALUES (1203042722219294720, 1203042721313325056, 1143583807417352192, '2024-02-02 18:22:16', '2024-02-02 18:22:16');
INSERT INTO `sys_role_menu` VALUES (1203042722647113728, 1203042721313325056, 1000000000000003001, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042723058155520, 1203042721313325056, 1000000000000003011, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042723481780224, 1203042721313325056, 1171101653676327841, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042723926376448, 1203042721313325056, 1000000000000003111, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042724366778368, 1203042721313325056, 1000000000000003112, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042724790403072, 1203042721313325056, 1000000000000003113, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042725222416384, 1203042721313325056, 1000000000000003114, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042725658624000, 1203042721313325056, 1000000000000003115, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042726078054400, 1203042721313325056, 1000000000000003116, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042726493290496, 1203042721313325056, 1000000000000003012, '2024-02-02 18:22:17', '2024-02-02 18:22:17');
INSERT INTO `sys_role_menu` VALUES (1203042726925303808, 1203042721313325056, 1000000000000003211, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042727365705728, 1203042721313325056, 1000000000000003212, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042727793524736, 1203042721313325056, 1000000000000003213, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042728229732352, 1203042721313325056, 1000000000000003214, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042728649162752, 1203042721313325056, 1000000000000003013, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042729076981760, 1203042721313325056, 1000000000000003311, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042729697738752, 1203042721313325056, 1000000000000003312, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042730129752064, 1203042721313325056, 1000000000000003313, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042730553376768, 1203042721313325056, 1000000000000003314, '2024-02-02 18:22:18', '2024-02-02 18:22:18');
INSERT INTO `sys_role_menu` VALUES (1203042731060887552, 1203042721313325056, 1170141074442682368, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042731488706560, 1203042721313325056, 1170141074442682369, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042731912331264, 1203042721313325056, 1170141074442682370, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042732344344576, 1203042721313325056, 1170141074442682371, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042732780552192, 1203042721313325056, 1170141074442682372, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042733212565504, 1203042721313325056, 1171101653676327296, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042733631995904, 1203042721313325056, 1171101653676327723, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042734059814912, 1203042721313325056, 1171101653676327724, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042734483439616, 1203042721313325056, 1171101653676327725, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042734919647232, 1203042721313325056, 1171101653676327726, '2024-02-02 18:22:19', '2024-02-02 18:22:19');
INSERT INTO `sys_role_menu` VALUES (1203042735343271936, 1203042721313325056, 1171101653676327727, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042735783673856, 1203042721313325056, 1171101653676327441, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042736219881472, 1203042721313325056, 1171101653676327442, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042736651894784, 1203042721313325056, 1171101653676327443, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042737071325184, 1203042721313325056, 1171101653676327444, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042737511727104, 1203042721313325056, 1171101653676327445, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042737943740416, 1203042721313325056, 1171101653676327436, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042738371559424, 1203042721313325056, 1171101653676327437, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042738799378432, 1203042721313325056, 1171101653676327438, '2024-02-02 18:22:20', '2024-02-02 18:22:20');
INSERT INTO `sys_role_menu` VALUES (1203042739294306304, 1203042721313325056, 1171101653676327439, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042739717931008, 1203042721313325056, 1171101653676327440, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042740137361408, 1203042721313325056, 1171101653676327314, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042740611317760, 1203042721313325056, 1171101653676327315, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042741047525376, 1203042721313325056, 1171101653676327316, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042741475344384, 1203042721313325056, 1171101653676327317, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042741898969088, 1203042721313325056, 1171101653676327318, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042742335176704, 1203042721313325056, 1171101653676327308, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042742788161536, 1203042721313325056, 1171101653676327309, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042743228563456, 1203042721313325056, 1171101653676327310, '2024-02-02 18:22:21', '2024-02-02 18:22:21');
INSERT INTO `sys_role_menu` VALUES (1203042743652188160, 1203042721313325056, 1171101653676327311, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042744113561600, 1203042721313325056, 1171101653676327312, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042744537186304, 1203042721313325056, 1171101653676327674, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042744965005312, 1203042721313325056, 1171101653676327680, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042745397018624, 1203042721313325056, 1171101653676327681, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042745850003456, 1203042721313325056, 1171101653676327682, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042746286211072, 1203042721313325056, 1171101653676327683, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042746747584512, 1203042721313325056, 1171101653676327684, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042747179597824, 1203042721313325056, 1171101653676327675, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042747615805440, 1203042721313325056, 1171101653676327676, '2024-02-02 18:22:22', '2024-02-02 18:22:22');
INSERT INTO `sys_role_menu` VALUES (1203042748039430144, 1203042721313325056, 1171101653676327677, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042748471443456, 1203042721313325056, 1171101653676327678, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042748895068160, 1203042721313325056, 1171101653676327679, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042749314498560, 1203042721313325056, 1171101653676327917, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042749733928960, 1203042721313325056, 1171101653676327918, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042750161747968, 1203042721313325056, 1171101653676327919, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042750593761280, 1203042721313325056, 1171101653676327920, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042751025774592, 1203042721313325056, 1171101653676327921, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042751449399296, 1203042721313325056, 1171101653676327922, '2024-02-02 18:22:23', '2024-02-02 18:22:23');
INSERT INTO `sys_role_menu` VALUES (1203042751902384128, 1203042721313325056, 1171101653676327928, '2024-02-02 18:22:24', '2024-02-02 18:22:24');
INSERT INTO `sys_role_menu` VALUES (1203042751902384129, 1200132110178844672, 1171101653676327941, '2024-02-05 04:12:13', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384130, 1200132110178844672, 1171101653676327942, '2024-02-05 04:12:13', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384131, 1200132110178844672, 1171101653676327943, '2024-02-05 04:12:13', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384132, 1200132110178844672, 1171101653676327944, '2024-02-05 04:12:13', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384133, 1200132110178844672, 1171101653676327945, '2024-02-05 04:12:13', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384134, 1200132110178844672, 1171101653676327946, '2024-02-05 04:12:13', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384141, 1200132110178844672, 1171101653676327947, '2024-02-05 04:30:36', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384142, 1200132110178844672, 1171101653676327948, '2024-02-05 04:30:36', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384143, 1200132110178844672, 1171101653676327949, '2024-02-05 04:30:36', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384144, 1200132110178844672, 1171101653676327950, '2024-02-05 04:30:36', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384145, 1200132110178844672, 1171101653676327951, '2024-02-05 04:30:36', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384146, 1200132110178844672, 1171101653676327952, '2024-02-05 04:30:36', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384153, 1200132110178844672, 1171101653676327953, '2024-02-05 04:34:16', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384154, 1200132110178844672, 1171101653676327954, '2024-02-05 04:34:16', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384155, 1200132110178844672, 1171101653676327955, '2024-02-05 04:34:16', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384156, 1200132110178844672, 1171101653676327956, '2024-02-05 04:34:16', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384157, 1200132110178844672, 1171101653676327957, '2024-02-05 04:34:16', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384158, 1200132110178844672, 1171101653676327958, '2024-02-05 04:34:16', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384165, 1200132110178844672, 1171101653676327959, '2024-02-05 04:52:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384166, 1200132110178844672, 1171101653676327960, '2024-02-05 04:52:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384167, 1200132110178844672, 1171101653676327961, '2024-02-05 04:52:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384168, 1200132110178844672, 1171101653676327962, '2024-02-05 04:52:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384169, 1200132110178844672, 1171101653676327963, '2024-02-05 04:52:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384170, 1200132110178844672, 1171101653676327964, '2024-02-05 04:52:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1203042751902384393, 1052613170658541568, 1143582605195608064, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384394, 1052613170658541568, 1143583807417352192, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384395, 1052613170658541568, 1000000000000003001, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384396, 1052613170658541568, 1000000000000003011, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384397, 1052613170658541568, 1171101653676327841, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384398, 1052613170658541568, 1000000000000003111, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384399, 1052613170658541568, 1000000000000003112, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384400, 1052613170658541568, 1000000000000003113, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384401, 1052613170658541568, 1000000000000003114, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384402, 1052613170658541568, 1000000000000003115, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384403, 1052613170658541568, 1000000000000003116, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384404, 1052613170658541568, 1000000000000003012, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384405, 1052613170658541568, 1000000000000003211, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384406, 1052613170658541568, 1000000000000003212, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384407, 1052613170658541568, 1000000000000003213, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384408, 1052613170658541568, 1000000000000003214, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384409, 1052613170658541568, 1000000000000003013, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384410, 1052613170658541568, 1000000000000003311, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384411, 1052613170658541568, 1000000000000003312, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384412, 1052613170658541568, 1000000000000003313, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384413, 1052613170658541568, 1000000000000003314, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384414, 1052613170658541568, 1170141074442682368, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384415, 1052613170658541568, 1170141074442682369, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384416, 1052613170658541568, 1170141074442682370, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384417, 1052613170658541568, 1170141074442682371, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384418, 1052613170658541568, 1170141074442682372, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384419, 1052613170658541568, 1000000000000003003, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384420, 1052613170658541568, 1171101653676327249, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384421, 1052613170658541568, 1171101653676327253, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384422, 1052613170658541568, 1171101653676327252, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384423, 1052613170658541568, 1171101653676327251, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384424, 1052613170658541568, 1171101653676327250, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384425, 1052613170658541568, 1171101653676327244, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384426, 1052613170658541568, 1171101653676327248, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384427, 1052613170658541568, 1171101653676327247, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384428, 1052613170658541568, 1171101653676327246, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384429, 1052613170658541568, 1171101653676327245, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384430, 1052613170658541568, 1000000000000003002, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384431, 1052613170658541568, 1000000000000003021, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384432, 1052613170658541568, 1000000000000003421, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384433, 1052613170658541568, 1000000000000003422, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384434, 1052613170658541568, 1171101653676326936, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384435, 1052613170658541568, 1171101653676327177, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384436, 1052613170658541568, 1171101653676326938, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384437, 1052613170658541568, 1171101653676327446, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384438, 1052613170658541568, 1171101653676327685, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384439, 1052613170658541568, 1171101653676327686, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384440, 1052613170658541568, 1171101653676327296, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384441, 1052613170658541568, 1171101653676327441, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384442, 1052613170658541568, 1171101653676327442, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384443, 1052613170658541568, 1171101653676327443, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384444, 1052613170658541568, 1171101653676327444, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384445, 1052613170658541568, 1171101653676327445, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384446, 1052613170658541568, 1171101653676327436, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384447, 1052613170658541568, 1171101653676327440, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384448, 1052613170658541568, 1171101653676327439, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384449, 1052613170658541568, 1171101653676327438, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384450, 1052613170658541568, 1171101653676327437, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384451, 1052613170658541568, 1171101653676327314, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384452, 1052613170658541568, 1171101653676327316, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384453, 1052613170658541568, 1171101653676327315, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384454, 1052613170658541568, 1171101653676327317, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384455, 1052613170658541568, 1171101653676327318, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384456, 1052613170658541568, 1171101653676327308, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384457, 1052613170658541568, 1171101653676327312, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384458, 1052613170658541568, 1171101653676327311, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384459, 1052613170658541568, 1171101653676327310, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384460, 1052613170658541568, 1171101653676327309, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384461, 1052613170658541568, 1171101653676327674, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384462, 1052613170658541568, 1171101653676327680, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384463, 1052613170658541568, 1171101653676327681, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384464, 1052613170658541568, 1171101653676327682, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384465, 1052613170658541568, 1171101653676327683, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384466, 1052613170658541568, 1171101653676327684, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384467, 1052613170658541568, 1171101653676327675, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384468, 1052613170658541568, 1171101653676327679, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384469, 1052613170658541568, 1171101653676327678, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384470, 1052613170658541568, 1171101653676327677, '2024-03-05 17:49:34', '2024-03-05 17:49:34');
INSERT INTO `sys_role_menu` VALUES (1203042751902384471, 1052613170658541568, 1171101653676327676, '2024-03-05 17:49:34', '2024-03-05 17:49:34');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `real_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `sex` tinyint NOT NULL DEFAULT 1 COMMENT '性别(1男  2女)',
  `birth_date` date NULL DEFAULT NULL COMMENT '出生日期',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号码',
  `type` tinyint NOT NULL DEFAULT 0 COMMENT '用户类型(0内置用户 1注册用户)',
  `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1199019160365957125 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1000000000000000001, 'admin', '$2a$10$MR.UyWPrlIG1Qp8YCffaHuVFQuNwTNkXGucdDx6W2HjezeX/NR9ee', '管理员', 2, '2014-12-15', '18888888888', 1, 'https://mars-factory.oss-cn-beijing.aliyuncs.com/2023/12/27/fdf5a0c21dc64830b16f84607d631e9dlogo.jpg', '成都市', NULL, '2020-12-14 09:57:51', NULL, 1000000000000000001, '2024-03-05 17:48:52', 'admin', 0);
INSERT INTO `sys_user` VALUES (1167805277462855680, 'mars', '$2a$10$jIzbnaFLxA2UXgE.oAaShOronaqNOpguf0ZAG.wKqW2sjB0SH7y7W', '程序员Mars', 1, '2023-10-27', '18888888888', 1, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', NULL, '2023-10-28 12:41:15', NULL, 1000000000000000001, '2024-01-25 17:31:27', 'admin', 0);
INSERT INTO `sys_user` VALUES (1170035678893834240, 'zhangsan', '$2a$10$sYk0NHkdQ08G7rZk38K9y.LI6TTUfzV49A2/kofo2XehHbg.cq6xy', '张三', 1, '2023-11-02', '18483678311', 0, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', 1000000000000000001, '2023-11-03 16:24:04', 'admin', 1000000000000000001, '2023-11-03 16:24:04', 'admin', 1);
INSERT INTO `sys_user` VALUES (1170050787619897344, 'lisi', '$2a$10$2O6zNU6Wfzxz49pkvvPpXuadHh7y1wnhTTXyv0nvI2J9aO6/o6x2O', '李四', 1, '2023-11-02', '18483678323', 0, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', 1000000000000000001, '2023-11-03 17:24:06', 'admin', 1000000000000000001, '2023-12-27 17:39:47', 'admin', 0);
INSERT INTO `sys_user` VALUES (1171489978102841344, 'wangwu', '$2a$10$Awurz12q516iFaf2wYP0/On1uTpusixlsh1i3vaymHQV6s3AT5HCe', '王五', 1, '2023-11-07', '18483688322', 0, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', 1000000000000000001, '2023-11-07 16:42:56', 'admin', 1000000000000000001, '2023-11-07 16:42:56', 'admin', 1);
INSERT INTO `sys_user` VALUES (1199019160365957120, '测试用户', '$2a$10$TpHOrHVqVtFms/jILeXdoOSwC4A.8R.RRjhqdmZhbeNqWCoNA2oJK', 'Sol', 1, '2023-12-31', '13388880520', 0, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '', 1000000000000000001, '2024-01-22 15:54:05', 'admin', NULL, '2024-01-22 15:54:05', NULL, 1);
INSERT INTO `sys_user` VALUES (1199019160365957121, 'wangwu', '$2a$10$bGVEJf8UyjyPoIUkiUTrEuTF5a6XEJW.rDohEio5oW9NtDqS06mQK', '王五', 1, NULL, '18483678369', 1, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', NULL, NULL, '2024-01-31 14:29:37', NULL, NULL, '2024-01-31 14:29:37', NULL, 0);
INSERT INTO `sys_user` VALUES (1199019160365957122, 'wangwu1', '$2a$10$euNttcR6eEw6oxCeT/.ydOJUzbONndLBqDmQgI0qKGGLTHhgwajem', '王五1', 1, '2024-02-21', '18483686952', 1, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', NULL, NULL, '2024-01-31 14:33:35', NULL, 1000000000000000001, '2024-02-02 18:25:21', 'admin', 0);
INSERT INTO `sys_user` VALUES (1199019160365957123, 'wangwu2', '$2a$10$JU6AzER/q/XUkGGUGsWC..qJj4jNlmyUQ3B3wiOhHBYF4FPzuBQb2', '王五2', 1, '2024-02-08', '18483678612', 1, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', NULL, NULL, '2024-01-31 14:35:29', NULL, 1000000000000000001, '2024-02-02 18:25:46', 'admin', 0);
INSERT INTO `sys_user` VALUES (1199019160365957124, 'wangwu4', '$2a$10$eR39pL4Ofq9TT27JO4odPOEw9TLLBfOz.XkpIV0ZxsBVYRda4AUZG', '王五', 1, NULL, '1848367963', 1, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', NULL, NULL, '2024-01-31 23:01:08', NULL, NULL, '2024-01-31 23:01:08', NULL, 0);

-- ----------------------------
-- Table structure for sys_user_message_stats
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_message_stats`;
CREATE TABLE `sys_user_message_stats`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户ID',
  `un_read` int NULL DEFAULT 0 COMMENT '未读数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户消息统计表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_message_stats
-- ----------------------------
INSERT INTO `sys_user_message_stats` VALUES (3, 1000000000000000001, 0);
INSERT INTO `sys_user_message_stats` VALUES (4, 1167805277462855680, 4);
INSERT INTO `sys_user_message_stats` VALUES (5, 1170050787619897344, 6);
INSERT INTO `sys_user_message_stats` VALUES (6, 1171489978102841344, 98);
INSERT INTO `sys_user_message_stats` VALUES (7, 1199019160365957122, 9);
INSERT INTO `sys_user_message_stats` VALUES (8, 1199019160365957123, 5);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (6, 1171489978102841344, 1, '2023-11-28 13:29:52', '2023-11-28 13:29:52');
INSERT INTO `sys_user_post` VALUES (11, 1170050787619897344, 1, '2023-12-21 16:51:24', '2023-12-21 16:51:24');
INSERT INTO `sys_user_post` VALUES (12, 1199019160365957120, 5, '2024-01-22 15:54:05', '2024-01-22 15:54:05');
INSERT INTO `sys_user_post` VALUES (13, 1167805277462855680, 5, '2024-01-25 17:31:27', '2024-01-25 17:31:27');
INSERT INTO `sys_user_post` VALUES (17, 1000000000000000001, 1, '2024-03-05 09:48:52', '2024-03-05 09:48:52');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1214630727233044481 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户关联角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1052613759698206720, 1052613759316525056, 1052613468324102144, '2022-12-14 15:51:17', '2022-12-14 15:51:17');
INSERT INTO `sys_user_role` VALUES (1052613787443527680, 1052613658141523968, 1052613170658541568, '2022-12-14 15:51:24', '2022-12-14 15:51:24');
INSERT INTO `sys_user_role` VALUES (1167407043858923520, 1052613759316525091, 1052613170658541568, '2023-10-27 10:18:49', '2023-10-27 10:18:49');
INSERT INTO `sys_user_role` VALUES (1179051535192031232, 1171489978102841344, 1052613468324102144, '2023-11-28 13:29:52', '2023-11-28 13:29:52');
INSERT INTO `sys_user_role` VALUES (1187437174530768896, 1170050787619897344, 1170050863721349120, '2023-12-21 16:51:24', '2023-12-21 16:51:24');
INSERT INTO `sys_user_role` VALUES (1199019161217400832, 1199019160365957120, 1170050863721349120, '2024-01-22 15:54:05', '2024-01-22 15:54:05');
INSERT INTO `sys_user_role` VALUES (1200130828252741632, 1167805277462855680, 1200109719943905280, '2024-01-25 17:31:27', '2024-01-25 17:31:27');
INSERT INTO `sys_user_role` VALUES (1202259395858137088, 1199019160365957121, 1052613468324102144, '2024-01-31 14:29:37', '2024-01-31 14:29:37');
INSERT INTO `sys_user_role` VALUES (1202388124936699904, 1199019160365957124, 1052613468324102144, '2024-01-31 23:01:08', '2024-01-31 23:01:08');
INSERT INTO `sys_user_role` VALUES (1203043495003029504, 1199019160365957122, 1203042721313325056, '2024-02-02 18:25:21', '2024-02-02 18:25:21');
INSERT INTO `sys_user_role` VALUES (1203043601794203648, 1199019160365957123, 1200132110178844672, '2024-02-02 18:25:46', '2024-02-02 18:25:46');
INSERT INTO `sys_user_role` VALUES (1214630727233044480, 1000000000000000001, 1052613170658541568, '2024-03-05 17:48:52', '2024-03-05 17:48:52');

SET FOREIGN_KEY_CHECKS = 1;
