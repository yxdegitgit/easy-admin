/**
 * 分页查询动态列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apContent/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询动态详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apContent/query/' + id,
        method: 'get'
    })
}

/**
 * 新增动态
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apContent/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改动态
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apContent/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除动态
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apContent/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出动态
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/apContent/export',
        method: 'post',
        data: data
    })
}
