/**
 * 分页查询测试列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTest/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTest/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTest/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTest/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTest/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试
 *
 * @param query
 * @returns {*}
 */
function exportTest(query) {
    return requests({
        url: '/admin/apTest/export',
        method: 'get',
        params: query
    })
}
