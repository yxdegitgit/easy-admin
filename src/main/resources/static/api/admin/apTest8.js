/**
 * 分页查询测试8列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTest8/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试8详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTest8/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试8
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTest8/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试8
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTest8/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试8
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTest8/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试8
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/apTest8/export',
        method: 'post',
        data: data
    })
}
