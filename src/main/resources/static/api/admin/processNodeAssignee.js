/**
 * 分页查询审批人员列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/processNodeAssignee/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询审批人员详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/processNodeAssignee/query/' + id,
        method: 'get'
    })
}

/**
 * 新增审批人员
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/processNodeAssignee/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改审批人员
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/processNodeAssignee/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除审批人员
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/processNodeAssignee/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出审批人员
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/processNodeAssignee/export',
        method: 'post',
        data: data
    })
}
