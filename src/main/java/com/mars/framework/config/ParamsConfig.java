package com.mars.framework.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 参数配置
 *
 * @author 源码字节-程序员Mars
 */
@Configuration
@ConfigurationProperties(prefix = "params")
public class ParamsConfig {

    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
