package com.mars.framework.context;

import com.mars.common.base.UserContextInfo;

/**
 * 上下文管理器
 *
 * @author Mars
 */
public class ContextUserInfoThreadHolder {

    private final static InheritableThreadLocal<UserContextInfo> THREAD_LOCAL = new InheritableThreadLocal<>();

    /**
     * Set.
     *
     * @param value the value
     */
    public static void set(UserContextInfo value) {
        THREAD_LOCAL.set(value);
    }

    /**
     * Get context user info.
     *
     * @return the context user info
     */
    public static UserContextInfo get() {
        return THREAD_LOCAL.get();
    }

    /**
     * Remove.
     */
    public static void remove() {
        THREAD_LOCAL.remove();
    }

}
