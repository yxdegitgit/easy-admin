package com.mars.module.admin.service;

import com.mars.module.admin.entity.Students;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.StudentsRequest;

import java.util.List;

/**
 * 学生成绩接口
 *
 * @author mars
 * @date 2024-03-12
 */
public interface IStudentsService {
    /**
     * 新增
     *
     * @param param param
     * @return Students
     */
    Students add(StudentsRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Integer> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(StudentsRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return Students
     */
    Students getById(Integer id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<Students>
     */
    PageInfo<Students> pageList(StudentsRequest param);


    /**
     * 查询所有数据
     *
     * @return List<Students>
     */
    List<Students> list(StudentsRequest param);
}
