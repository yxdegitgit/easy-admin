package com.mars.module.tool.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mars.module.system.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 接口日志
 *
 * @author 源码字节-程序员Mars
 */
@Data
@TableName(value = "sys_log")
@EqualsAndHashCode(callSuper = true)
public class SysLog extends BaseEntity {
    /**
     * ID
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 接口地址
     */
    private String url;

    /**
     * 耗时
     */
    private Integer time;

    /**
     * IP
     */
    private String ip;

    /**
     * 请求参数
     */
    private String param;

    /**
     * 异常
     */
    private String err;


}
