package com.mars.module.tool.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-11-09 15:37:12
 */
@Data
@ApiModel(value = "导入表请求参数")
public class GenTableImportRequest {

    @ApiModelProperty(value = "表名称列表")
    private List<String> tableNames;
}
