package com.mars.module.tool.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-11-13 23:00:44
 */
@Data
@ApiModel(value = "建表请求参数")
public class CreateTableRequest {

    @ApiModelProperty(value = "表中文名")
    private String tableName;

    @ApiModelProperty(value = "表英文名称")
    private String tableEnName;

    @ApiModelProperty(value = "是否生成基础字段")
    private boolean genBaseField;

    @ApiModelProperty(value = "字段列表")
    private List<FieldRequest> fieldRequestList;

}
