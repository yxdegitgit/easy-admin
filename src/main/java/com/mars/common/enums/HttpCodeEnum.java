package com.mars.common.enums;

/**
 * 系统状态码
 * 200：成功
 * 500：失败
 * 403：token过期
 *
 * @author 源码字节-程序员Mars
 */
public enum HttpCodeEnum {

    /**
     * 成功
     */
    SUCCESS("200", "成功"),
    /**
     * 失败
     */
    ERROR("500", "系统异常，稍后重试"),
    /**
     * token过期
     */
    TOKEN_INVALID("403", "当前登录已过期，请重新登录");

    private String code;
    private String message;

    HttpCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String getValue(String index) {
        for (HttpCodeEnum dataSourceEnum : HttpCodeEnum.values()) {
            if (dataSourceEnum.getCode().equals(index)) {
                return dataSourceEnum.getMessage();
            }
        }
        return null;
    }
}
