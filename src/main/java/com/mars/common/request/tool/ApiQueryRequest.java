package com.mars.common.request.tool;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Api查询DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class ApiQueryRequest {

    @ApiModelProperty(value = "swagger地址")
    private String swaggerUrl;

    @ApiModelProperty(value = "接口地址")
    private String url;

    @ApiModelProperty(value = "描述")
    private String title;


}
