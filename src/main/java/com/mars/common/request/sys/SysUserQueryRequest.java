package com.mars.common.request.sys;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户查询DTO
 *
 * @author 源码字节-程序员Mars
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysUserQueryRequest extends PageRequest {

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "姓名")
    private String realName;

    @ApiModelProperty(value = "手机号码")
    private String phone;

}
